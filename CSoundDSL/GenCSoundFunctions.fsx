﻿
#r @"F:\src\music\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#I @"..\packages\FParsec.1.0.2\lib\net40-client\"
#r @"FParsec.dll"
#r @"FParsecCS.dll"

#load "AST.fs"
#load "CSoundOpcode.fs"
#load "CSoundFunctionGenerator.fs"

open System.Linq
open System.Collections.Generic

let genericChars = {(int)'a' .. (int)'z'}  |> Seq.append {(int)'A' .. (int)'Z'} |> Seq.toArray


let createFunctions (opcodes:IDictionary<string, IList<csound6netlib.OpcodeArgumentTypes>>) methodFn =
    let mutable methods = []
    for opcode in opcodes do
        let fnName = opcode.Key
        let dbgFn = "bformenc1"
        if fnName = dbgFn then
            printfn "Found %s" dbgFn
        let skip = [|"return"; "in"|] |> Array.contains fnName
        if not skip then
            let argsUnique = opcode.Value |> Seq.groupBy (fun opcode -> opcode.intypes)
//            let argsUnique = opcode.Value |> Seq.groupBy (fun opcode -> opcode.intypes, (opcode.outypes.Replace("[]", "").Length))
            for inargs, args in argsUnique do
                let uniqueArgs = args |> Seq.distinctBy (fun oc -> oc.intypes, oc.outypes) 
                let sigInstances = uniqueArgs |> Seq.length
                if sigInstances < 2 then
                    let meth = methodFn fnName (uniqueArgs |> Seq.head)
                    methods <- List.append methods [meth]
                elif sigInstances <= genericChars.Length then
                    let patchedArgs = csound6netlib.OpcodeArgumentTypes()
                    patchedArgs.intypes <- inargs
                    patchedArgs.outypes <- ""
                    let allOutTypes = uniqueArgs |> Seq.map (fun oc -> oc.outypes)
                                                    |> Seq.toArray
                    let mostArgsCount = allOutTypes |> Seq.maxBy (fun ot -> ot.Length)
                    let genericChars = genericChars |> Seq.take (min (genericChars.Count()) mostArgsCount.Length)
                                                    |> Seq.map char
                                                    |> Seq.map (fun x -> "'" + x.ToString())
                                                    |> Seq.toArray
                    let genericStr = sprintf "<%s>" (System.String.Join(",", genericChars))
                    let uncheckedStrs = genericChars |> Seq.map (fun c -> sprintf "Unchecked.defaultof<%s>" c) |> Seq.toArray
                    let retStr = System.String.Join(", ", uncheckedStrs)
                    let meth = methodFn (fnName + genericStr) patchedArgs
                    let methStr = meth.ToString()
                    //replace the unit () return with the generic defaults
                    let methRep = methStr.Substring(0, methStr.Length - 2) + retStr
                    let methRepLines = methRep.Split([|'\n'; '\r'|])
                    let commentRep = sprintf "%s -> (%s)" methRepLines.[0] (allOutTypes |> Seq.map (fun a -> a.ToString()) |> String.concat " | ")
                    let recombined = Array.append [|commentRep|] (methRepLines |> Array.skip 1) |> String.concat "\n"
                    methods <- List.append methods [recombined]
    methods


let getOpCodes fileName methodGenerator typeGenerator = async {
    let csound = new csound6netlib.Csound6Net()
    let! opcodeT = csound.GetOpcodeListAsync() |> Async.AwaitTask
    let methods = createFunctions opcodeT methodGenerator//CSoundCodeDomGen.createFunction
//    let opCodeFilt = opcodeT.Where(fun kvp -> kvp.Key.StartsWith("pol2r")).Select(fun kvp -> kvp.Key, kvp.Value) |> dict//.ToDictionary(fun kvp -> kvp.Key)//, fun kvp -> kvp.Value)
//    let methods = createFunctions opCodeFilt methodGenerator//CSoundCodeDomGen.createFunction
    let code = methods |> typeGenerator//CSoundCodeDomGen.generateTypes 
    printf "%s" code
    let ofle = System.IO.Path.Combine(__SOURCE_DIRECTORY__, fileName)
    printfn "Writing code out to %s" ofle
    System.IO.File.WriteAllText(ofle, code)

    }

//let vanillaFsharpFns = CSoundFunctionGenerator.createFunction CSoundOpcode.mapParamStr
//let vanillaHeader = """namespace CSoundCG
//open System"""
//getOpCodes "CSoundFunctionGen.fs" vanillaFsharpFns (CSoundFunctionGenerator.generateTypes vanillaHeader) |> Async.RunSynchronously
let vectorFsharpFns = CSoundFunctionGenerator.createFunction CSoundOpcode.mapParamStrVector
let vectorHeader = """namespace CSoundCGVector
open System
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic"""
getOpCodes "CSoundFunctionGenVector.fs" vectorFsharpFns (CSoundFunctionGenerator.generateTypes vectorHeader) |> Async.RunSynchronously
//getOpCodes |> Async.RunSynchronously


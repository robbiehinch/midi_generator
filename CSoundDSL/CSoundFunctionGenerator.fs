﻿module CSoundFunctionGenerator


let createDefaultStatement (name:string) (atts:string list) =
    let defaultVal = atts |> List.head
    let nameSplit = name.Split( [|' '|])
    let nameLast = (Array.last nameSplit)
    sprintf "let %s = defaultArg %s %s" nameLast nameLast defaultVal

let createDeclarationExpression (paramType:string) (name:string) (atts:string list) =
    let optional = if atts |> List.isEmpty then "" else "?"
    let nameSplit = name.Split( [|' '|])
    let nameLast = sprintf "%s%s" optional (Array.last nameSplit)
    let nameSplitLen = Array.length nameSplit
    let nameFirst = if nameSplitLen > 1 then Array.take (nameSplitLen-1) nameSplit else [||]
    let newNameSplit = Array.concat [| nameFirst; [| nameLast |] |]
    let nameFlat = String.concat " " newNameSplit
    sprintf "%s:%s" nameFlat paramType

let toMap dictionary = 
    (dictionary :> seq<_>)
    |> Seq.map (|KeyValue|)
    |> Map.ofSeq

let findType (t:string) (prms:Map<string,string>) =
    match Map.tryFind t prms with
    | Some v -> v
    | _ -> match t with
            | "unit" -> "()" 
            | "decimal" -> "0.0m"
//            | "double" -> "0.0"
            | "single" -> "0.0f"
            | "int" -> "0"
            | "double[]" -> "[|(double)0.0|]"
            | "vector<double>" -> "vector [(double)0.0]"
            | "Vector<double>" -> "vector [(double)0.0]"
            | "decimal[]" -> "[|0.0m|]"
            | _ -> "0.0"


let createReturnStatement (prms:System.Collections.Generic.IDictionary<string,string>) (retArgs:string[]) =
    let paramMap = toMap prms
    let argNamesAsReferenced = retArgs |> Array.map (fun arg -> findType arg paramMap)
    if argNamesAsReferenced |> Array.isEmpty then "()" else argNamesAsReferenced |> String.concat ", "

let createFunction paramMapFn (fnName:string) (args:csound6netlib.OpcodeArgumentTypes) =
    let origSigComment = sprintf "//%s %s %s" fnName (args.intypes.ToString()) (args.outypes.ToString())
    let inargsP = CSoundOpcode.parseArgumentDefinition paramMapFn args.intypes
                            |> List.filter (fun (p, n, a) -> null <> p)
    let inargsNamed = [0 .. inargsP.Length]
                            |> Seq.zip inargsP
                            |> Seq.map (fun ((p, n, a), c) -> p, (n+c.ToString()), a)
    let inArgTypeDict = inargsNamed |> Seq.groupBy (fun (p, n, c) -> p)
                                    |> Seq.map (fun (k, s) -> k, (s |> Seq.head) |> (fun (a, b, c) -> b))
                                    |> dict
    let paramDecs = inargsNamed |> Seq.map (fun (p, n, c) -> createDeclarationExpression p n c)
    let defaultStatements = inargsNamed |> Seq.filter (fun (p, n, c) -> not c.IsEmpty)
                                        |> Seq.map (fun (p, n, c) -> createDefaultStatement n c)
//    if fnName.StartsWith("FL") then
//        printfn "%s" fnName
    let fnSig = sprintf "static member %s (%s) =" fnName (System.String.Join(", ", paramDecs))   
//    let fnSig = sprintf "static member %s %s =" fnName (System.String.Join(" ", paramDecs))   
//    let fnSig = sprintf "let %s %s =" fnName (System.String.Join(" ", paramDecs))
    let outargsP = CSoundOpcode.parseArgumentDefinition paramMapFn args.outypes
    let retStatement = createReturnStatement inArgTypeDict (outargsP |> List.map (fun (a, _, _) -> a) |> List.toArray)
    let allStatements = Seq.append defaultStatements [|retStatement|] |> Seq.map (fun s -> "        " + s)
                                                                        |> Seq.toArray
    let functionBody = System.String.Join(System.Environment.NewLine, allStatements)
    sprintf """    %s
    %s
%s""" origSigComment fnSig functionBody
    
let generateTypes headerStr (methods:seq<string>) = 
    let functionDefs = System.String.Join(System.Environment.NewLine, methods)
    sprintf """%s
type Functions() =
%s""" headerStr functionDefs


﻿module Compiler

open Microsoft.FSharp.Quotations
open QuotationActivePatterns


type CompileState = { IsMultiParamFn : bool }

let mapFstArray sep x = x |> List.map fst |> List.toArray |> String.concat sep

open MathNet.Numerics.LinearAlgebra.Double
let vec = vector [0.0]



let compileInstrument outputHookHandler exprIn =
    
    let (|HookMatcher|_|) expr = outputHookHandler expr

    let rec compileRec exprMatch =
        match exprMatch with
        | Patterns.Application(expr1, expr2) ->
            // Function application.
            let r1 = compileRec expr1
            let r2 = compileRec expr2
            sprintf "%s %s" (fst r1) (fst r2), { CompileState.IsMultiParamFn = ((snd r1).IsMultiParamFn || (snd r2).IsMultiParamFn) }
        | HookMatcher(hookRet) ->
            hookRet
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.int @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.decimal @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.float32 @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.float @> (_, _, exprList) ->
            exprList |> List.map compileRec |> mapFstArray System.String.Empty, { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.LanguagePrimitives.IntrinsicFunctions.GetArray @> (_, _, exprList) ->
            let r1 = compileRec exprList.Head
            let r2 = compileRec exprList.Tail.Head
            sprintf "%s[%s]" (fst r1) (fst r2), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.LanguagePrimitives.IntrinsicFunctions.SetArray @> (_, _, exprList) ->
            let r1 = compileRec exprList.Head
            let r2 = compileRec exprList.Tail.Head
            let r3 = compileRec exprList.Tail.Tail.Head
            sprintf "%s[%s] = %s" (fst r1) (fst r2) (fst r3), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ (+) @> (_, _, exprList) ->
            sprintf "( %s )" (exprList |> List.map compileRec |> mapFstArray " + "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ (-) @> (_, _, exprList) ->
            sprintf "( %s )" (exprList |> List.map compileRec |> mapFstArray " - "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ (*) @> (_, _, exprList) ->
            sprintf "( %s )" (exprList |> List.map compileRec |> mapFstArray " * "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ (/) @> (exprOpt, methodInfo, exprList) ->
            sprintf "( %s )" (exprList |> List.map compileRec |> mapFstArray " / "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ (%) @> (exprOpt, methodInfo, exprList) ->
            sprintf "( %s )" (exprList |> List.map compileRec |> mapFstArray " % "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.SpecificCall <@ vec.PointwiseMultiply @> (instanceExprOption, _, exprList) ->
            let objVar, _ = match instanceExprOption with
                            | Some instanceExpr -> compileRec instanceExpr
                            | _ -> System.String.Empty, { CompileState.IsMultiParamFn = false }
            sprintf "( %s * %s )" objVar (exprList |> List.map compileRec |> mapFstArray " * "), { CompileState.IsMultiParamFn = false }
        | Patterns.NewArray(_, exprList)
        | Patterns.NewTuple(exprList)
        | Patterns.NewUnionCase(_, exprList)
        | DerivedPatterns.SpecificCall <@ vector @> (_, _, exprList) ->
            (exprList |> List.map compileRec |> mapFstArray ", "), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.Decimal(n) ->
            n.ToString(), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.Int32(n) ->
            n.ToString(), { CompileState.IsMultiParamFn = false }
        | Patterns.Call(None, DerivedPatterns.MethodWithReflectedDefinition(n), expList) ->
//            printf "MethodWithReflectedDefinition"
            compileRec n
        | Patterns.Call(exprOpt, methodInfo, exprList) ->
            // Method or module function call.
            let opt = match exprOpt with
                                | Some e -> fst (compileRec e)
                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
            let callParams = exprList |> List.map compileRec
                                    |> List.map fst
                                    |> List.filter (fun e -> System.String.IsNullOrWhiteSpace(e) |> not)
                                    |> List.toArray
            let callParmStr = callParams |> String.concat ", "
            let methodName = methodInfo.Name
            if methodName <> "chnget" && callParmStr.Contains(", ") |> not then
                sprintf "%s %s( %s )" opt methodName callParmStr, { CompileState.IsMultiParamFn = false }
            else
                sprintf "%s %s %s" opt methodName callParmStr, { CompileState.IsMultiParamFn = true }

        | Patterns.Lambda(param, body) ->
            // Lambda expression.
//            printf "fun (%s:%s) -> " param.Name (param.Type.ToString())
            compileRec body
        | TupleLet(vars, expr1, expr2) ->
            //if any are mutable, they are all mutable
            let varNames = vars |> List.map (function | Some(v) -> v.Name | None -> "_")
            let e1Compiled, compileState = compileRec expr1
            let equalsStr = if compileState.IsMultiParamFn then " " else " = "
            sprintf "%s %s %s %s %s" (varNames |> String.concat ", ") equalsStr (e1Compiled) (System.Environment.NewLine) (compileRec expr2 |> fst), { CompileState.IsMultiParamFn = false }
        | Patterns.Let(var, expr1, expr2) ->
            let e1Compiled, compileState = compileRec expr1
            let equalsStr = if compileState.IsMultiParamFn then " " else " = "
            sprintf "%s %s %s %s %s" (var.Name) equalsStr (e1Compiled) (System.Environment.NewLine) (compileRec expr2 |> fst), { CompileState.IsMultiParamFn = false }
        | Patterns.TupleGet(expr, i) ->
            sprintf "%s.[%d]" (compileRec expr |> fst) i, { CompileState.IsMultiParamFn = false }
        | Patterns.PropertyGet(_, propOrValInfo, _) ->
            propOrValInfo.Name, { CompileState.IsMultiParamFn = false }
        | Patterns.PropertySet(_, propOrValInfo, _, e) ->
            sprintf "%s %s" (propOrValInfo.Name) (compileRec e |> fst), { CompileState.IsMultiParamFn = false }
        | DerivedPatterns.String(str) ->
            sprintf "\"%s\"" str, { CompileState.IsMultiParamFn = false }
        | Patterns.Value(value, typ) ->
            (if null <> value then value.ToString() else System.String.Empty), { CompileState.IsMultiParamFn = false }
        | Patterns.Sequential(expr1, expr2) ->
            let e1, _ = compileRec expr1
            let e2, _ = compileRec expr2
            sprintf "%s %s %s" e1 System.Environment.NewLine e2, { CompileState.IsMultiParamFn = false }
        | Patterns.FieldSet(exprOpt, fi, expr) ->
            let opt = match exprOpt with
                                | Some expr -> fst (compileRec expr)
                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
            let rhs, rhsState = compileRec expr
            let assignExpr = if rhsState.IsMultiParamFn then " " else " = "
            sprintf "%s%s%s" fi.Name assignExpr rhs, { CompileState.IsMultiParamFn = false }
        | Patterns.FieldGet(exprOpt, fi) ->
            let opt = match exprOpt with
                                | Some expr -> fst (compileRec expr)
                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
            sprintf "%s" fi.Name, { CompileState.IsMultiParamFn = false }
        | Patterns.Var(var) ->
            var.Name, { CompileState.IsMultiParamFn = false }
        | Patterns.IfThenElse(condExpr, tbExpr, fbExpr) ->
            let cond, _ = compileRec condExpr
            let tb, _ = compileRec tbExpr
            let fb, _ = compileRec fbExpr
            sprintf "if ( %s ) then
                        %s
                        else
                        %s
                        endif" cond tb fb, { CompileState.IsMultiParamFn = false }
        | Patterns.Coerce(expr, _) ->
            compileRec expr
        | _ -> sprintf "%s ;Unhandled" (exprMatch.ToString()), { CompileState.IsMultiParamFn = false }
    compileRec exprIn |> fst


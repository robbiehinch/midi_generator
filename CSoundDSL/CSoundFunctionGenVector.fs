namespace CSoundCGVector
open System
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic
type Functions() =
    //a k a
    static member a (k0:single) =
        vector [(double)0.0]
    //abs a a
    static member abs (a0:Vector<double>) =
        a0
    //abs i i
    static member abs (i0:decimal) =
        i0
    //abs k k
    static member abs (k0:single) =
        k0
    //active<'A> Soo  -> (i | k)

    static member active<'A> (s0:string, ?o1:decimal, ?o2:decimal) =

        let o1 = defaultArg o1 0m

        let o2 = defaultArg o2 0m

        Unchecked.defaultof<'A>
    //active ioo i
    static member active (i0:decimal, ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        i0
    //active koo k
    static member active (k0:single, ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        k0
    //adsr<'A> iiiio  -> (a | k)

    static member adsr<'A> (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal) =

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //adsyn kkkSo a
    static member adsyn (k0:single, k1:single, k2:single, s3:string, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //adsyn kkkio a
    static member adsyn (k0:single, k1:single, k2:single, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //adsynt kkiiiio a
    static member adsynt (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //adsynt2 kkiiiio a
    static member adsynt2 (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //aftouch oh k
    static member aftouch (?o0:decimal, ?h1:decimal) =
        let o0 = defaultArg o0 0m
        let h1 = defaultArg h1 127m
        0.0f
    //alpass axioo a
    static member alpass (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //alwayson Sm 
    static member alwayson (s0:string, m1:Vector<double>) =
        ()
    //alwayson im 
    static member alwayson (i0:decimal, m1:Vector<double>) =
        ()
    //ampdb a a
    static member ampdb (a0:Vector<double>) =
        a0
    //ampdb i i
    static member ampdb (i0:decimal) =
        i0
    //ampdb k k
    static member ampdb (k0:single) =
        k0
    //ampdbfs a a
    static member ampdbfs (a0:Vector<double>) =
        a0
    //ampdbfs i i
    static member ampdbfs (i0:decimal) =
        i0
    //ampdbfs k k
    static member ampdbfs (k0:single) =
        k0
    //ampmidi io i
    static member ampmidi (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //ampmidid ii i
    static member ampmidid (i0:decimal, i1:decimal) =
        i0
    //ampmidid ki k
    static member ampmidid (k0:single, i1:decimal) =
        k0
    //areson aaaoo a
    static member areson (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //areson aakoo a
    static member areson (a0:Vector<double>, a1:Vector<double>, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //areson akaoo a
    static member areson (a0:Vector<double>, k1:single, a2:Vector<double>, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //areson akkoo a
    static member areson (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //aresonk kkkpo k
    static member aresonk (k0:single, k1:single, k2:single, ?p3:decimal, ?o4:decimal) =
        let p3 = defaultArg p3 1m
        let o4 = defaultArg o4 0m
        k0
    //array<'A,'B,'C> m  -> (i[] | k[])

    static member array<'A,'B,'C> (m0:Vector<double>) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>
    //atone ako a
    static member atone (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //atonek kko k
    static member atonek (k0:single, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //atonex aaoo a
    static member atonex (a0:Vector<double>, a1:Vector<double>, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0
    //atonex akoo a
    static member atonex (a0:Vector<double>, k1:single, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0
    //ATSadd kkSiiopo a
    static member ATSadd (k0:single, k1:single, s2:string, i3:decimal, i4:decimal, ?o5:decimal, ?p6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //ATSadd kkiiiopo a
    static member ATSadd (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?p6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //ATSaddnz kSiop a
    static member ATSaddnz (k0:single, s1:string, i2:decimal, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        vector [(double)0.0]
    //ATSaddnz kiiop a
    static member ATSaddnz (k0:single, i1:decimal, i2:decimal, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        vector [(double)0.0]
    //ATSbufread kkSiop 
    static member ATSbufread (k0:single, k1:single, s2:string, i3:decimal, ?o4:decimal, ?p5:decimal) =
        let o4 = defaultArg o4 0m
        let p5 = defaultArg p5 1m
        ()
    //ATSbufread kkiiop 
    static member ATSbufread (k0:single, k1:single, i2:decimal, i3:decimal, ?o4:decimal, ?p5:decimal) =
        let o4 = defaultArg o4 0m
        let p5 = defaultArg p5 1m
        ()
    //ATScross kkSikkiopoo a
    static member ATScross (k0:single, k1:single, s2:string, i3:decimal, k4:single, k5:single, i6:decimal, ?o7:decimal, ?p8:decimal, ?o9:decimal, ?o10:decimal) =
        let o7 = defaultArg o7 0m
        let p8 = defaultArg p8 1m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        vector [(double)0.0]
    //ATScross kkiikkiopoo a
    static member ATScross (k0:single, k1:single, i2:decimal, i3:decimal, k4:single, k5:single, i6:decimal, ?o7:decimal, ?p8:decimal, ?o9:decimal, ?o10:decimal) =
        let o7 = defaultArg o7 0m
        let p8 = defaultArg p8 1m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        vector [(double)0.0]
    //ATSinfo Si i
    static member ATSinfo (s0:string, i1:decimal) =
        i1
    //ATSinfo ii i
    static member ATSinfo (i0:decimal, i1:decimal) =
        i0
    //ATSinterpread k k
    static member ATSinterpread (k0:single) =
        k0
    //ATSpartialtap i kk
    static member ATSpartialtap (i0:decimal) =
        0.0f, 0.0f
    //ATSread kSi kk
    static member ATSread (k0:single, s1:string, i2:decimal) =
        k0, k0
    //ATSread kii kk
    static member ATSread (k0:single, i1:decimal, i2:decimal) =
        k0, k0
    //ATSreadnz kSi k
    static member ATSreadnz (k0:single, s1:string, i2:decimal) =
        k0
    //ATSreadnz kii k
    static member ATSreadnz (k0:single, i1:decimal, i2:decimal) =
        k0
    //ATSsinnoi kkkkSiop a
    static member ATSsinnoi (k0:single, k1:single, k2:single, k3:single, s4:string, i5:decimal, ?o6:decimal, ?p7:decimal) =
        let o6 = defaultArg o6 0m
        let p7 = defaultArg p7 1m
        vector [(double)0.0]
    //ATSsinnoi kkkkiiop a
    static member ATSsinnoi (k0:single, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, ?o6:decimal, ?p7:decimal) =
        let o6 = defaultArg o6 0m
        let p7 = defaultArg p7 1m
        vector [(double)0.0]
    //babo akkkiiijj aa
    static member babo (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, i6:decimal, ?j7:decimal, ?j8:decimal) =
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        a0, a0
    //balance aaqo a
    static member balance (a0:Vector<double>, a1:Vector<double>, ?q2:decimal, ?o3:decimal) =
        let q2 = defaultArg q2 10m
        let o3 = defaultArg o3 0m
        a0
    //bamboo kioooooo a
    static member bamboo (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //barmodel kkiikiiii a
    static member barmodel (k0:single, k1:single, i2:decimal, i3:decimal, k4:single, i5:decimal, i6:decimal, i7:decimal, i8:decimal) =
        vector [(double)0.0]
    //bbcutm aiiiiipop a
    static member bbcutm (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?p6:decimal, ?o7:decimal, ?p8:decimal) =
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        let p8 = defaultArg p8 1m
        a0
    //bbcuts aaiiiiipop aa
    static member bbcuts (a0:Vector<double>, a1:Vector<double>, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, ?p7:decimal, ?o8:decimal, ?p9:decimal) =
        let p7 = defaultArg p7 1m
        let o8 = defaultArg o8 0m
        let p9 = defaultArg p9 1m
        a0, a0
    //betarand<'A> kkk  -> (a | i | k)

    static member betarand<'A> (k0:single, k1:single, k2:single) =

        Unchecked.defaultof<'A>
    //bexprnd<'A> k  -> (a | i | k)

    static member bexprnd<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //bformdec iaaay mmmmmmmm
    static member bformdec (i0:decimal, a1:Vector<double>, a2:Vector<double>, a3:Vector<double>, y4:Vector<double>[][]) =
        a1, a1, a1, a1, a1, a1, a1, a1
    //bformdec1 ia[] a[]
    static member bformdec1 (i0:decimal, a1:Vector<double>, M2:decimal[]) =
        a1, M2, 0.0
    //bformdec1 iy mmmmmmmm
    static member bformdec1 (i0:decimal, y1:Vector<double>[][]) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //bformenc akkPPPP mmmmmmmmmmmmmmmm
    static member bformenc (a0:Vector<double>, k1:single, k2:single) =
        a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0
    //bformenc1<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P> akk  -> (a[] | mmmmmmmmmmmmmmmm)

    static member bformenc1<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P> (a0:Vector<double>, k1:single, k2:single) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>
    //binit fi f
    static member binit (S0:_[], i1:decimal) =
        S0
    //biquad akkkkkko a
    static member biquad (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0
    //biquada aaaaaaao a
    static member biquada (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, a3:Vector<double>, a4:Vector<double>, a5:Vector<double>, a6:Vector<double>, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0
    //birnd i i
    static member birnd (i0:decimal) =
        i0
    //birnd k k
    static member birnd (k0:single) =
        k0
    //bqrez axxoo a
    static member bqrez (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //buchla aakkaPP a
    static member buchla (a0:Vector<double>, a1:Vector<double>, k2:single, k3:single, a4:Vector<double>) =
        a0
    //butbp axxo a
    static member butbp (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //butbr axxo a
    static member butbr (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //buthp aao a
    static member buthp (a0:Vector<double>, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //buthp ako a
    static member buthp (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butlp aao a
    static member butlp (a0:Vector<double>, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butlp ako a
    static member butlp (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butterbp axxo a
    static member butterbp (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //butterbr axxo a
    static member butterbr (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //butterhp aao a
    static member butterhp (a0:Vector<double>, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butterhp ako a
    static member butterhp (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butterlp aao a
    static member butterlp (a0:Vector<double>, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //butterlp ako a
    static member butterlp (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //buzz xxkio a
    static member buzz (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, k2:single, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //c2r k[] k[]
    static member c2r (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //cabasa iiooo a
    static member cabasa (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //cauchy<'A> k  -> (a | i | k)

    static member cauchy<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //cauchyi<'A> kxx  -> (a | i | k)

    static member cauchyi<'A> (k0:single, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray) =

        Unchecked.defaultof<'A>
    //ceil a a
    static member ceil (a0:Vector<double>) =
        a0
    //ceil i i
    static member ceil (i0:decimal) =
        i0
    //ceil k k
    static member ceil (k0:single) =
        k0
    //cell kkiiii 
    static member cell (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal) =
        ()
    //cent a a
    static member cent (a0:Vector<double>) =
        a0
    //cent i i
    static member cent (i0:decimal) =
        i0
    //cent k k
    static member cent (k0:single) =
        k0
    //centroid aki k
    static member centroid (a0:Vector<double>, k1:single, i2:decimal) =
        k1
    //ceps k[]k k[]
    static member ceps (k0:single, M1:decimal[], k2:single) =
        k0, M1, 0.0
    //cggoto Bl 
    static member cggoto (B0:_, l1:_) =
        ()
    //chanctrl<'A> iioh  -> (i | k)

    static member chanctrl<'A> (i0:decimal, i1:decimal, ?o2:decimal, ?h3:decimal) =

        let o2 = defaultArg o2 0m

        let h3 = defaultArg h3 127m

        Unchecked.defaultof<'A>
    //changed S k
    static member changed (s0:string) =
        0.0f
    //changed z k
    static member changed (z0:single[]) =
        0.0f
    //chani<'A> k  -> (a | k)

    static member chani<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //chano ak 
    static member chano (a0:Vector<double>, k1:single) =
        ()
    //chano kk 
    static member chano (k0:single, k1:single) =
        ()
    //chebyshevpoly az a
    static member chebyshevpoly (a0:Vector<double>, z1:single[]) =
        a0
    //chn_a Si 
    static member chn_a (s0:string, i1:decimal) =
        ()
    //chn_k SiooooooooN 
    static member chn_k (s0:string, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, [<ParamArray>] ?N10:Object[]) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let N10 = defaultArg N10 
        ()
    //chn_S Si 
    static member chn_S (s0:string, i1:decimal) =
        ()
    //chnclear S 
    static member chnclear (s0:string) =
        ()
    //chnexport<'A> Si  -> (S | a)

    static member chnexport<'A> (s0:string, i1:decimal) =

        Unchecked.defaultof<'A>
    //chnexport<'A> Sioooo  -> (i | k)

    static member chnexport<'A> (s0:string, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        let o5 = defaultArg o5 0m

        Unchecked.defaultof<'A>
    //chnget<'A> S  -> (S | a | i | k)

    static member chnget<'A> (s0:string) =

        Unchecked.defaultof<'A>
    //chnmix aS 
    static member chnmix (a0:Vector<double>, s1:string) =
        ()
    //chnparams S iiiiii
    static member chnparams (s0:string) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //chnset SS 
    static member chnset (s0:string, s1:string) =
        ()
    //chnset aS 
    static member chnset (a0:Vector<double>, s1:string) =
        ()
    //chnset iS 
    static member chnset (i0:decimal, s1:string) =
        ()
    //chnset kS 
    static member chnset (k0:single, s1:string) =
        ()
    //chuap kkkkkkkkiiik aaa
    static member chuap (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, i8:decimal, i9:decimal, i10:decimal, k11:single) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //cigoto Bl 
    static member cigoto (B0:_, l1:_) =
        ()
    //cingoto Bl 
    static member cingoto (B0:_, l1:_) =
        ()
    //ckgoto Bl 
    static member ckgoto (B0:_, l1:_) =
        ()
    //clear y 
    static member clear (y0:Vector<double>[][]) =
        ()
    //clfilt akiioppo a
    static member clfilt (a0:Vector<double>, k1:single, i2:decimal, i3:decimal, ?o4:decimal, ?p5:decimal, ?p6:decimal, ?o7:decimal) =
        let o4 = defaultArg o4 0m
        let p5 = defaultArg p5 1m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        a0
    //clip aiiv a
    static member clip (a0:Vector<double>, i1:decimal, i2:decimal, ?v3:decimal) =
        let v3 = defaultArg v3 0.5m
        a0
    //clockoff i 
    static member clockoff (i0:decimal) =
        ()
    //clockon i 
    static member clockon (i0:decimal) =
        ()
    //cmplxprod k[]k[] k[]
    static member cmplxprod (k0:single, M1:decimal[], k2:single, M3:decimal[]) =
        k0, M1, 0.0
    //cngoto Bl 
    static member cngoto (B0:_, l1:_) =
        ()
    //comb akioo a
    static member comb (a0:Vector<double>, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //combinv akioo a
    static member combinv (a0:Vector<double>, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //compilecsd S i
    static member compilecsd (s0:string) =
        0.0m
    //compileorc S i
    static member compileorc (s0:string) =
        0.0m
    //compilestr S i
    static member compilestr (s0:string) =
        0.0m
    //compress aakkkkkki a
    static member compress (a0:Vector<double>, a1:Vector<double>, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, i8:decimal) =
        a0
    //connect SSSSp 
    static member connect (s0:string, s1:string, s2:string, s3:string, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        ()
    //connect SSiSp 
    static member connect (s0:string, s1:string, i2:decimal, s3:string, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        ()
    //connect iSSSp 
    static member connect (i0:decimal, s1:string, s2:string, s3:string, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        ()
    //connect iSiSp 
    static member connect (i0:decimal, s1:string, i2:decimal, s3:string, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        ()
    //convle aSo mmmm
    static member convle (a0:Vector<double>, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0, a0, a0, a0
    //convle aio mmmm
    static member convle (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0, a0, a0, a0
    //convolve aSo mmmm
    static member convolve (a0:Vector<double>, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0, a0, a0, a0
    //convolve aio mmmm
    static member convolve (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0, a0, a0, a0
    //copy2ftab k[]k 
    static member copy2ftab (k0:single, M1:decimal[], k2:single) =
        ()
    //copy2ttab k[]k 
    static member copy2ttab (k0:single, M1:decimal[], k2:single) =
        ()
    //copya2ftab i[]i 
    static member copya2ftab (i0:decimal, M1:decimal[], i2:decimal) =
        ()
    //copya2ftab k[]k 
    static member copya2ftab (k0:single, M1:decimal[], k2:single) =
        ()
    //copyf2array i[]i 
    static member copyf2array (i0:decimal, M1:decimal[], i2:decimal) =
        ()
    //copyf2array k[]k 
    static member copyf2array (k0:single, M1:decimal[], k2:single) =
        ()
    //cos a a
    static member cos (a0:Vector<double>) =
        a0
    //cos i i
    static member cos (i0:decimal) =
        i0
    //cos k k
    static member cos (k0:single) =
        k0
    //cosh a a
    static member cosh (a0:Vector<double>) =
        a0
    //cosh i i
    static member cosh (i0:decimal) =
        i0
    //cosh k k
    static member cosh (k0:single) =
        k0
    //cosinv a a
    static member cosinv (a0:Vector<double>) =
        a0
    //cosinv i i
    static member cosinv (i0:decimal) =
        i0
    //cosinv k k
    static member cosinv (k0:single) =
        k0
    //cosseg<'A> iin  -> (a | k)

    static member cosseg<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //cossegb<'A> iin  -> (a | k)

    static member cossegb<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //cossegr<'A> iin  -> (a | k)

    static member cossegr<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //cps2pch ii i
    static member cps2pch (i0:decimal, i1:decimal) =
        i0
    //cpsmidi  i
    static member cpsmidi () =
        0.0m
    //cpsmidib<'A> o  -> (i | k)

    static member cpsmidib<'A> (?o0:decimal) =

        let o0 = defaultArg o0 0m

        Unchecked.defaultof<'A>
    //cpsmidinn i i
    static member cpsmidinn (i0:decimal) =
        i0
    //cpsmidinn k k
    static member cpsmidinn (k0:single) =
        k0
    //cpsoct a a
    static member cpsoct (a0:Vector<double>) =
        a0
    //cpsoct i i
    static member cpsoct (i0:decimal) =
        i0
    //cpsoct k k
    static member cpsoct (k0:single) =
        k0
    //cpspch i i
    static member cpspch (i0:decimal) =
        i0
    //cpspch k k
    static member cpspch (k0:single) =
        k0
    //cpstmid i i
    static member cpstmid (i0:decimal) =
        i0
    //cpstun kkk k
    static member cpstun (k0:single, k1:single, k2:single) =
        k0
    //cpstuni ii i
    static member cpstuni (i0:decimal, i1:decimal) =
        i0
    //cpsxpch iiii i
    static member cpsxpch (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        i0
    //cpuprc Si 
    static member cpuprc (s0:string, i1:decimal) =
        ()
    //cpuprc ii 
    static member cpuprc (i0:decimal, i1:decimal) =
        ()
    //cross2 aaiiik a
    static member cross2 (a0:Vector<double>, a1:Vector<double>, i2:decimal, i3:decimal, i4:decimal, k5:single) =
        a0
    //crossfm xxxxkiioo aa
    static member crossfm (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crossfmi xxxxkiioo aa
    static member crossfmi (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crossfmpm xxxxkiioo aa
    static member crossfmpm (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crossfmpmi xxxxkiioo aa
    static member crossfmpmi (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crosspm xxxxkiioo aa
    static member crosspm (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crosspmi xxxxkiioo aa
    static member crosspmi (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, i5:decimal, i6:decimal, ?o7:decimal, ?o8:decimal) =
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //crunch iiooo a
    static member crunch (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //ctlchn oo kkk
    static member ctlchn (?o0:decimal, ?o1:decimal) =
        let o0 = defaultArg o0 0m
        let o1 = defaultArg o1 0m
        0.0f, 0.0f, 0.0f
    //ctrl14 iiiiio i
    static member ctrl14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        i0
    //ctrl14 iiikko k
    static member ctrl14 (i0:decimal, i1:decimal, i2:decimal, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        k3
    //ctrl21 iiiiiio i
    static member ctrl21 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        i0
    //ctrl21 iiiikko k
    static member ctrl21 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, k4:single, k5:single, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        k4
    //ctrl7 iikkoo a
    static member ctrl7 (i0:decimal, i1:decimal, k2:single, k3:single, ?o4:decimal, ?o5:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //ctrl7 iiiio i
    static member ctrl7 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        i0
    //ctrl7 iikko k
    static member ctrl7 (i0:decimal, i1:decimal, k2:single, k3:single, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        k2
    //ctrlinit im 
    static member ctrlinit (i0:decimal, m1:Vector<double>) =
        ()
    //cuserrnd<'A> kkk  -> (a | k)

    static member cuserrnd<'A> (k0:single, k1:single, k2:single) =

        Unchecked.defaultof<'A>
    //cuserrnd iii i
    static member cuserrnd (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //dam akiiii a
    static member dam (a0:Vector<double>, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal) =
        a0
    //date  i
    static member date () =
        0.0m
    //dates j S
    static member dates (?j0:decimal) =
        let j0 = defaultArg j0 -0.1m
        0.0
    //db a a
    static member db (a0:Vector<double>) =
        a0
    //db i i
    static member db (i0:decimal) =
        i0
    //db k k
    static member db (k0:single) =
        k0
    //dbamp i i
    static member dbamp (i0:decimal) =
        i0
    //dbamp k k
    static member dbamp (k0:single) =
        k0
    //dbfsamp i i
    static member dbfsamp (i0:decimal) =
        i0
    //dbfsamp k k
    static member dbfsamp (k0:single) =
        k0
    //dcblock ao a
    static member dcblock (a0:Vector<double>, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        a0
    //dcblock2 aoo a
    static member dcblock2 (a0:Vector<double>, ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        a0
    //dconv aii a
    static member dconv (a0:Vector<double>, i1:decimal, i2:decimal) =
        a0
    //delay aio a
    static member delay (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //delay1 ao a
    static member delay1 (a0:Vector<double>, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        a0
    //delayk kio k
    static member delayk (k0:single, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //delayr io aX
    static member delayr (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //delayw a 
    static member delayw (a0:Vector<double>) =
        ()
    //deltap ko a
    static member deltap (k0:single, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //deltap3 xo a
    static member deltap3 (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //deltapi xo a
    static member deltapi (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //deltapn xo a
    static member deltapn (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //deltapx aio a
    static member deltapx (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //deltapxw aaio 
    static member deltapxw (a0:Vector<double>, a1:Vector<double>, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //denorm y 
    static member denorm (y0:Vector<double>[][]) =
        ()
    //diff<'A> xo  -> (a | k)

    static member diff<'A> (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =

        let o1 = defaultArg o1 0m

        Unchecked.defaultof<'A>
    //diskgrain Skkkkkiipo mmmm
    static member diskgrain (s0:string, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, ?p8:decimal, ?o9:decimal) =
        let p8 = defaultArg p8 1m
        let o9 = defaultArg o9 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //diskin<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> Skooooooo  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member diskin<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> (s0:string, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        let o5 = defaultArg o5 0m

        let o6 = defaultArg o6 0m

        let o7 = defaultArg o7 0m

        let o8 = defaultArg o8 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>
    //diskin<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> ikooooooo  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member diskin<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> (i0:decimal, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        let o5 = defaultArg o5 0m

        let o6 = defaultArg o6 0m

        let o7 = defaultArg o7 0m

        let o8 = defaultArg o8 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>
    //diskin2<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> Skooooooo  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member diskin2<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> (s0:string, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        let o5 = defaultArg o5 0m

        let o6 = defaultArg o6 0m

        let o7 = defaultArg o7 0m

        let o8 = defaultArg o8 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>
    //diskin2<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> ikooooooo  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member diskin2<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n> (i0:decimal, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        let o5 = defaultArg o5 0m

        let o6 = defaultArg o6 0m

        let o7 = defaultArg o7 0m

        let o8 = defaultArg o8 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>
    //dispfft xiiooooo 
    static member dispfft (x0:CSoundOpcode.ScalarOrArray, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        ()
    //display xioo 
    static member display (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        ()
    //distort akiqo a
    static member distort (a0:Vector<double>, k1:single, i2:decimal, ?q3:decimal, ?o4:decimal) =
        let q3 = defaultArg q3 10m
        let o4 = defaultArg o4 0m
        a0
    //distort1 akkkko a
    static member distort1 (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0
    //divz aak a
    static member divz (a0:Vector<double>, a1:Vector<double>, k2:single) =
        a0
    //divz akk a
    static member divz (a0:Vector<double>, k1:single, k2:single) =
        a0
    //divz kak a
    static member divz (k0:single, a1:Vector<double>, k2:single) =
        a1
    //divz iii i
    static member divz (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //divz kkk k
    static member divz (k0:single, k1:single, k2:single) =
        k0
    //doppler akkjj a
    static member doppler (a0:Vector<double>, k1:single, k2:single, ?j3:decimal, ?j4:decimal) =
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        a0
    //downsamp ao k
    static member downsamp (a0:Vector<double>, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        0.0f
    //dripwater kioooooo a
    static member dripwater (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //dumpk kSii 
    static member dumpk (k0:single, s1:string, i2:decimal, i3:decimal) =
        ()
    //dumpk kiii 
    static member dumpk (k0:single, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //dumpk2 kkSii 
    static member dumpk2 (k0:single, k1:single, s2:string, i3:decimal, i4:decimal) =
        ()
    //dumpk2 kkiii 
    static member dumpk2 (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //dumpk3 kkkSii 
    static member dumpk3 (k0:single, k1:single, k2:single, s3:string, i4:decimal, i5:decimal) =
        ()
    //dumpk3 kkkiii 
    static member dumpk3 (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, i5:decimal) =
        ()
    //dumpk4 kkkkSii 
    static member dumpk4 (k0:single, k1:single, k2:single, k3:single, s4:string, i5:decimal, i6:decimal) =
        ()
    //dumpk4 kkkkiii 
    static member dumpk4 (k0:single, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, i6:decimal) =
        ()
    //duserrnd<'A> k  -> (a | k)

    static member duserrnd<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //duserrnd i i
    static member duserrnd (i0:decimal) =
        i0
    //dust<'A> kk  -> (a | k)

    static member dust<'A> (k0:single, k1:single) =

        Unchecked.defaultof<'A>
    //dust2<'A> kk  -> (a | k)

    static member dust2<'A> (k0:single, k1:single) =

        Unchecked.defaultof<'A>
    //endin  
    static member endin () =
        ()
    //endop  
    static member endop () =
        ()
    //envlpx aiiiiiio a
    static member envlpx (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0
    //envlpx<'A> kiiiiiio  -> (a | k)

    static member envlpx<'A> (k0:single, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, ?o7:decimal) =

        let o7 = defaultArg o7 0m

        Unchecked.defaultof<'A>
    //envlpxr aiiiiioo a
    static member envlpxr (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        a0
    //envlpxr<'A> kiiiiioo  -> (a | k)

    static member envlpxr<'A> (k0:single, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal) =

        let o6 = defaultArg o6 0m

        let o7 = defaultArg o7 0m

        Unchecked.defaultof<'A>
    //ephasor xko aa
    static member ephasor (x0:CSoundOpcode.ScalarOrArray, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        vector [(double)0.0], vector [(double)0.0]
    //eqfil akkko a
    static member eqfil (a0:Vector<double>, k1:single, k2:single, k3:single, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //evalstr S i
    static member evalstr (s0:string) =
        0.0m
    //evalstr Sk k
    static member evalstr (s0:string, k1:single) =
        k1
    //event SSz 
    static member event (s0:string, s1:string, z2:single[]) =
        ()
    //event Skz 
    static member event (s0:string, k1:single, z2:single[]) =
        ()
    //event_i SSm 
    static member event_i (s0:string, s1:string, m2:Vector<double>) =
        ()
    //event_i Sim 
    static member event_i (s0:string, i1:decimal, m2:Vector<double>) =
        ()
    //exitnow o 
    static member exitnow (?o0:decimal) =
        let o0 = defaultArg o0 0m
        ()
    //exp a a
    static member exp (a0:Vector<double>) =
        a0
    //exp i i
    static member exp (i0:decimal) =
        i0
    //exp k k
    static member exp (k0:single) =
        k0
    //expcurve kk k
    static member expcurve (k0:single, k1:single) =
        k0
    //expon<'A> iii  -> (a | k)

    static member expon<'A> (i0:decimal, i1:decimal, i2:decimal) =

        Unchecked.defaultof<'A>
    //exprand<'A> k  -> (a | i | k)

    static member exprand<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //exprandi<'A> kxx  -> (a | i | k)

    static member exprandi<'A> (k0:single, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray) =

        Unchecked.defaultof<'A>
    //expseg<'A> iin  -> (a | k)

    static member expseg<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //expsega iin a
    static member expsega (i0:decimal, i1:decimal, n2:decimal[]) =
        vector [(double)0.0]
    //expsegb<'A> iin  -> (a | k)

    static member expsegb<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //expsegba iin a
    static member expsegba (i0:decimal, i1:decimal, n2:decimal[]) =
        vector [(double)0.0]
    //expsegr<'A> iin  -> (a | k)

    static member expsegr<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //fareylen k k
    static member fareylen (k0:single) =
        k0
    //fareyleni i i
    static member fareyleni (i0:decimal) =
        i0
    //fft k[] k[]
    static member fft (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //fftinv k[] k[]
    static member fftinv (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //ficlose S 
    static member ficlose (s0:string) =
        ()
    //ficlose i 
    static member ficlose (i0:decimal) =
        ()
    //filebit Sp i
    static member filebit (s0:string, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        p1
    //filebit ip i
    static member filebit (i0:decimal, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        i0
    //filelen Sp i
    static member filelen (s0:string, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        p1
    //filelen ip i
    static member filelen (i0:decimal, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        i0
    //filenchnls Sp i
    static member filenchnls (s0:string, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        p1
    //filenchnls ip i
    static member filenchnls (i0:decimal, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        i0
    //filepeak So i
    static member filepeak (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        o1
    //filepeak io i
    static member filepeak (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //filesr Sp i
    static member filesr (s0:string, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        p1
    //filesr ip i
    static member filesr (i0:decimal, ?p1:decimal) =
        let p1 = defaultArg p1 1m
        i0
    //filevalid S i
    static member filevalid (s0:string) =
        0.0m
    //filevalid i i
    static member filevalid (i0:decimal) =
        i0
    //fillarray W S[]
    static member fillarray () =
        0.0, [|0.0m|], 0.0
    //fillarray<'A,'B,'C> m  -> (i[] | k[])

    static member fillarray<'A,'B,'C> (m0:Vector<double>) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>
    //filter2 aiim a
    static member filter2 (a0:Vector<double>, i1:decimal, i2:decimal, m3:Vector<double>) =
        a0
    //filter2 kiim k
    static member filter2 (k0:single, i1:decimal, i2:decimal, m3:Vector<double>) =
        k0
    //fin Siia[] 
    static member fin (s0:string, i1:decimal, i2:decimal, a3:Vector<double>, M4:decimal[]) =
        ()
    //fin Siiy 
    static member fin (s0:string, i1:decimal, i2:decimal, y3:Vector<double>[][]) =
        ()
    //fin iiiy 
    static member fin (i0:decimal, i1:decimal, i2:decimal, y3:Vector<double>[][]) =
        ()
    //fini Siim 
    static member fini (s0:string, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //fini iiim 
    static member fini (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //fink Siiz 
    static member fink (s0:string, i1:decimal, i2:decimal, z3:single[]) =
        ()
    //fink iiiz 
    static member fink (i0:decimal, i1:decimal, i2:decimal, z3:single[]) =
        ()
    //fiopen Si i
    static member fiopen (s0:string, i1:decimal) =
        i1
    //fiopen ii i
    static member fiopen (i0:decimal, i1:decimal) =
        i0
    //flanger aakv a
    static member flanger (a0:Vector<double>, a1:Vector<double>, k2:single, ?v3:decimal) =
        let v3 = defaultArg v3 0.5m
        a0
    //FLbox Siiiiiii i
    static member FLbox (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal) =
        i1
    //FLbutBank iiiiiiiz ki
    static member FLbutBank (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, z7:single[]) =
        0.0f, i0
    //FLbutton Siiiiiiiz ki
    static member FLbutton (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, z8:single[]) =
        0.0f, i1
    //FLcloseButton Siiii i
    static member FLcloseButton (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        i1
    //FLcolor jjjjjj 
    static member FLcolor (?j0:decimal, ?j1:decimal, ?j2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal) =
        let j0 = defaultArg j0 -0.1m
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        ()
    //FLcolor2 jjj 
    static member FLcolor2 (?j0:decimal, ?j1:decimal, ?j2:decimal) =
        let j0 = defaultArg j0 -0.1m
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        ()
    //FLcount Siiiiiiiiiz ki
    static member FLcount (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, z10:single[]) =
        0.0f, i1
    //FLexecButton Siiii i
    static member FLexecButton (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        i1
    //FLgetsnap io i
    static member FLgetsnap (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //FLgroup Siiiij 
    static member FLgroup (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?j5:decimal) =
        let j5 = defaultArg j5 -0.1m
        ()
    //FLgroup_end  
    static member FLgroup_end () =
        ()
    //FLgroupEnd  
    static member FLgroupEnd () =
        ()
    //FLhide i 
    static member FLhide (i0:decimal) =
        ()
    //FLhvsBox iiiiiio i
    static member FLhvsBox (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        i0
    //FLhvsBoxSetValue kki 
    static member FLhvsBoxSetValue (k0:single, k1:single, i2:decimal) =
        ()
    //FLjoy Siiiijjjjjjjj kkii
    static member FLjoy (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?j5:decimal, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal, ?j11:decimal, ?j12:decimal) =
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        let j11 = defaultArg j11 -0.1m
        let j12 = defaultArg j12 -0.1m
        0.0f, 0.0f, i1, i1
    //FLkeyIn o k
    static member FLkeyIn (?o0:decimal) =
        let o0 = defaultArg o0 0m
        0.0f
    //FLknob Siijjjjjjo ki
    static member FLknob (s0:string, i1:decimal, i2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?o9:decimal) =
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let o9 = defaultArg o9 0m
        0.0f, i1
    //FLlabel ojojjj 
    static member FLlabel (?o0:decimal, ?j1:decimal, ?o2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal) =
        let o0 = defaultArg o0 0m
        let j1 = defaultArg j1 -0.1m
        let o2 = defaultArg o2 0m
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        ()
    //FLloadsnap So 
    static member FLloadsnap (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //FLmouse o kkkkk
    static member FLmouse (?o0:decimal) =
        let o0 = defaultArg o0 0m
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //flooper kkiiii a
    static member flooper (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal) =
        vector [(double)0.0]
    //flooper2 kkkkkiooooO a
    static member flooper2 (k0:single, k1:single, k2:single, k3:single, k4:single, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?O10:single) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let O10 = defaultArg O10 0.f
        vector [(double)0.0]
    //floor a a
    static member floor (a0:Vector<double>) =
        a0
    //floor i i
    static member floor (i0:decimal) =
        i0
    //floor k k
    static member floor (k0:single) =
        k0
    //FLpack iiiiooo 
    static member FLpack (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        ()
    //FLpack_end  
    static member FLpack_end () =
        ()
    //FLpackEnd  
    static member FLpackEnd () =
        ()
    //FLpanel Sjjjoooo 
    static member FLpanel (s0:string, ?j1:decimal, ?j2:decimal, ?j3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        let j3 = defaultArg j3 -0.1m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        ()
    //FLpanel_end  
    static member FLpanel_end () =
        ()
    //FLpanelEnd  
    static member FLpanelEnd () =
        ()
    //FLprintk iki 
    static member FLprintk (i0:decimal, k1:single, i2:decimal) =
        ()
    //FLprintk2 ki 
    static member FLprintk2 (k0:single, i1:decimal) =
        ()
    //FLroller Siijjjjjjjj ki
    static member FLroller (s0:string, i1:decimal, i2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal) =
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        0.0f, i1
    //FLrun  
    static member FLrun () =
        ()
    //FLsavesnap So 
    static member FLsavesnap (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //FLscroll iiii 
    static member FLscroll (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //FLscroll_end  
    static member FLscroll_end () =
        ()
    //FLscrollEnd  
    static member FLscrollEnd () =
        ()
    //FLsetAlign ii 
    static member FLsetAlign (i0:decimal, i1:decimal) =
        ()
    //FLsetBox ii 
    static member FLsetBox (i0:decimal, i1:decimal) =
        ()
    //FLsetColor iiii 
    static member FLsetColor (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //FLsetColor2 iiii 
    static member FLsetColor2 (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //FLsetFont ii 
    static member FLsetFont (i0:decimal, i1:decimal) =
        ()
    //FLsetPosition iii 
    static member FLsetPosition (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //FLsetSize iii 
    static member FLsetSize (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //FLsetsnap ioo ii
    static member FLsetsnap (i0:decimal, ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        i0, i0
    //FLsetSnapGroup i 
    static member FLsetSnapGroup (i0:decimal) =
        ()
    //FLsetText Ti 
    static member FLsetText (T0:'a, i1:decimal) =
        ()
    //FLsetTextColor iiii 
    static member FLsetTextColor (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //FLsetTextSize ii 
    static member FLsetTextSize (i0:decimal, i1:decimal) =
        ()
    //FLsetTextType ii 
    static member FLsetTextType (i0:decimal, i1:decimal) =
        ()
    //FLsetVal kki 
    static member FLsetVal (k0:single, k1:single, i2:decimal) =
        ()
    //FLsetVal_i ii 
    static member FLsetVal_i (i0:decimal, i1:decimal) =
        ()
    //FLsetVali ii 
    static member FLsetVali (i0:decimal, i1:decimal) =
        ()
    //FLshow i 
    static member FLshow (i0:decimal) =
        ()
    //FLslidBnk Siooooooooo 
    static member FLslidBnk (s0:string, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        ()
    //FLslidBnk iiooooooooo 
    static member FLslidBnk (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        ()
    //FLslidBnk2 Iiiiooooo 
    static member FLslidBnk2 () =
        ()
    //FLslidBnk2 Siiiooooo 
    static member FLslidBnk2 (s0:string, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        ()
    //FLslidBnk2Set iiooo 
    static member FLslidBnk2Set (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //FLslidBnk2Setk kiiooo 
    static member FLslidBnk2Setk (k0:single, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //FLslidBnkGetHandle  i
    static member FLslidBnkGetHandle () =
        0.0m
    //FLslidBnkSet iiooo 
    static member FLslidBnkSet (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //FLslidBnkSetk kiiooo 
    static member FLslidBnkSetk (k0:single, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //FLslider Siijjjjjjj ki
    static member FLslider (s0:string, i1:decimal, i2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal) =
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        0.0f, i1
    //FLtabs iiii 
    static member FLtabs (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //FLtabs_end  
    static member FLtabs_end () =
        ()
    //FLtabsEnd  
    static member FLtabsEnd () =
        ()
    //FLtext Siijjjjjj ki
    static member FLtext (s0:string, i1:decimal, i2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal, ?j6:decimal, ?j7:decimal, ?j8:decimal) =
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        0.0f, i1
    //fluidAllOut  aa
    static member fluidAllOut () =
        vector [(double)0.0], vector [(double)0.0]
    //fluidCCi iiii 
    static member fluidCCi (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //fluidCCk ikkk 
    static member fluidCCk (i0:decimal, k1:single, k2:single, k3:single) =
        ()
    //fluidControl ikkkk 
    static member fluidControl (i0:decimal, k1:single, k2:single, k3:single, k4:single) =
        ()
    //fluidEngine ppoo i
    static member fluidEngine (?p0:decimal, ?p1:decimal, ?o2:decimal, ?o3:decimal) =
        let p0 = defaultArg p0 1m
        let p1 = defaultArg p1 1m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        p0
    //fluidLoad Tio i
    static member fluidLoad (T0:'a, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        i1
    //fluidNote iiii 
    static member fluidNote (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //fluidOut i aa
    static member fluidOut (i0:decimal) =
        vector [(double)0.0], vector [(double)0.0]
    //fluidProgramSelect iiiii 
    static member fluidProgramSelect (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //fluidSetInterpMethod iii 
    static member fluidSetInterpMethod (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //FLupdate  
    static member FLupdate () =
        ()
    //FLvalue Sjjjj i
    static member FLvalue (s0:string, ?j1:decimal, ?j2:decimal, ?j3:decimal, ?j4:decimal) =
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        j1
    //FLvkeybd Siiii 
    static member FLvkeybd (s0:string, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //FLvslidBnk Siooooooooo 
    static member FLvslidBnk (s0:string, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        ()
    //FLvslidBnk iiooooooooo 
    static member FLvslidBnk (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        ()
    //FLvslidBnk2 Siiiooooo 
    static member FLvslidBnk2 (s0:string, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        ()
    //FLvslidBnk2 iiiiooooo 
    static member FLvslidBnk2 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        ()
    //FLxyin iiiiiiiioooo kkk
    static member FLxyin (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal, ?o11:decimal) =
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        0.0f, 0.0f, 0.0f
    //fmb3 kkkkkkjjjjj a
    static member fmb3 (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal) =
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        vector [(double)0.0]
    //fmbell kkkkkkjjjjjo a
    static member fmbell (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal, ?o11:decimal) =
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        let o11 = defaultArg o11 0m
        vector [(double)0.0]
    //fmmetal kkkkkkiiiii a
    static member fmmetal (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal) =
        vector [(double)0.0]
    //fmpercfl kkkkkkjjjjj a
    static member fmpercfl (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal) =
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        vector [(double)0.0]
    //fmrhode kkkkkkiiiii a
    static member fmrhode (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal) =
        vector [(double)0.0]
    //fmvoice kkkkkkjjjjj a
    static member fmvoice (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, ?j6:decimal, ?j7:decimal, ?j8:decimal, ?j9:decimal, ?j10:decimal) =
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let j8 = defaultArg j8 -0.1m
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        vector [(double)0.0]
    //fmwurlie kkkkkkiiiii a
    static member fmwurlie (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal) =
        vector [(double)0.0]
    //fof xxxkkkkkiiiiooo a
    static member fof (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, k3:single, k4:single, k5:single, k6:single, k7:single, i8:decimal, i9:decimal, i10:decimal, i11:decimal, ?o12:decimal, ?o13:decimal, ?o14:decimal) =
        let o12 = defaultArg o12 0m
        let o13 = defaultArg o13 0m
        let o14 = defaultArg o14 0m
        vector [(double)0.0]
    //fof2 xxxkkkkkiiiikko a
    static member fof2 (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, k3:single, k4:single, k5:single, k6:single, k7:single, i8:decimal, i9:decimal, i10:decimal, i11:decimal, k12:single, k13:single, ?o14:decimal) =
        let o14 = defaultArg o14 0m
        vector [(double)0.0]
    //fofilter axxxp a
    static member fofilter (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        a0
    //fog xxxakkkkkiiiiooo a
    static member fog (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, a3:Vector<double>, k4:single, k5:single, k6:single, k7:single, k8:single, i9:decimal, i10:decimal, i11:decimal, i12:decimal, ?o13:decimal, ?o14:decimal, ?o15:decimal) =
        let o13 = defaultArg o13 0m
        let o14 = defaultArg o14 0m
        let o15 = defaultArg o15 0m
        a3
    //fold ak a
    static member fold (a0:Vector<double>, k1:single) =
        a0
    //follow ai a
    static member follow (a0:Vector<double>, i1:decimal) =
        a0
    //follow2 akk a
    static member follow2 (a0:Vector<double>, k1:single, k2:single) =
        a0
    //foscil xkxxkjo a
    static member foscil (x0:CSoundOpcode.ScalarOrArray, k1:single, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, ?j5:decimal, ?o6:decimal) =
        let j5 = defaultArg j5 -0.1m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //foscili xkxxkjo a
    static member foscili (x0:CSoundOpcode.ScalarOrArray, k1:single, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, k4:single, ?j5:decimal, ?o6:decimal) =
        let j5 = defaultArg j5 -0.1m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //fout Sia[] 
    static member fout (s0:string, i1:decimal, a2:Vector<double>, M3:decimal[]) =
        ()
    //fout Siy 
    static member fout (s0:string, i1:decimal, y2:Vector<double>[][]) =
        ()
    //fout iiy 
    static member fout (i0:decimal, i1:decimal, y2:Vector<double>[][]) =
        ()
    //fouti iiim 
    static member fouti (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //foutir iiim 
    static member foutir (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //foutk Siz 
    static member foutk (s0:string, i1:decimal, z2:single[]) =
        ()
    //foutk iiz 
    static member foutk (i0:decimal, i1:decimal, z2:single[]) =
        ()
    //fprintks SSM 
    static member fprintks (s0:string, s1:string, M2:decimal[]) =
        ()
    //fprintks iSM 
    static member fprintks (i0:decimal, s1:string, M2:decimal[]) =
        ()
    //fprints SSM 
    static member fprints (s0:string, s1:string, M2:decimal[]) =
        ()
    //fprints iSM 
    static member fprints (i0:decimal, s1:string, M2:decimal[]) =
        ()
    //frac a a
    static member frac (a0:Vector<double>) =
        a0
    //frac i i
    static member frac (i0:decimal) =
        i0
    //frac k k
    static member frac (k0:single) =
        k0
    //fractalnoise kk a
    static member fractalnoise (k0:single, k1:single) =
        vector [(double)0.0]
    //freeverb aakkjo aa
    static member freeverb (a0:Vector<double>, a1:Vector<double>, k2:single, k3:single, ?j4:decimal, ?o5:decimal) =
        let j4 = defaultArg j4 -0.1m
        let o5 = defaultArg o5 0m
        a0, a0
    //ftchnls i i
    static member ftchnls (i0:decimal) =
        i0
    //ftconv aiiooo mmmmmmmm
    static member ftconv (a0:Vector<double>, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0, a0, a0, a0, a0, a0, a0, a0
    //ftcps i i
    static member ftcps (i0:decimal) =
        i0
    //ftfree ii 
    static member ftfree (i0:decimal, i1:decimal) =
        ()
    //ftgen iiiSSm i
    static member ftgen (i0:decimal, i1:decimal, i2:decimal, s3:string, s4:string, m5:Vector<double>) =
        i0
    //ftgen iiiSim i
    static member ftgen (i0:decimal, i1:decimal, i2:decimal, s3:string, i4:decimal, m5:Vector<double>) =
        i0
    //ftgen iiiiSm i
    static member ftgen (i0:decimal, i1:decimal, i2:decimal, i3:decimal, s4:string, m5:Vector<double>) =
        i0
    //ftgen iiiiim i
    static member ftgen (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, m5:Vector<double>) =
        i0
    //ftgenonce iiiiSm i
    static member ftgenonce (i0:decimal, i1:decimal, i2:decimal, i3:decimal, s4:string, m5:Vector<double>) =
        i0
    //ftgenonce iiiiim i
    static member ftgenonce (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, m5:Vector<double>) =
        i0
    //ftgentmp iiiiSm i
    static member ftgentmp (i0:decimal, i1:decimal, i2:decimal, i3:decimal, s4:string, m5:Vector<double>) =
        i0
    //ftgentmp iiiiim i
    static member ftgentmp (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, m5:Vector<double>) =
        i0
    //ftlen i i
    static member ftlen (i0:decimal) =
        i0
    //ftload Sim 
    static member ftload (s0:string, i1:decimal, m2:Vector<double>) =
        ()
    //ftload iim 
    static member ftload (i0:decimal, i1:decimal, m2:Vector<double>) =
        ()
    //ftloadk Skim 
    static member ftloadk (s0:string, k1:single, i2:decimal, m3:Vector<double>) =
        ()
    //ftloadk ikim 
    static member ftloadk (i0:decimal, k1:single, i2:decimal, m3:Vector<double>) =
        ()
    //ftlptim i i
    static member ftlptim (i0:decimal) =
        i0
    //ftmorf kii 
    static member ftmorf (k0:single, i1:decimal, i2:decimal) =
        ()
    //ftresize kk k
    static member ftresize (k0:single, k1:single) =
        k0
    //ftresizei ii i
    static member ftresizei (i0:decimal, i1:decimal) =
        i0
    //ftsave Sim 
    static member ftsave (s0:string, i1:decimal, m2:Vector<double>) =
        ()
    //ftsave iim 
    static member ftsave (i0:decimal, i1:decimal, m2:Vector<double>) =
        ()
    //ftsavek Skim 
    static member ftsavek (s0:string, k1:single, i2:decimal, m3:Vector<double>) =
        ()
    //ftsavek ikim 
    static member ftsavek (i0:decimal, k1:single, i2:decimal, m3:Vector<double>) =
        ()
    //ftsr i i
    static member ftsr (i0:decimal) =
        i0
    //gain akqo a
    static member gain (a0:Vector<double>, k1:single, ?q2:decimal, ?o3:decimal) =
        let q2 = defaultArg q2 10m
        let o3 = defaultArg o3 0m
        a0
    //gainslider k k
    static member gainslider (k0:single) =
        k0
    //gauss<'A> k  -> (a | i | k)

    static member gauss<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //gaussi<'A> kxx  -> (a | i | k)

    static member gaussi<'A> (k0:single, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray) =

        Unchecked.defaultof<'A>
    //gausstrig<'A> kkko  -> (a | k)

    static member gausstrig<'A> (k0:single, k1:single, k2:single, ?o3:decimal) =

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //gbuzz xxkkkio a
    static member gbuzz (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, k2:single, k3:single, k4:single, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //genarray iip i[]
    static member genarray (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        i0, [|0.0m|], 0.0
    //genarray kkp k[]
    static member genarray (k0:single, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        k0, [|0.0m|], 0.0
    //genarray_i iip k[]
    static member genarray_i (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        0.0f, [|0.0m|], 0.0
    //gendy<'A> kkkkkkkkkoO  -> (a | k)

    static member gendy<'A> (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single, ?o9:decimal, ?O10:single) =

        let o9 = defaultArg o9 0m

        let O10 = defaultArg O10 0.f

        Unchecked.defaultof<'A>
    //gendyc<'A> kkkkkkkkkoO  -> (a | k)

    static member gendyc<'A> (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single, ?o9:decimal, ?O10:single) =

        let o9 = defaultArg o9 0m

        let O10 = defaultArg O10 0.f

        Unchecked.defaultof<'A>
    //gendyx<'A> kkkkkkkkkkkoO  -> (a | k)

    static member gendyx<'A> (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single, k9:single, k10:single, ?o11:decimal, ?O12:single) =

        let o11 = defaultArg o11 0m

        let O12 = defaultArg O12 0.f

        Unchecked.defaultof<'A>
    //getcfg i S
    static member getcfg (i0:decimal) =
        0.0
    //getcol k[]k k[]
    static member getcol (k0:single, M1:decimal[], k2:single) =
        k0, M1, 0.0
    //getrow k[]k k[]
    static member getrow (k0:single, M1:decimal[], k2:single) =
        k0, M1, 0.0
    //gogobel kkiiikki a
    static member gogobel (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, k5:single, k6:single, i7:decimal) =
        vector [(double)0.0]
    //goto l 
    static member goto (l0:_) =
        ()
    //grain xxxkkkiiio a
    static member grain (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, i8:decimal, ?o9:decimal) =
        let o9 = defaultArg o9 0m
        vector [(double)0.0]
    //grain2 kkkikiooo a
    static member grain2 (k0:single, k1:single, k2:single, i3:decimal, k4:single, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0]
    //grain3 kkkkkkikikkoo a
    static member grain3 (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, k7:single, i8:decimal, k9:single, k10:single, ?o11:decimal, ?o12:decimal) =
        let o11 = defaultArg o11 0m
        let o12 = defaultArg o12 0m
        vector [(double)0.0]
    //granule xiiiiiiiiikikiiivppppo a
    static member granule (x0:CSoundOpcode.ScalarOrArray, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, k10:single, i11:decimal, k12:single, i13:decimal, i14:decimal, i15:decimal, ?v16:decimal, ?p17:decimal, ?p18:decimal, ?p19:decimal, ?p20:decimal, ?o21:decimal) =
        let v16 = defaultArg v16 0.5m
        let p17 = defaultArg p17 1m
        let p18 = defaultArg p18 1m
        let p19 = defaultArg p19 1m
        let p20 = defaultArg p20 1m
        let o21 = defaultArg o21 0m
        vector [(double)0.0]
    //guiro kiooooo a
    static member guiro (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //harmon akkkkiii a
    static member harmon (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, i5:decimal, i6:decimal, i7:decimal) =
        a0
    //harmon2 akkkiip a
    static member harmon2 (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, ?p6:decimal) =
        let p6 = defaultArg p6 1m
        a0
    //harmon3 akkkkiip a
    static member harmon3 (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, i5:decimal, i6:decimal, ?p7:decimal) =
        let p7 = defaultArg p7 1m
        a0
    //harmon4 akkkkkiip a
    static member harmon4 (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, ?p8:decimal) =
        let p8 = defaultArg p8 1m
        a0
    //hilbert a aa
    static member hilbert (a0:Vector<double>) =
        a0, a0
    //hrtfearly axxxxxxSSioopoOoooooooooooooooooo aaiii
    static member hrtfearly (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, x4:CSoundOpcode.ScalarOrArray, x5:CSoundOpcode.ScalarOrArray, x6:CSoundOpcode.ScalarOrArray, s7:string, s8:string, i9:decimal, ?o10:decimal, ?o11:decimal, ?p12:decimal, ?o13:decimal, ?O14:single, ?o15:decimal, ?o16:decimal, ?o17:decimal, ?o18:decimal, ?o19:decimal, ?o20:decimal, ?o21:decimal, ?o22:decimal, ?o23:decimal, ?o24:decimal, ?o25:decimal, ?o26:decimal, ?o27:decimal, ?o28:decimal, ?o29:decimal, ?o30:decimal, ?o31:decimal, ?o32:decimal) =
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        let p12 = defaultArg p12 1m
        let o13 = defaultArg o13 0m
        let O14 = defaultArg O14 0.f
        let o15 = defaultArg o15 0m
        let o16 = defaultArg o16 0m
        let o17 = defaultArg o17 0m
        let o18 = defaultArg o18 0m
        let o19 = defaultArg o19 0m
        let o20 = defaultArg o20 0m
        let o21 = defaultArg o21 0m
        let o22 = defaultArg o22 0m
        let o23 = defaultArg o23 0m
        let o24 = defaultArg o24 0m
        let o25 = defaultArg o25 0m
        let o26 = defaultArg o26 0m
        let o27 = defaultArg o27 0m
        let o28 = defaultArg o28 0m
        let o29 = defaultArg o29 0m
        let o30 = defaultArg o30 0m
        let o31 = defaultArg o31 0m
        let o32 = defaultArg o32 0m
        a0, a0, i9, i9, i9
    //hrtfer akkS aa
    static member hrtfer (a0:Vector<double>, k1:single, k2:single, s3:string) =
        a0, a0
    //hrtfmove akkSSooo aa
    static member hrtfmove (a0:Vector<double>, k1:single, k2:single, s3:string, s4:string, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        a0, a0
    //hrtfmove2 akkSSooo aa
    static member hrtfmove2 (a0:Vector<double>, k1:single, k2:single, s3:string, s4:string, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        a0, a0
    //hrtfreverb aiiSSoop aai
    static member hrtfreverb (a0:Vector<double>, i1:decimal, i2:decimal, s3:string, s4:string, ?o5:decimal, ?o6:decimal, ?p7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let p7 = defaultArg p7 1m
        a0, a0, i1
    //hrtfstat aiiSSoo aa
    static member hrtfstat (a0:Vector<double>, i1:decimal, i2:decimal, s3:string, s4:string, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        a0, a0
    //hsboscil kkkiiioo a
    static member hsboscil (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //hvs1 kiiiiio 
    static member hvs1 (k0:single, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, ?o6:decimal) =
        let o6 = defaultArg o6 0m
        ()
    //hvs2 kkiiiiiio 
    static member hvs2 (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, ?o8:decimal) =
        let o8 = defaultArg o8 0m
        ()
    //hvs3 kkkiiiiiiio 
    static member hvs3 (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, ?o10:decimal) =
        let o10 = defaultArg o10 0m
        ()
    //i i i
    static member i (i0:decimal) =
        i0
    //i k i
    static member i (k0:single) =
        0.0m
    //iceps k[] k[]
    static member iceps (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //igoto l 
    static member igoto (l0:_) =
        ()
    //ihold  
    static member ihold () =
        ()
    //imagecreate ii i
    static member imagecreate (i0:decimal, i1:decimal) =
        i0
    //imagefree i 
    static member imagefree (i0:decimal) =
        ()
    //imagegetpixel ixx sss
    static member imagegetpixel (i0:decimal, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray) =
        ()
    //imageload S i
    static member imageload (s0:string) =
        0.0m
    //imagesave iS 
    static member imagesave (i0:decimal, s1:string) =
        ()
    //imagesetpixel ixxxxx 
    static member imagesetpixel (i0:decimal, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, x4:CSoundOpcode.ScalarOrArray, x5:CSoundOpcode.ScalarOrArray) =
        ()
    //imagesize i ii
    static member imagesize (i0:decimal) =
        i0, i0
    //in32  aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    static member in32 () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //inch k a
    static member inch (k0:single) =
        vector [(double)0.0]
    //inch z mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member inch (z0:single[]) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //inh  aaaaaa
    static member inh () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //init<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X> m  -> (.[] | IIIIIIIIIIIIIIIIIIIIIIII | mmmmmmmmmmmmmmmmmmmmmmmm | zzzzzzzzzzzzzzzzzzzzzzzz)

    static member init<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X> (m0:Vector<double>) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>
    //init S S
    static member init (s0:string) =
        s0
    //init i S
    static member init (i0:decimal) =
        0.0
    //init f f
    static member init (S0:_[]) =
        S0
    //initc14 iiii 
    static member initc14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //initc21 iiiii 
    static member initc21 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //initc7 iii 
    static member initc7 (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //inleta S a
    static member inleta (s0:string) =
        vector [(double)0.0]
    //inletf S f
    static member inletf (s0:string) =
        0.0
    //inletk S k
    static member inletk (s0:string) =
        0.0f
    //inletkid SS k
    static member inletkid (s0:string, s1:string) =
        0.0f
    //inletv S a[]
    static member inletv (s0:string) =
        vector [(double)0.0], [|0.0m|], 0.0
    //ino  aaaaaaaa
    static member ino () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //inq  aaaa
    static member inq () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //inrg ky 
    static member inrg (k0:single, y1:Vector<double>[][]) =
        ()
    //ins  aa
    static member ins () =
        vector [(double)0.0], vector [(double)0.0]
    //insglobal Sm 
    static member insglobal (s0:string, m1:Vector<double>) =
        ()
    //insremot SSm 
    static member insremot (s0:string, s1:string, m2:Vector<double>) =
        ()
    //instr  
    static member instr () =
        ()
    //int a a
    static member int (a0:Vector<double>) =
        a0
    //int i i
    static member int (i0:decimal) =
        i0
    //int k k
    static member int (k0:single) =
        k0
    //integ<'A> xo  -> (a | k)

    static member integ<'A> (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =

        let o1 = defaultArg o1 0m

        Unchecked.defaultof<'A>
    //interp koo a
    static member interp (k0:single, ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        vector [(double)0.0]
    //invalue<'A> S  -> (S | i | k)

    static member invalue<'A> (s0:string) =

        Unchecked.defaultof<'A>
    //invalue<'A> i  -> (S | i | k)

    static member invalue<'A> (i0:decimal) =

        Unchecked.defaultof<'A>
    //inx  aaaaaaaaaaaaaaaa
    static member inx () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //inz k 
    static member inz (k0:single) =
        ()
    //jitter kkk k
    static member jitter (k0:single, k1:single, k2:single) =
        k0
    //jitter2 kkkkkkk k
    static member jitter2 (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single) =
        k0
    //jspline<'A> xkk  -> (a | k)

    static member jspline<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, k2:single) =

        Unchecked.defaultof<'A>
    //k ao k
    static member k (a0:Vector<double>, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        0.0f
    //k i k
    static member k (i0:decimal) =
        0.0f
    //kgoto l 
    static member kgoto (l0:_) =
        ()
    //ktableseg iin 
    static member ktableseg (i0:decimal, i1:decimal, n2:decimal[]) =
        ()
    //la_i_add_mc ii i
    static member la_i_add_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_add_mr ii i
    static member la_i_add_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_add_vc ii i
    static member la_i_add_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_add_vr ii i
    static member la_i_add_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_assign_mc i i
    static member la_i_assign_mc (i0:decimal) =
        i0
    //la_i_assign_mr i i
    static member la_i_assign_mr (i0:decimal) =
        i0
    //la_i_assign_t i i
    static member la_i_assign_t (i0:decimal) =
        i0
    //la_i_assign_vc i i
    static member la_i_assign_vc (i0:decimal) =
        i0
    //la_i_assign_vr i i
    static member la_i_assign_vr (i0:decimal) =
        i0
    //la_i_conjugate_mc i i
    static member la_i_conjugate_mc (i0:decimal) =
        i0
    //la_i_conjugate_mr i i
    static member la_i_conjugate_mr (i0:decimal) =
        i0
    //la_i_conjugate_vc i i
    static member la_i_conjugate_vc (i0:decimal) =
        i0
    //la_i_conjugate_vr i i
    static member la_i_conjugate_vr (i0:decimal) =
        i0
    //la_i_distance_vc ii i
    static member la_i_distance_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_distance_vr ii i
    static member la_i_distance_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_divide_mc ii i
    static member la_i_divide_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_divide_mr ii i
    static member la_i_divide_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_divide_vc ii i
    static member la_i_divide_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_divide_vr ii i
    static member la_i_divide_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_dot_mc ii i
    static member la_i_dot_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_dot_mc_vc ii i
    static member la_i_dot_mc_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_dot_mr ii i
    static member la_i_dot_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_dot_mr_vr ii i
    static member la_i_dot_mr_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_dot_vc ii ii
    static member la_i_dot_vc (i0:decimal, i1:decimal) =
        i0, i0
    //la_i_dot_vr ii i
    static member la_i_dot_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_get_mc iii ii
    static member la_i_get_mc (i0:decimal, i1:decimal, i2:decimal) =
        i0, i0
    //la_i_get_mr iii i
    static member la_i_get_mr (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //la_i_get_vc ii ii
    static member la_i_get_vc (i0:decimal, i1:decimal) =
        i0, i0
    //la_i_get_vr ii i
    static member la_i_get_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_invert_mc i iii
    static member la_i_invert_mc (i0:decimal) =
        i0, i0, i0
    //la_i_invert_mr i ii
    static member la_i_invert_mr (i0:decimal) =
        i0, i0
    //la_i_lower_solve_mc io i
    static member la_i_lower_solve_mc (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //la_i_lower_solve_mr io i
    static member la_i_lower_solve_mr (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //la_i_lu_det_mc i ii
    static member la_i_lu_det_mc (i0:decimal) =
        i0, i0
    //la_i_lu_det_mr i i
    static member la_i_lu_det_mr (i0:decimal) =
        i0
    //la_i_lu_factor_mc i iii
    static member la_i_lu_factor_mc (i0:decimal) =
        i0, i0, i0
    //la_i_lu_factor_mr i iii
    static member la_i_lu_factor_mr (i0:decimal) =
        i0, i0, i0
    //la_i_lu_solve_mc ii i
    static member la_i_lu_solve_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_lu_solve_mr ii i
    static member la_i_lu_solve_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_mc_create iioo i
    static member la_i_mc_create (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        i0
    //la_i_mc_set iiii i
    static member la_i_mc_set (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        i0
    //la_i_mr_create iio i
    static member la_i_mr_create (i0:decimal, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        i0
    //la_i_mr_set iii i
    static member la_i_mr_set (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //la_i_multiply_mc ii i
    static member la_i_multiply_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_multiply_mr ii i
    static member la_i_multiply_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_multiply_vc ii i
    static member la_i_multiply_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_multiply_vr ii i
    static member la_i_multiply_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_norm_euclid_mc i i
    static member la_i_norm_euclid_mc (i0:decimal) =
        i0
    //la_i_norm_euclid_mr i i
    static member la_i_norm_euclid_mr (i0:decimal) =
        i0
    //la_i_norm_euclid_vc i i
    static member la_i_norm_euclid_vc (i0:decimal) =
        i0
    //la_i_norm_euclid_vr i i
    static member la_i_norm_euclid_vr (i0:decimal) =
        i0
    //la_i_norm_inf_mc i i
    static member la_i_norm_inf_mc (i0:decimal) =
        i0
    //la_i_norm_inf_mr i i
    static member la_i_norm_inf_mr (i0:decimal) =
        i0
    //la_i_norm_inf_vc i i
    static member la_i_norm_inf_vc (i0:decimal) =
        i0
    //la_i_norm_inf_vr i i
    static member la_i_norm_inf_vr (i0:decimal) =
        i0
    //la_i_norm_max_mc i i
    static member la_i_norm_max_mc (i0:decimal) =
        i0
    //la_i_norm_max_mr i i
    static member la_i_norm_max_mr (i0:decimal) =
        i0
    //la_i_norm1_mc i i
    static member la_i_norm1_mc (i0:decimal) =
        i0
    //la_i_norm1_mr i i
    static member la_i_norm1_mr (i0:decimal) =
        i0
    //la_i_norm1_vc i i
    static member la_i_norm1_vc (i0:decimal) =
        i0
    //la_i_norm1_vr i i
    static member la_i_norm1_vr (i0:decimal) =
        i0
    //la_i_print_mc i 
    static member la_i_print_mc (i0:decimal) =
        ()
    //la_i_print_mr i 
    static member la_i_print_mr (i0:decimal) =
        ()
    //la_i_print_vc i 
    static member la_i_print_vc (i0:decimal) =
        ()
    //la_i_print_vr i 
    static member la_i_print_vr (i0:decimal) =
        ()
    //la_i_qr_eigen_mc ii i
    static member la_i_qr_eigen_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_qr_eigen_mr ii i
    static member la_i_qr_eigen_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_qr_factor_mc i ii
    static member la_i_qr_factor_mc (i0:decimal) =
        i0, i0
    //la_i_qr_factor_mr i ii
    static member la_i_qr_factor_mr (i0:decimal) =
        i0, i0
    //la_i_qr_sym_eigen_mc ii ii
    static member la_i_qr_sym_eigen_mc (i0:decimal, i1:decimal) =
        i0, i0
    //la_i_qr_sym_eigen_mr ii ii
    static member la_i_qr_sym_eigen_mr (i0:decimal, i1:decimal) =
        i0, i0
    //la_i_random_mc p i
    static member la_i_random_mc (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_i_random_mr p i
    static member la_i_random_mr (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_i_random_vc p i
    static member la_i_random_vc (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_i_random_vr p i
    static member la_i_random_vr (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_i_size_mc i ii
    static member la_i_size_mc (i0:decimal) =
        i0, i0
    //la_i_size_mr i ii
    static member la_i_size_mr (i0:decimal) =
        i0, i0
    //la_i_size_vc i i
    static member la_i_size_vc (i0:decimal) =
        i0
    //la_i_size_vr i i
    static member la_i_size_vr (i0:decimal) =
        i0
    //la_i_subtract_mc ii i
    static member la_i_subtract_mc (i0:decimal, i1:decimal) =
        i0
    //la_i_subtract_mr ii i
    static member la_i_subtract_mr (i0:decimal, i1:decimal) =
        i0
    //la_i_subtract_vc ii i
    static member la_i_subtract_vc (i0:decimal, i1:decimal) =
        i0
    //la_i_subtract_vr ii i
    static member la_i_subtract_vr (i0:decimal, i1:decimal) =
        i0
    //la_i_t_assign i i
    static member la_i_t_assign (i0:decimal) =
        i0
    //la_i_trace_mc i ii
    static member la_i_trace_mc (i0:decimal) =
        i0, i0
    //la_i_trace_mr i i
    static member la_i_trace_mr (i0:decimal) =
        i0
    //la_i_transpose_mc i i
    static member la_i_transpose_mc (i0:decimal) =
        i0
    //la_i_transpose_mr i i
    static member la_i_transpose_mr (i0:decimal) =
        i0
    //la_i_transpose_mr k i
    static member la_i_transpose_mr (k0:single) =
        0.0m
    //la_i_upper_solve_mc io i
    static member la_i_upper_solve_mc (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //la_i_upper_solve_mr io i
    static member la_i_upper_solve_mr (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        i0
    //la_i_vc_create i i
    static member la_i_vc_create (i0:decimal) =
        i0
    //la_i_vc_set iii i
    static member la_i_vc_set (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //la_i_vr_create i i
    static member la_i_vr_create (i0:decimal) =
        i0
    //la_i_vr_set ii i
    static member la_i_vr_set (i0:decimal, i1:decimal) =
        i0
    //la_k_a_assign k a
    static member la_k_a_assign (k0:single) =
        vector [(double)0.0]
    //la_k_add_mc ii i
    static member la_k_add_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_add_mr ii i
    static member la_k_add_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_add_vc ii i
    static member la_k_add_vc (i0:decimal, i1:decimal) =
        i0
    //la_k_add_vr ii i
    static member la_k_add_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_assign_a a i
    static member la_k_assign_a (a0:Vector<double>) =
        0.0m
    //la_k_assign_f f i
    static member la_k_assign_f (S0:_[]) =
        0.0m
    //la_k_assign_mc k i
    static member la_k_assign_mc (k0:single) =
        0.0m
    //la_k_assign_mr k i
    static member la_k_assign_mr (k0:single) =
        0.0m
    //la_k_assign_t k i
    static member la_k_assign_t (k0:single) =
        0.0m
    //la_k_assign_vc k i
    static member la_k_assign_vc (k0:single) =
        0.0m
    //la_k_assign_vr k i
    static member la_k_assign_vr (k0:single) =
        0.0m
    //la_k_conjugate_mc i i
    static member la_k_conjugate_mc (i0:decimal) =
        i0
    //la_k_conjugate_mr i i
    static member la_k_conjugate_mr (i0:decimal) =
        i0
    //la_k_conjugate_vc i i
    static member la_k_conjugate_vc (i0:decimal) =
        i0
    //la_k_conjugate_vr i i
    static member la_k_conjugate_vr (i0:decimal) =
        i0
    //la_k_current_f f k
    static member la_k_current_f (S0:_[]) =
        0.0f
    //la_k_current_vr i k
    static member la_k_current_vr (i0:decimal) =
        0.0f
    //la_k_distance_vc ii k
    static member la_k_distance_vc (i0:decimal, i1:decimal) =
        0.0f
    //la_k_distance_vr ii k
    static member la_k_distance_vr (i0:decimal, i1:decimal) =
        0.0f
    //la_k_divide_mc ii i
    static member la_k_divide_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_divide_mr ii i
    static member la_k_divide_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_divide_vc kk i
    static member la_k_divide_vc (k0:single, k1:single) =
        0.0m
    //la_k_divide_vr ii i
    static member la_k_divide_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_dot_mc ii i
    static member la_k_dot_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_dot_mc_vc ii i
    static member la_k_dot_mc_vc (i0:decimal, i1:decimal) =
        i0
    //la_k_dot_mr ii i
    static member la_k_dot_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_dot_mr_vr ii i
    static member la_k_dot_mr_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_dot_vc ii ii
    static member la_k_dot_vc (i0:decimal, i1:decimal) =
        i0, i0
    //la_k_dot_vr ii i
    static member la_k_dot_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_f_assign p f
    static member la_k_f_assign (?p0:decimal) =
        let p0 = defaultArg p0 1m
        0.0
    //la_k_get_mc ikk kk
    static member la_k_get_mc (i0:decimal, k1:single, k2:single) =
        k1, k1
    //la_k_get_mr ikk k
    static member la_k_get_mr (i0:decimal, k1:single, k2:single) =
        k1
    //la_k_get_vc ik kk
    static member la_k_get_vc (i0:decimal, k1:single) =
        k1, k1
    //la_k_get_vr ik k
    static member la_k_get_vr (i0:decimal, k1:single) =
        k1
    //la_k_invert_mc i ikk
    static member la_k_invert_mc (i0:decimal) =
        i0, 0.0f, 0.0f
    //la_k_invert_mr i ik
    static member la_k_invert_mr (i0:decimal) =
        i0, 0.0f
    //la_k_lower_solve_mc iO i
    static member la_k_lower_solve_mc (i0:decimal, ?O1:single) =
        let O1 = defaultArg O1 0.f
        i0
    //la_k_lower_solve_mr iO i
    static member la_k_lower_solve_mr (i0:decimal, ?O1:single) =
        let O1 = defaultArg O1 0.f
        i0
    //la_k_lu_det_mc i kk
    static member la_k_lu_det_mc (i0:decimal) =
        0.0f, 0.0f
    //la_k_lu_det_mr i k
    static member la_k_lu_det_mr (i0:decimal) =
        0.0f
    //la_k_lu_factor_mc i i
    static member la_k_lu_factor_mc (i0:decimal) =
        i0
    //la_k_lu_factor_mr i iik
    static member la_k_lu_factor_mr (i0:decimal) =
        i0, i0, 0.0f
    //la_k_lu_solve_mc ii i
    static member la_k_lu_solve_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_lu_solve_mr ii i
    static member la_k_lu_solve_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_mc_set kkkk i
    static member la_k_mc_set (k0:single, k1:single, k2:single, k3:single) =
        0.0m
    //la_k_mr_set kkk i
    static member la_k_mr_set (k0:single, k1:single, k2:single) =
        0.0m
    //la_k_multiply_mc ii i
    static member la_k_multiply_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_multiply_mr ii i
    static member la_k_multiply_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_multiply_vc ii i
    static member la_k_multiply_vc (i0:decimal, i1:decimal) =
        i0
    //la_k_multiply_vr ii i
    static member la_k_multiply_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_norm_euclid_mc i k
    static member la_k_norm_euclid_mc (i0:decimal) =
        0.0f
    //la_k_norm_euclid_mr i k
    static member la_k_norm_euclid_mr (i0:decimal) =
        0.0f
    //la_k_norm_euclid_vc i k
    static member la_k_norm_euclid_vc (i0:decimal) =
        0.0f
    //la_k_norm_euclid_vr i k
    static member la_k_norm_euclid_vr (i0:decimal) =
        0.0f
    //la_k_norm_inf_mc i k
    static member la_k_norm_inf_mc (i0:decimal) =
        0.0f
    //la_k_norm_inf_mr i k
    static member la_k_norm_inf_mr (i0:decimal) =
        0.0f
    //la_k_norm_inf_vc i k
    static member la_k_norm_inf_vc (i0:decimal) =
        0.0f
    //la_k_norm_inf_vr i k
    static member la_k_norm_inf_vr (i0:decimal) =
        0.0f
    //la_k_norm_max_mc i k
    static member la_k_norm_max_mc (i0:decimal) =
        0.0f
    //la_k_norm_max_mr i k
    static member la_k_norm_max_mr (i0:decimal) =
        0.0f
    //la_k_norm1_mc i k
    static member la_k_norm1_mc (i0:decimal) =
        0.0f
    //la_k_norm1_mr i k
    static member la_k_norm1_mr (i0:decimal) =
        0.0f
    //la_k_norm1_vc i k
    static member la_k_norm1_vc (i0:decimal) =
        0.0f
    //la_k_norm1_vr i k
    static member la_k_norm1_vr (i0:decimal) =
        0.0f
    //la_k_qr_eigen_mc ik i
    static member la_k_qr_eigen_mc (i0:decimal, k1:single) =
        i0
    //la_k_qr_eigen_mr ik i
    static member la_k_qr_eigen_mr (i0:decimal, k1:single) =
        i0
    //la_k_qr_factor_mc i ii
    static member la_k_qr_factor_mc (i0:decimal) =
        i0, i0
    //la_k_qr_factor_mr i ii
    static member la_k_qr_factor_mr (i0:decimal) =
        i0, i0
    //la_k_qr_sym_eigen_mc ik ii
    static member la_k_qr_sym_eigen_mc (i0:decimal, k1:single) =
        i0, i0
    //la_k_qr_sym_eigen_mr ik ii
    static member la_k_qr_sym_eigen_mr (i0:decimal, k1:single) =
        i0, i0
    //la_k_random_mc p i
    static member la_k_random_mc (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_k_random_mr p i
    static member la_k_random_mr (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_k_random_vc p i
    static member la_k_random_vc (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_k_random_vr p i
    static member la_k_random_vr (?p0:decimal) =
        let p0 = defaultArg p0 1m
        p0
    //la_k_subtract_mc ii i
    static member la_k_subtract_mc (i0:decimal, i1:decimal) =
        i0
    //la_k_subtract_mr ii i
    static member la_k_subtract_mr (i0:decimal, i1:decimal) =
        i0
    //la_k_subtract_vc ii i
    static member la_k_subtract_vc (i0:decimal, i1:decimal) =
        i0
    //la_k_subtract_vr ii i
    static member la_k_subtract_vr (i0:decimal, i1:decimal) =
        i0
    //la_k_t_assign k i
    static member la_k_t_assign (k0:single) =
        0.0m
    //la_k_trace_mc i kk
    static member la_k_trace_mc (i0:decimal) =
        0.0f, 0.0f
    //la_k_trace_mr i k
    static member la_k_trace_mr (i0:decimal) =
        0.0f
    //la_k_upper_solve_mc iO i
    static member la_k_upper_solve_mc (i0:decimal, ?O1:single) =
        let O1 = defaultArg O1 0.f
        i0
    //la_k_upper_solve_mr iO i
    static member la_k_upper_solve_mr (i0:decimal, ?O1:single) =
        let O1 = defaultArg O1 0.f
        i0
    //la_k_vc_set kkk i
    static member la_k_vc_set (k0:single, k1:single, k2:single) =
        0.0m
    //la_k_vr_set kk i
    static member la_k_vr_set (k0:single, k1:single) =
        0.0m
    //lenarray<'A> .[]p  -> (i | k)

    static member lenarray<'A> (M0:decimal[], ?p1:decimal) =

        let p1 = defaultArg p1 1m

        Unchecked.defaultof<'A>
    //lentab<'A> k[]p  -> (i | k)

    static member lentab<'A> (k0:single, M1:decimal[], ?p2:decimal) =

        let p2 = defaultArg p2 1m

        Unchecked.defaultof<'A>
    //lfo<'A> kko  -> (a | k)

    static member lfo<'A> (k0:single, k1:single, ?o2:decimal) =

        let o2 = defaultArg o2 0m

        Unchecked.defaultof<'A>
    //limit<'A> xkk  -> (a | k)

    static member limit<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, k2:single) =

        Unchecked.defaultof<'A>
    //limit iii i
    static member limit (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //line<'A> iii  -> (a | k)

    static member line<'A> (i0:decimal, i1:decimal, i2:decimal) =

        Unchecked.defaultof<'A>
    //linen aiii a
    static member linen (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal) =
        a0
    //linen<'A> kiii  -> (a | k)

    static member linen<'A> (k0:single, i1:decimal, i2:decimal, i3:decimal) =

        Unchecked.defaultof<'A>
    //linenr aiii a
    static member linenr (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal) =
        a0
    //linenr<'A> kiii  -> (a | k)

    static member linenr<'A> (k0:single, i1:decimal, i2:decimal, i3:decimal) =

        Unchecked.defaultof<'A>
    //lineto kk k
    static member lineto (k0:single, k1:single) =
        k0
    //linrand<'A> k  -> (a | i | k)

    static member linrand<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //linseg<'A> iin  -> (a | k)

    static member linseg<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //linsegb<'A> iin  -> (a | k)

    static member linsegb<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //linsegr<'A> iin  -> (a | k)

    static member linsegr<'A> (i0:decimal, i1:decimal, n2:decimal[]) =

        Unchecked.defaultof<'A>
    //locsend  mmmm
    static member locsend () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //locsig akkk mmmm
    static member locsig (a0:Vector<double>, k1:single, k2:single, k3:single) =
        a0, a0, a0, a0
    //log a a
    static member log (a0:Vector<double>) =
        a0
    //log i i
    static member log (i0:decimal) =
        i0
    //log k k
    static member log (k0:single) =
        k0
    //log k[]o k[]
    static member log (k0:single, M1:decimal[], ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0, M1, 0.0
    //log10 a a
    static member log10 (a0:Vector<double>) =
        a0
    //log10 i i
    static member log10 (i0:decimal) =
        i0
    //log10 k k
    static member log10 (k0:single) =
        k0
    //log2 a a
    static member log2 (a0:Vector<double>) =
        a0
    //log2 i i
    static member log2 (i0:decimal) =
        i0
    //log2 k k
    static member log2 (k0:single) =
        k0
    //logbtwo a a
    static member logbtwo (a0:Vector<double>) =
        a0
    //logbtwo i i
    static member logbtwo (i0:decimal) =
        i0
    //logbtwo k k
    static member logbtwo (k0:single) =
        k0
    //logcurve kk k
    static member logcurve (k0:single, k1:single) =
        k0
    //loop_ge iiil 
    static member loop_ge (i0:decimal, i1:decimal, i2:decimal, l3:_) =
        ()
    //loop_ge kkkl 
    static member loop_ge (k0:single, k1:single, k2:single, l3:_) =
        ()
    //loop_gt iiil 
    static member loop_gt (i0:decimal, i1:decimal, i2:decimal, l3:_) =
        ()
    //loop_gt kkkl 
    static member loop_gt (k0:single, k1:single, k2:single, l3:_) =
        ()
    //loop_le iiil 
    static member loop_le (i0:decimal, i1:decimal, i2:decimal, l3:_) =
        ()
    //loop_le kkkl 
    static member loop_le (k0:single, k1:single, k2:single, l3:_) =
        ()
    //loop_lt iiil 
    static member loop_lt (i0:decimal, i1:decimal, i2:decimal, l3:_) =
        ()
    //loop_lt kkkl 
    static member loop_lt (k0:single, k1:single, k2:single, l3:_) =
        ()
    //loopseg kkiz k
    static member loopseg (k0:single, k1:single, i2:decimal, z3:single[]) =
        k0
    //loopsegp kz k
    static member loopsegp (k0:single, z1:single[]) =
        k0
    //looptseg kkiz k
    static member looptseg (k0:single, k1:single, i2:decimal, z3:single[]) =
        k0
    //loopxseg kkiz k
    static member loopxseg (k0:single, k1:single, i2:decimal, z3:single[]) =
        k0
    //lorenz kkkkiiiio aaa
    static member lorenz (k0:single, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, i6:decimal, i7:decimal, ?o8:decimal) =
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //loscil xkjojoojoo mm
    static member loscil (x0:CSoundOpcode.ScalarOrArray, k1:single, ?j2:decimal, ?o3:decimal, ?j4:decimal, ?o5:decimal, ?o6:decimal, ?j7:decimal, ?o8:decimal, ?o9:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        let j4 = defaultArg j4 -0.1m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let j7 = defaultArg j7 -0.1m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        vector [(double)0.0], vector [(double)0.0]
    //loscil3 xkjojoojoo mm
    static member loscil3 (x0:CSoundOpcode.ScalarOrArray, k1:single, ?j2:decimal, ?o3:decimal, ?j4:decimal, ?o5:decimal, ?o6:decimal, ?j7:decimal, ?o8:decimal, ?o9:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        let j4 = defaultArg j4 -0.1m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let j7 = defaultArg j7 -0.1m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        vector [(double)0.0], vector [(double)0.0]
    //loscilx xkioojjoo mmmmmmmmmmmmmmmm
    static member loscilx (x0:CSoundOpcode.ScalarOrArray, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal, ?j5:decimal, ?j6:decimal, ?o7:decimal, ?o8:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let j5 = defaultArg j5 -0.1m
        let j6 = defaultArg j6 -0.1m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //lowpass2 aaao a
    static member lowpass2 (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowpass2 aakao a
    static member lowpass2 (a0:Vector<double>, a1:Vector<double>, k2:single, a3:Vector<double>, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //lowpass2 akao a
    static member lowpass2 (a0:Vector<double>, k1:single, a2:Vector<double>, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowpass2 akko a
    static member lowpass2 (a0:Vector<double>, k1:single, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowres aaao a
    static member lowres (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowres aako a
    static member lowres (a0:Vector<double>, a1:Vector<double>, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowres akao a
    static member lowres (a0:Vector<double>, k1:single, a2:Vector<double>, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowres akko a
    static member lowres (a0:Vector<double>, k1:single, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //lowresx axxoo a
    static member lowresx (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //lpf18 axxxo a
    static member lpf18 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //lpform k kk
    static member lpform (k0:single) =
        k0, k0
    //lpfreson ak a
    static member lpfreson (a0:Vector<double>, k1:single) =
        a0
    //lphasor xooooooo a
    static member lphasor (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //lpinterp iik 
    static member lpinterp (i0:decimal, i1:decimal, k2:single) =
        ()
    //lposcil kkkkjo a
    static member lposcil (k0:single, k1:single, k2:single, k3:single, ?j4:decimal, ?o5:decimal) =
        let j4 = defaultArg j4 -0.1m
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //lposcil3 kkkkjo a
    static member lposcil3 (k0:single, k1:single, k2:single, k3:single, ?j4:decimal, ?o5:decimal) =
        let j4 = defaultArg j4 -0.1m
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //lposcila akkkio a
    static member lposcila (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0
    //lposcilsa akkkio aa
    static member lposcilsa (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0, a0
    //lposcilsa2 akkkio aa
    static member lposcilsa2 (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0, a0
    //lpread kSoo kkkk
    static member lpread (k0:single, s1:string, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        k0, k0, k0, k0
    //lpread kioo kkkk
    static member lpread (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        k0, k0, k0, k0
    //lpreson a a
    static member lpreson (a0:Vector<double>) =
        a0
    //lpshold kkiz k
    static member lpshold (k0:single, k1:single, i2:decimal, z3:single[]) =
        k0
    //lpsholdp kz k
    static member lpsholdp (k0:single, z1:single[]) =
        k0
    //lpslot i 
    static member lpslot (i0:decimal) =
        ()
    //lua_exec TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_exec (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_iaopcall TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_iaopcall (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_iaopcall_off TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_iaopcall_off (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_ikopcall TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_ikopcall (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_ikopcall_off TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_ikopcall_off (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_iopcall TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_iopcall (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_iopcall_off TNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
    static member lua_iopcall_off (T0:'a, [<ParamArray>] ?N1:Object[], [<ParamArray>] ?N2:Object[], [<ParamArray>] ?N3:Object[], [<ParamArray>] ?N4:Object[], [<ParamArray>] ?N5:Object[], [<ParamArray>] ?N6:Object[], [<ParamArray>] ?N7:Object[], [<ParamArray>] ?N8:Object[], [<ParamArray>] ?N9:Object[], [<ParamArray>] ?N10:Object[], [<ParamArray>] ?N11:Object[], [<ParamArray>] ?N12:Object[], [<ParamArray>] ?N13:Object[], [<ParamArray>] ?N14:Object[], [<ParamArray>] ?N15:Object[], [<ParamArray>] ?N16:Object[], [<ParamArray>] ?N17:Object[], [<ParamArray>] ?N18:Object[], [<ParamArray>] ?N19:Object[], [<ParamArray>] ?N20:Object[], [<ParamArray>] ?N21:Object[], [<ParamArray>] ?N22:Object[], [<ParamArray>] ?N23:Object[], [<ParamArray>] ?N24:Object[], [<ParamArray>] ?N25:Object[], [<ParamArray>] ?N26:Object[], [<ParamArray>] ?N27:Object[], [<ParamArray>] ?N28:Object[], [<ParamArray>] ?N29:Object[], [<ParamArray>] ?N30:Object[], [<ParamArray>] ?N31:Object[], [<ParamArray>] ?N32:Object[], [<ParamArray>] ?N33:Object[], [<ParamArray>] ?N34:Object[], [<ParamArray>] ?N35:Object[], [<ParamArray>] ?N36:Object[], [<ParamArray>] ?N37:Object[], [<ParamArray>] ?N38:Object[], [<ParamArray>] ?N39:Object[], [<ParamArray>] ?N40:Object[], [<ParamArray>] ?N41:Object[], [<ParamArray>] ?N42:Object[], [<ParamArray>] ?N43:Object[], [<ParamArray>] ?N44:Object[], [<ParamArray>] ?N45:Object[], [<ParamArray>] ?N46:Object[], [<ParamArray>] ?N47:Object[], [<ParamArray>] ?N48:Object[], [<ParamArray>] ?N49:Object[], [<ParamArray>] ?N50:Object[], [<ParamArray>] ?N51:Object[], [<ParamArray>] ?N52:Object[], [<ParamArray>] ?N53:Object[], [<ParamArray>] ?N54:Object[], [<ParamArray>] ?N55:Object[], [<ParamArray>] ?N56:Object[], [<ParamArray>] ?N57:Object[], [<ParamArray>] ?N58:Object[], [<ParamArray>] ?N59:Object[], [<ParamArray>] ?N60:Object[], [<ParamArray>] ?N61:Object[], [<ParamArray>] ?N62:Object[], [<ParamArray>] ?N63:Object[], [<ParamArray>] ?N64:Object[], [<ParamArray>] ?N65:Object[], [<ParamArray>] ?N66:Object[], [<ParamArray>] ?N67:Object[], [<ParamArray>] ?N68:Object[], [<ParamArray>] ?N69:Object[], [<ParamArray>] ?N70:Object[], [<ParamArray>] ?N71:Object[], [<ParamArray>] ?N72:Object[], [<ParamArray>] ?N73:Object[], [<ParamArray>] ?N74:Object[], [<ParamArray>] ?N75:Object[], [<ParamArray>] ?N76:Object[], [<ParamArray>] ?N77:Object[], [<ParamArray>] ?N78:Object[], [<ParamArray>] ?N79:Object[], [<ParamArray>] ?N80:Object[], [<ParamArray>] ?N81:Object[], [<ParamArray>] ?N82:Object[], [<ParamArray>] ?N83:Object[], [<ParamArray>] ?N84:Object[], [<ParamArray>] ?N85:Object[], [<ParamArray>] ?N86:Object[], [<ParamArray>] ?N87:Object[], [<ParamArray>] ?N88:Object[], [<ParamArray>] ?N89:Object[], [<ParamArray>] ?N90:Object[], [<ParamArray>] ?N91:Object[], [<ParamArray>] ?N92:Object[], [<ParamArray>] ?N93:Object[], [<ParamArray>] ?N94:Object[], [<ParamArray>] ?N95:Object[], [<ParamArray>] ?N96:Object[], [<ParamArray>] ?N97:Object[], [<ParamArray>] ?N98:Object[], [<ParamArray>] ?N99:Object[], [<ParamArray>] ?N100:Object[], [<ParamArray>] ?N101:Object[], [<ParamArray>] ?N102:Object[], [<ParamArray>] ?N103:Object[], [<ParamArray>] ?N104:Object[], [<ParamArray>] ?N105:Object[], [<ParamArray>] ?N106:Object[], [<ParamArray>] ?N107:Object[], [<ParamArray>] ?N108:Object[], [<ParamArray>] ?N109:Object[], [<ParamArray>] ?N110:Object[], [<ParamArray>] ?N111:Object[], [<ParamArray>] ?N112:Object[], [<ParamArray>] ?N113:Object[], [<ParamArray>] ?N114:Object[], [<ParamArray>] ?N115:Object[], [<ParamArray>] ?N116:Object[], [<ParamArray>] ?N117:Object[], [<ParamArray>] ?N118:Object[], [<ParamArray>] ?N119:Object[], [<ParamArray>] ?N120:Object[], [<ParamArray>] ?N121:Object[], [<ParamArray>] ?N122:Object[], [<ParamArray>] ?N123:Object[], [<ParamArray>] ?N124:Object[], [<ParamArray>] ?N125:Object[], [<ParamArray>] ?N126:Object[], [<ParamArray>] ?N127:Object[], [<ParamArray>] ?N128:Object[], [<ParamArray>] ?N129:Object[], [<ParamArray>] ?N130:Object[], [<ParamArray>] ?N131:Object[], [<ParamArray>] ?N132:Object[], [<ParamArray>] ?N133:Object[], [<ParamArray>] ?N134:Object[], [<ParamArray>] ?N135:Object[], [<ParamArray>] ?N136:Object[], [<ParamArray>] ?N137:Object[], [<ParamArray>] ?N138:Object[], [<ParamArray>] ?N139:Object[], [<ParamArray>] ?N140:Object[], [<ParamArray>] ?N141:Object[], [<ParamArray>] ?N142:Object[], [<ParamArray>] ?N143:Object[], [<ParamArray>] ?N144:Object[], [<ParamArray>] ?N145:Object[], [<ParamArray>] ?N146:Object[], [<ParamArray>] ?N147:Object[], [<ParamArray>] ?N148:Object[], [<ParamArray>] ?N149:Object[], [<ParamArray>] ?N150:Object[], [<ParamArray>] ?N151:Object[], [<ParamArray>] ?N152:Object[], [<ParamArray>] ?N153:Object[], [<ParamArray>] ?N154:Object[], [<ParamArray>] ?N155:Object[], [<ParamArray>] ?N156:Object[], [<ParamArray>] ?N157:Object[], [<ParamArray>] ?N158:Object[], [<ParamArray>] ?N159:Object[], [<ParamArray>] ?N160:Object[], [<ParamArray>] ?N161:Object[], [<ParamArray>] ?N162:Object[], [<ParamArray>] ?N163:Object[], [<ParamArray>] ?N164:Object[], [<ParamArray>] ?N165:Object[], [<ParamArray>] ?N166:Object[], [<ParamArray>] ?N167:Object[], [<ParamArray>] ?N168:Object[], [<ParamArray>] ?N169:Object[], [<ParamArray>] ?N170:Object[], [<ParamArray>] ?N171:Object[], [<ParamArray>] ?N172:Object[], [<ParamArray>] ?N173:Object[], [<ParamArray>] ?N174:Object[], [<ParamArray>] ?N175:Object[], [<ParamArray>] ?N176:Object[], [<ParamArray>] ?N177:Object[], [<ParamArray>] ?N178:Object[], [<ParamArray>] ?N179:Object[], [<ParamArray>] ?N180:Object[], [<ParamArray>] ?N181:Object[], [<ParamArray>] ?N182:Object[], [<ParamArray>] ?N183:Object[], [<ParamArray>] ?N184:Object[], [<ParamArray>] ?N185:Object[], [<ParamArray>] ?N186:Object[], [<ParamArray>] ?N187:Object[], [<ParamArray>] ?N188:Object[], [<ParamArray>] ?N189:Object[], [<ParamArray>] ?N190:Object[], [<ParamArray>] ?N191:Object[], [<ParamArray>] ?N192:Object[], [<ParamArray>] ?N193:Object[], [<ParamArray>] ?N194:Object[], [<ParamArray>] ?N195:Object[], [<ParamArray>] ?N196:Object[], [<ParamArray>] ?N197:Object[], [<ParamArray>] ?N198:Object[], [<ParamArray>] ?N199:Object[], [<ParamArray>] ?N200:Object[], [<ParamArray>] ?N201:Object[], [<ParamArray>] ?N202:Object[], [<ParamArray>] ?N203:Object[], [<ParamArray>] ?N204:Object[], [<ParamArray>] ?N205:Object[], [<ParamArray>] ?N206:Object[], [<ParamArray>] ?N207:Object[], [<ParamArray>] ?N208:Object[], [<ParamArray>] ?N209:Object[], [<ParamArray>] ?N210:Object[], [<ParamArray>] ?N211:Object[], [<ParamArray>] ?N212:Object[], [<ParamArray>] ?N213:Object[], [<ParamArray>] ?N214:Object[], [<ParamArray>] ?N215:Object[], [<ParamArray>] ?N216:Object[], [<ParamArray>] ?N217:Object[], [<ParamArray>] ?N218:Object[], [<ParamArray>] ?N219:Object[], [<ParamArray>] ?N220:Object[], [<ParamArray>] ?N221:Object[], [<ParamArray>] ?N222:Object[], [<ParamArray>] ?N223:Object[], [<ParamArray>] ?N224:Object[], [<ParamArray>] ?N225:Object[], [<ParamArray>] ?N226:Object[], [<ParamArray>] ?N227:Object[], [<ParamArray>] ?N228:Object[], [<ParamArray>] ?N229:Object[], [<ParamArray>] ?N230:Object[], [<ParamArray>] ?N231:Object[], [<ParamArray>] ?N232:Object[], [<ParamArray>] ?N233:Object[], [<ParamArray>] ?N234:Object[], [<ParamArray>] ?N235:Object[], [<ParamArray>] ?N236:Object[], [<ParamArray>] ?N237:Object[], [<ParamArray>] ?N238:Object[], [<ParamArray>] ?N239:Object[], [<ParamArray>] ?N240:Object[], [<ParamArray>] ?N241:Object[], [<ParamArray>] ?N242:Object[], [<ParamArray>] ?N243:Object[], [<ParamArray>] ?N244:Object[], [<ParamArray>] ?N245:Object[], [<ParamArray>] ?N246:Object[], [<ParamArray>] ?N247:Object[], [<ParamArray>] ?N248:Object[], [<ParamArray>] ?N249:Object[], [<ParamArray>] ?N250:Object[], [<ParamArray>] ?N251:Object[], [<ParamArray>] ?N252:Object[], [<ParamArray>] ?N253:Object[], [<ParamArray>] ?N254:Object[], [<ParamArray>] ?N255:Object[], [<ParamArray>] ?N256:Object[]) =
        let N1 = defaultArg N1 
        let N2 = defaultArg N2 
        let N3 = defaultArg N3 
        let N4 = defaultArg N4 
        let N5 = defaultArg N5 
        let N6 = defaultArg N6 
        let N7 = defaultArg N7 
        let N8 = defaultArg N8 
        let N9 = defaultArg N9 
        let N10 = defaultArg N10 
        let N11 = defaultArg N11 
        let N12 = defaultArg N12 
        let N13 = defaultArg N13 
        let N14 = defaultArg N14 
        let N15 = defaultArg N15 
        let N16 = defaultArg N16 
        let N17 = defaultArg N17 
        let N18 = defaultArg N18 
        let N19 = defaultArg N19 
        let N20 = defaultArg N20 
        let N21 = defaultArg N21 
        let N22 = defaultArg N22 
        let N23 = defaultArg N23 
        let N24 = defaultArg N24 
        let N25 = defaultArg N25 
        let N26 = defaultArg N26 
        let N27 = defaultArg N27 
        let N28 = defaultArg N28 
        let N29 = defaultArg N29 
        let N30 = defaultArg N30 
        let N31 = defaultArg N31 
        let N32 = defaultArg N32 
        let N33 = defaultArg N33 
        let N34 = defaultArg N34 
        let N35 = defaultArg N35 
        let N36 = defaultArg N36 
        let N37 = defaultArg N37 
        let N38 = defaultArg N38 
        let N39 = defaultArg N39 
        let N40 = defaultArg N40 
        let N41 = defaultArg N41 
        let N42 = defaultArg N42 
        let N43 = defaultArg N43 
        let N44 = defaultArg N44 
        let N45 = defaultArg N45 
        let N46 = defaultArg N46 
        let N47 = defaultArg N47 
        let N48 = defaultArg N48 
        let N49 = defaultArg N49 
        let N50 = defaultArg N50 
        let N51 = defaultArg N51 
        let N52 = defaultArg N52 
        let N53 = defaultArg N53 
        let N54 = defaultArg N54 
        let N55 = defaultArg N55 
        let N56 = defaultArg N56 
        let N57 = defaultArg N57 
        let N58 = defaultArg N58 
        let N59 = defaultArg N59 
        let N60 = defaultArg N60 
        let N61 = defaultArg N61 
        let N62 = defaultArg N62 
        let N63 = defaultArg N63 
        let N64 = defaultArg N64 
        let N65 = defaultArg N65 
        let N66 = defaultArg N66 
        let N67 = defaultArg N67 
        let N68 = defaultArg N68 
        let N69 = defaultArg N69 
        let N70 = defaultArg N70 
        let N71 = defaultArg N71 
        let N72 = defaultArg N72 
        let N73 = defaultArg N73 
        let N74 = defaultArg N74 
        let N75 = defaultArg N75 
        let N76 = defaultArg N76 
        let N77 = defaultArg N77 
        let N78 = defaultArg N78 
        let N79 = defaultArg N79 
        let N80 = defaultArg N80 
        let N81 = defaultArg N81 
        let N82 = defaultArg N82 
        let N83 = defaultArg N83 
        let N84 = defaultArg N84 
        let N85 = defaultArg N85 
        let N86 = defaultArg N86 
        let N87 = defaultArg N87 
        let N88 = defaultArg N88 
        let N89 = defaultArg N89 
        let N90 = defaultArg N90 
        let N91 = defaultArg N91 
        let N92 = defaultArg N92 
        let N93 = defaultArg N93 
        let N94 = defaultArg N94 
        let N95 = defaultArg N95 
        let N96 = defaultArg N96 
        let N97 = defaultArg N97 
        let N98 = defaultArg N98 
        let N99 = defaultArg N99 
        let N100 = defaultArg N100 
        let N101 = defaultArg N101 
        let N102 = defaultArg N102 
        let N103 = defaultArg N103 
        let N104 = defaultArg N104 
        let N105 = defaultArg N105 
        let N106 = defaultArg N106 
        let N107 = defaultArg N107 
        let N108 = defaultArg N108 
        let N109 = defaultArg N109 
        let N110 = defaultArg N110 
        let N111 = defaultArg N111 
        let N112 = defaultArg N112 
        let N113 = defaultArg N113 
        let N114 = defaultArg N114 
        let N115 = defaultArg N115 
        let N116 = defaultArg N116 
        let N117 = defaultArg N117 
        let N118 = defaultArg N118 
        let N119 = defaultArg N119 
        let N120 = defaultArg N120 
        let N121 = defaultArg N121 
        let N122 = defaultArg N122 
        let N123 = defaultArg N123 
        let N124 = defaultArg N124 
        let N125 = defaultArg N125 
        let N126 = defaultArg N126 
        let N127 = defaultArg N127 
        let N128 = defaultArg N128 
        let N129 = defaultArg N129 
        let N130 = defaultArg N130 
        let N131 = defaultArg N131 
        let N132 = defaultArg N132 
        let N133 = defaultArg N133 
        let N134 = defaultArg N134 
        let N135 = defaultArg N135 
        let N136 = defaultArg N136 
        let N137 = defaultArg N137 
        let N138 = defaultArg N138 
        let N139 = defaultArg N139 
        let N140 = defaultArg N140 
        let N141 = defaultArg N141 
        let N142 = defaultArg N142 
        let N143 = defaultArg N143 
        let N144 = defaultArg N144 
        let N145 = defaultArg N145 
        let N146 = defaultArg N146 
        let N147 = defaultArg N147 
        let N148 = defaultArg N148 
        let N149 = defaultArg N149 
        let N150 = defaultArg N150 
        let N151 = defaultArg N151 
        let N152 = defaultArg N152 
        let N153 = defaultArg N153 
        let N154 = defaultArg N154 
        let N155 = defaultArg N155 
        let N156 = defaultArg N156 
        let N157 = defaultArg N157 
        let N158 = defaultArg N158 
        let N159 = defaultArg N159 
        let N160 = defaultArg N160 
        let N161 = defaultArg N161 
        let N162 = defaultArg N162 
        let N163 = defaultArg N163 
        let N164 = defaultArg N164 
        let N165 = defaultArg N165 
        let N166 = defaultArg N166 
        let N167 = defaultArg N167 
        let N168 = defaultArg N168 
        let N169 = defaultArg N169 
        let N170 = defaultArg N170 
        let N171 = defaultArg N171 
        let N172 = defaultArg N172 
        let N173 = defaultArg N173 
        let N174 = defaultArg N174 
        let N175 = defaultArg N175 
        let N176 = defaultArg N176 
        let N177 = defaultArg N177 
        let N178 = defaultArg N178 
        let N179 = defaultArg N179 
        let N180 = defaultArg N180 
        let N181 = defaultArg N181 
        let N182 = defaultArg N182 
        let N183 = defaultArg N183 
        let N184 = defaultArg N184 
        let N185 = defaultArg N185 
        let N186 = defaultArg N186 
        let N187 = defaultArg N187 
        let N188 = defaultArg N188 
        let N189 = defaultArg N189 
        let N190 = defaultArg N190 
        let N191 = defaultArg N191 
        let N192 = defaultArg N192 
        let N193 = defaultArg N193 
        let N194 = defaultArg N194 
        let N195 = defaultArg N195 
        let N196 = defaultArg N196 
        let N197 = defaultArg N197 
        let N198 = defaultArg N198 
        let N199 = defaultArg N199 
        let N200 = defaultArg N200 
        let N201 = defaultArg N201 
        let N202 = defaultArg N202 
        let N203 = defaultArg N203 
        let N204 = defaultArg N204 
        let N205 = defaultArg N205 
        let N206 = defaultArg N206 
        let N207 = defaultArg N207 
        let N208 = defaultArg N208 
        let N209 = defaultArg N209 
        let N210 = defaultArg N210 
        let N211 = defaultArg N211 
        let N212 = defaultArg N212 
        let N213 = defaultArg N213 
        let N214 = defaultArg N214 
        let N215 = defaultArg N215 
        let N216 = defaultArg N216 
        let N217 = defaultArg N217 
        let N218 = defaultArg N218 
        let N219 = defaultArg N219 
        let N220 = defaultArg N220 
        let N221 = defaultArg N221 
        let N222 = defaultArg N222 
        let N223 = defaultArg N223 
        let N224 = defaultArg N224 
        let N225 = defaultArg N225 
        let N226 = defaultArg N226 
        let N227 = defaultArg N227 
        let N228 = defaultArg N228 
        let N229 = defaultArg N229 
        let N230 = defaultArg N230 
        let N231 = defaultArg N231 
        let N232 = defaultArg N232 
        let N233 = defaultArg N233 
        let N234 = defaultArg N234 
        let N235 = defaultArg N235 
        let N236 = defaultArg N236 
        let N237 = defaultArg N237 
        let N238 = defaultArg N238 
        let N239 = defaultArg N239 
        let N240 = defaultArg N240 
        let N241 = defaultArg N241 
        let N242 = defaultArg N242 
        let N243 = defaultArg N243 
        let N244 = defaultArg N244 
        let N245 = defaultArg N245 
        let N246 = defaultArg N246 
        let N247 = defaultArg N247 
        let N248 = defaultArg N248 
        let N249 = defaultArg N249 
        let N250 = defaultArg N250 
        let N251 = defaultArg N251 
        let N252 = defaultArg N252 
        let N253 = defaultArg N253 
        let N254 = defaultArg N254 
        let N255 = defaultArg N255 
        let N256 = defaultArg N256 
        ()
    //lua_opdef TT 
    static member lua_opdef (T0:'a, T1:'a) =
        ()
    //mac Z a
    static member mac (Z0:single[][]) =
        vector [(double)0.0]
    //maca y a
    static member maca (y0:Vector<double>[][]) =
        vector [(double)0.0]
    //madsr<'A> iiiioj  -> (k | s)

    static member madsr<'A> (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?j5:decimal) =

        let o4 = defaultArg o4 0m

        let j5 = defaultArg j5 -0.1m

        Unchecked.defaultof<'A>
    //mags k[] k[]
    static member mags (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //mandel kkkk kk
    static member mandel (k0:single, k1:single, k2:single, k3:single) =
        k0, k0
    //mandol kkkkkkio a
    static member mandol (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //maparray i[]S i[]
    static member maparray (i0:decimal, M1:decimal[], s2:string) =
        i0, M1, 0.0
    //maparray k[]S k[]
    static member maparray (k0:single, M1:decimal[], s2:string) =
        k0, M1, 0.0
    //maparray_i k[]S k[]
    static member maparray_i (k0:single, M1:decimal[], s2:string) =
        k0, M1, 0.0
    //marimba kkiiikkiijj a
    static member marimba (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, k5:single, k6:single, i7:decimal, i8:decimal, ?j9:decimal, ?j10:decimal) =
        let j9 = defaultArg j9 -0.1m
        let j10 = defaultArg j10 -0.1m
        vector [(double)0.0]
    //massign iSp 
    static member massign (i0:decimal, s1:string, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
    //massign iip 
    static member massign (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
    //max ay a
    static member max (a0:Vector<double>, y1:Vector<double>[][]) =
        a0
    //max im i
    static member max (i0:decimal, m1:Vector<double>) =
        i0
    //max kz k
    static member max (k0:single, z1:single[]) =
        k0
    //max_k aki k
    static member max_k (a0:Vector<double>, k1:single, i2:decimal) =
        k1
    //maxabs ay a
    static member maxabs (a0:Vector<double>, y1:Vector<double>[][]) =
        a0
    //maxabs kz k
    static member maxabs (k0:single, z1:single[]) =
        k0
    //maxabsaccum aa 
    static member maxabsaccum (a0:Vector<double>, a1:Vector<double>) =
        ()
    //maxaccum aa 
    static member maxaccum (a0:Vector<double>, a1:Vector<double>) =
        ()
    //maxalloc Si 
    static member maxalloc (s0:string, i1:decimal) =
        ()
    //maxalloc ii 
    static member maxalloc (i0:decimal, i1:decimal) =
        ()
    //maxarray i[] iI
    static member maxarray (i0:decimal, M1:decimal[]) =
        i0
    //maxarray k[] kz
    static member maxarray (k0:single, M1:decimal[]) =
        k0, 0.0
    //maxtab k[] kz
    static member maxtab (k0:single, M1:decimal[]) =
        k0, 0.0
    //mclock i 
    static member mclock (i0:decimal) =
        ()
    //mdelay kkkkk 
    static member mdelay (k0:single, k1:single, k2:single, k3:single, k4:single) =
        ()
    //median akio a
    static member median (a0:Vector<double>, k1:single, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //mediank kkio k
    static member mediank (k0:single, k1:single, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        k0
    //metro ko k
    static member metro (k0:single, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        k0
    //midglobal Sm 
    static member midglobal (s0:string, m1:Vector<double>) =
        ()
    //midic14 iiiio i
    static member midic14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        i0
    //midic14 iikko k
    static member midic14 (i0:decimal, i1:decimal, k2:single, k3:single, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        k2
    //midic21 iiiiio i
    static member midic21 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        i0
    //midic21 iiikko k
    static member midic21 (i0:decimal, i1:decimal, i2:decimal, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        k3
    //midic7 iiio i
    static member midic7 (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        i0
    //midic7 ikko k
    static member midic7 (i0:decimal, k1:single, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        k1
    //midichannelaftertouch xoh 
    static member midichannelaftertouch (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal, ?h2:decimal) =
        let o1 = defaultArg o1 0m
        let h2 = defaultArg h2 127m
        ()
    //midichn  i
    static member midichn () =
        0.0m
    //midicontrolchange xxoh 
    static member midicontrolchange (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, ?o2:decimal, ?h3:decimal) =
        let o2 = defaultArg o2 0m
        let h3 = defaultArg h3 127m
        ()
    //midictrl<'A> ioh  -> (i | k)

    static member midictrl<'A> (i0:decimal, ?o1:decimal, ?h2:decimal) =

        let o1 = defaultArg o1 0m

        let h2 = defaultArg h2 127m

        Unchecked.defaultof<'A>
    //mididefault xx 
    static member mididefault (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midifilestatus  k
    static member midifilestatus () =
        0.0f
    //midiin  kkkk
    static member midiin () =
        0.0f, 0.0f, 0.0f, 0.0f
    //midinoteoff xx 
    static member midinoteoff (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midinoteoncps xx 
    static member midinoteoncps (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midinoteonkey xx 
    static member midinoteonkey (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midinoteonoct xx 
    static member midinoteonoct (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midinoteonpch xx 
    static member midinoteonpch (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray) =
        ()
    //midion kkk 
    static member midion (k0:single, k1:single, k2:single) =
        ()
    //midion2 kkkk 
    static member midion2 (k0:single, k1:single, k2:single, k3:single) =
        ()
    //midiout kkkk 
    static member midiout (k0:single, k1:single, k2:single, k3:single) =
        ()
    //midipgm o i
    static member midipgm (?o0:decimal) =
        let o0 = defaultArg o0 0m
        o0
    //midipitchbend xoh 
    static member midipitchbend (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal, ?h2:decimal) =
        let o1 = defaultArg o1 0m
        let h2 = defaultArg h2 127m
        ()
    //midipolyaftertouch xxoh 
    static member midipolyaftertouch (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, ?o2:decimal, ?h3:decimal) =
        let o2 = defaultArg o2 0m
        let h3 = defaultArg h3 127m
        ()
    //midiprogramchange x 
    static member midiprogramchange (x0:CSoundOpcode.ScalarOrArray) =
        ()
    //miditempo  k
    static member miditempo () =
        0.0f
    //midremot SSm 
    static member midremot (s0:string, s1:string, m2:Vector<double>) =
        ()
    //min ay a
    static member min (a0:Vector<double>, y1:Vector<double>[][]) =
        a0
    //min im i
    static member min (i0:decimal, m1:Vector<double>) =
        i0
    //min kz k
    static member min (k0:single, z1:single[]) =
        k0
    //minabs ay a
    static member minabs (a0:Vector<double>, y1:Vector<double>[][]) =
        a0
    //minabs kz k
    static member minabs (k0:single, z1:single[]) =
        k0
    //minabsaccum aa 
    static member minabsaccum (a0:Vector<double>, a1:Vector<double>) =
        ()
    //minaccum aa 
    static member minaccum (a0:Vector<double>, a1:Vector<double>) =
        ()
    //minarray i[] iI
    static member minarray (i0:decimal, M1:decimal[]) =
        i0
    //minarray k[] kz
    static member minarray (k0:single, M1:decimal[]) =
        k0, 0.0
    //mincer akkkkoo mm
    static member mincer (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        a0, a0
    //mintab k[] kz
    static member mintab (k0:single, M1:decimal[]) =
        k0, 0.0
    //mirror akk a
    static member mirror (a0:Vector<double>, k1:single, k2:single) =
        a0
    //mirror iii i
    static member mirror (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //mirror kkk k
    static member mirror (k0:single, k1:single, k2:single) =
        k0
    //MixerClear  
    static member MixerClear () =
        ()
    //MixerGetLevel ii k
    static member MixerGetLevel (i0:decimal, i1:decimal) =
        0.0f
    //MixerReceive ii a
    static member MixerReceive (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //MixerSend aiii 
    static member MixerSend (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //MixerSetLevel iik 
    static member MixerSetLevel (i0:decimal, i1:decimal, k2:single) =
        ()
    //MixerSetLevel_i iii 
    static member MixerSetLevel_i (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //mode axxo a
    static member mode (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //modmatrix iiiiiik 
    static member modmatrix (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, k6:single) =
        ()
    //monitor  mmmmmmmmmmmmmmmmmmmmmmmm
    static member monitor () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //moog kkkkkkiii a
    static member moog (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal, i8:decimal) =
        vector [(double)0.0]
    //moogladder aaap a
    static member moogladder (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, ?p3:decimal) =
        let p3 = defaultArg p3 1m
        a0
    //moogladder aakp a
    static member moogladder (a0:Vector<double>, a1:Vector<double>, k2:single, ?p3:decimal) =
        let p3 = defaultArg p3 1m
        a0
    //moogladder akap a
    static member moogladder (a0:Vector<double>, k1:single, a2:Vector<double>, ?p3:decimal) =
        let p3 = defaultArg p3 1m
        a0
    //moogladder akkp a
    static member moogladder (a0:Vector<double>, k1:single, k2:single, ?p3:decimal) =
        let p3 = defaultArg p3 1m
        a0
    //moogvcf axxpo a
    static member moogvcf (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?p3:decimal, ?o4:decimal) =
        let p3 = defaultArg p3 1m
        let o4 = defaultArg o4 0m
        a0
    //moogvcf2 axxoo a
    static member moogvcf2 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //moscil kkkkk 
    static member moscil (k0:single, k1:single, k2:single, k3:single, k4:single) =
        ()
    //mp3bitrate S i
    static member mp3bitrate (s0:string) =
        0.0m
    //mp3bitrate i i
    static member mp3bitrate (i0:decimal) =
        i0
    //mp3in Soooo aa
    static member mp3in (s0:string, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0], vector [(double)0.0]
    //mp3in ioooo aa
    static member mp3in (i0:decimal, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0], vector [(double)0.0]
    //mp3len S i
    static member mp3len (s0:string) =
        0.0m
    //mp3len i i
    static member mp3len (i0:decimal) =
        i0
    //mp3nchnls S i
    static member mp3nchnls (s0:string) =
        0.0m
    //mp3nchnls i i
    static member mp3nchnls (i0:decimal) =
        i0
    //mp3sr S i
    static member mp3sr (s0:string) =
        0.0m
    //mp3sr i i
    static member mp3sr (i0:decimal) =
        i0
    //mpulse kko a
    static member mpulse (k0:single, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        vector [(double)0.0]
    //mrtmsg i 
    static member mrtmsg (i0:decimal) =
        ()
    //multitap am a
    static member multitap (a0:Vector<double>, m1:Vector<double>) =
        a0
    //mute So 
    static member mute (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //mute io 
    static member mute (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //mxadsr<'A> iiiioj  -> (a | k)

    static member mxadsr<'A> (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal, ?j5:decimal) =

        let o4 = defaultArg o4 0m

        let j5 = defaultArg j5 -0.1m

        Unchecked.defaultof<'A>
    //nestedap aiiiiooooo a
    static member nestedap (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        a0
    //nlalp akkoo a
    static member nlalp (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //nlfilt akkkkk a
    static member nlfilt (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, k5:single) =
        a0
    //nlfilt2 akkkkk a
    static member nlfilt2 (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, k5:single) =
        a0
    //noise xk a
    static member noise (x0:CSoundOpcode.ScalarOrArray, k1:single) =
        vector [(double)0.0]
    //noteoff iii 
    static member noteoff (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //noteon iii 
    static member noteon (i0:decimal, i1:decimal, i2:decimal) =
        ()
    //noteondur iiii 
    static member noteondur (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //noteondur2 iiii 
    static member noteondur2 (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //notnum  i
    static member notnum () =
        0.0m
    //nreverb akkoojoj a
    static member nreverb (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?o4:decimal, ?j5:decimal, ?o6:decimal, ?j7:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let j5 = defaultArg j5 -0.1m
        let o6 = defaultArg o6 0m
        let j7 = defaultArg j7 -0.1m
        a0
    //nrpn kkk 
    static member nrpn (k0:single, k1:single, k2:single) =
        ()
    //nsamp i i
    static member nsamp (i0:decimal) =
        i0
    //nstance Siim i
    static member nstance (s0:string, i1:decimal, i2:decimal, m3:Vector<double>) =
        i1
    //nstance iiim i
    static member nstance (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        i0
    //nstance SSz k
    static member nstance (s0:string, s1:string, z2:single[]) =
        0.0f
    //nstance kkz k
    static member nstance (k0:single, k1:single, z2:single[]) =
        k0
    //nstrnum S i
    static member nstrnum (s0:string) =
        0.0m
    //nstrnum i i
    static member nstrnum (i0:decimal) =
        i0
    //ntrpol aakop a
    static member ntrpol (a0:Vector<double>, a1:Vector<double>, k2:single, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        a0
    //ntrpol iiiop i
    static member ntrpol (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        i0
    //ntrpol kkkop k
    static member ntrpol (k0:single, k1:single, k2:single, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        k0
    //octave a a
    static member octave (a0:Vector<double>) =
        a0
    //octave i i
    static member octave (i0:decimal) =
        i0
    //octave k k
    static member octave (k0:single) =
        k0
    //octcps i i
    static member octcps (i0:decimal) =
        i0
    //octcps k k
    static member octcps (k0:single) =
        k0
    //octmidi  i
    static member octmidi () =
        0.0m
    //octmidib<'A> o  -> (i | k)

    static member octmidib<'A> (?o0:decimal) =

        let o0 = defaultArg o0 0m

        Unchecked.defaultof<'A>
    //octmidinn i i
    static member octmidinn (i0:decimal) =
        i0
    //octmidinn k k
    static member octmidinn (k0:single) =
        k0
    //octpch i i
    static member octpch (i0:decimal) =
        i0
    //octpch k k
    static member octpch (k0:single) =
        k0
    //opcode  
    static member opcode () =
        ()
    //oscbnk kkkkiikkkkikkkkkkikooooooo a
    static member oscbnk (k0:single, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, k6:single, k7:single, k8:single, k9:single, i10:decimal, k11:single, k12:single, k13:single, k14:single, k15:single, k16:single, i17:decimal, k18:single, ?o19:decimal, ?o20:decimal, ?o21:decimal, ?o22:decimal, ?o23:decimal, ?o24:decimal, ?o25:decimal) =
        let o19 = defaultArg o19 0m
        let o20 = defaultArg o20 0m
        let o21 = defaultArg o21 0m
        let o22 = defaultArg o22 0m
        let o23 = defaultArg o23 0m
        let o24 = defaultArg o24 0m
        let o25 = defaultArg o25 0m
        vector [(double)0.0]
    //oscil aai[]o a
    static member oscil (a0:Vector<double>, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscil aajo a
    static member oscil (a0:Vector<double>, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscil aki[]o a
    static member oscil (a0:Vector<double>, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscil akjo a
    static member oscil (a0:Vector<double>, k1:single, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscil kai[]o a
    static member oscil (k0:single, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a1
    //oscil kajo a
    static member oscil (k0:single, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a1
    //oscil kki[]o a
    static member oscil (k0:single, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //oscil<'A> kkjo  -> (a | s)

    static member oscil<'A> (k0:single, k1:single, ?j2:decimal, ?o3:decimal) =

        let j2 = defaultArg j2 -0.1m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //oscil1 ikij k
    static member oscil1 (i0:decimal, k1:single, i2:decimal, ?j3:decimal) =
        let j3 = defaultArg j3 -0.1m
        k1
    //oscil1i ikij k
    static member oscil1i (i0:decimal, k1:single, i2:decimal, ?j3:decimal) =
        let j3 = defaultArg j3 -0.1m
        k1
    //oscil3 aai[]o a
    static member oscil3 (a0:Vector<double>, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscil3 aajo a
    static member oscil3 (a0:Vector<double>, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscil3 aki[]o a
    static member oscil3 (a0:Vector<double>, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscil3 akjo a
    static member oscil3 (a0:Vector<double>, k1:single, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscil3 kai[]o a
    static member oscil3 (k0:single, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a1
    //oscil3 kajo a
    static member oscil3 (k0:single, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a1
    //oscil3<'A> kki[]o  -> (a | k)

    static member oscil3<'A> (k0:single, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //oscil3<'A> kkjo  -> (a | k)

    static member oscil3<'A> (k0:single, k1:single, ?j2:decimal, ?o3:decimal) =

        let j2 = defaultArg j2 -0.1m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //oscili aai[]o a
    static member oscili (a0:Vector<double>, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscili aajo a
    static member oscili (a0:Vector<double>, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscili aki[]o a
    static member oscili (a0:Vector<double>, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //oscili akjo a
    static member oscili (a0:Vector<double>, k1:single, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //oscili kai[]o a
    static member oscili (k0:single, a1:Vector<double>, i2:decimal, M3:decimal[], ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a1
    //oscili kajo a
    static member oscili (k0:single, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a1
    //oscili<'A> kki[]o  -> (a | k)

    static member oscili<'A> (k0:single, k1:single, i2:decimal, M3:decimal[], ?o4:decimal) =

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //oscili<'A> kkjo  -> (a | k)

    static member oscili<'A> (k0:single, k1:single, ?j2:decimal, ?o3:decimal) =

        let j2 = defaultArg j2 -0.1m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //oscilikt aakoo a
    static member oscilikt (a0:Vector<double>, a1:Vector<double>, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //oscilikt akkoo a
    static member oscilikt (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //oscilikt kakoo a
    static member oscilikt (k0:single, a1:Vector<double>, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a1
    //oscilikt<'A> kkkoo  -> (a | k)

    static member oscilikt<'A> (k0:single, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //osciliktp kkko a
    static member osciliktp (k0:single, k1:single, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        vector [(double)0.0]
    //oscilikts xxkako a
    static member oscilikts (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, k2:single, a3:Vector<double>, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a3
    //osciln kiii a
    static member osciln (k0:single, i1:decimal, i2:decimal, i3:decimal) =
        vector [(double)0.0]
    //oscils iiio a
    static member oscils (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        vector [(double)0.0]
    //oscilx kiii a
    static member oscilx (k0:single, i1:decimal, i2:decimal, i3:decimal) =
        vector [(double)0.0]
    //OSCinit i i
    static member OSCinit (i0:decimal) =
        i0
    //OSClisten iSSN k
    static member OSClisten (i0:decimal, s1:string, s2:string, [<ParamArray>] ?N3:Object[]) =
        let N3 = defaultArg N3 
        0.0f
    //OSCsend kSkSSN 
    static member OSCsend (k0:single, s1:string, k2:single, s3:string, s4:string, [<ParamArray>] ?N5:Object[]) =
        let N5 = defaultArg N5 
        ()
    //out a[] 
    static member out (a0:Vector<double>, M1:decimal[]) =
        ()
    //out y 
    static member out (y0:Vector<double>[][]) =
        ()
    //out32 y 
    static member out32 (y0:Vector<double>[][]) =
        ()
    //outc y 
    static member outc (y0:Vector<double>[][]) =
        ()
    //outch Z 
    static member outch (Z0:single[][]) =
        ()
    //outh y 
    static member outh (y0:Vector<double>[][]) =
        ()
    //outiat iiii 
    static member outiat (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //outic iiiii 
    static member outic (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //outic14 iiiiii 
    static member outic14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal) =
        ()
    //outipat iiiii 
    static member outipat (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //outipb iiii 
    static member outipb (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //outipc iiii 
    static member outipc (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //outkat kkkk 
    static member outkat (k0:single, k1:single, k2:single, k3:single) =
        ()
    //outkc kkkkk 
    static member outkc (k0:single, k1:single, k2:single, k3:single, k4:single) =
        ()
    //outkc14 kkkkkk 
    static member outkc14 (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single) =
        ()
    //outkpat kkkkk 
    static member outkpat (k0:single, k1:single, k2:single, k3:single, k4:single) =
        ()
    //outkpb kkkk 
    static member outkpb (k0:single, k1:single, k2:single, k3:single) =
        ()
    //outkpc kkkk 
    static member outkpc (k0:single, k1:single, k2:single, k3:single) =
        ()
    //outleta Sa 
    static member outleta (s0:string, a1:Vector<double>) =
        ()
    //outletf Sf 
    static member outletf (s0:string, S1:_[]) =
        ()
    //outletk Sk 
    static member outletk (s0:string, k1:single) =
        ()
    //outletkid SSk 
    static member outletkid (s0:string, s1:string, k2:single) =
        ()
    //outletv Sa[] 
    static member outletv (s0:string, a1:Vector<double>, M2:decimal[]) =
        ()
    //outo y 
    static member outo (y0:Vector<double>[][]) =
        ()
    //outq y 
    static member outq (y0:Vector<double>[][]) =
        ()
    //outq1 a 
    static member outq1 (a0:Vector<double>) =
        ()
    //outq2 a 
    static member outq2 (a0:Vector<double>) =
        ()
    //outq3 a 
    static member outq3 (a0:Vector<double>) =
        ()
    //outq4 a 
    static member outq4 (a0:Vector<double>) =
        ()
    //outrg ky 
    static member outrg (k0:single, y1:Vector<double>[][]) =
        ()
    //outs y 
    static member outs (y0:Vector<double>[][]) =
        ()
    //outs1 a 
    static member outs1 (a0:Vector<double>) =
        ()
    //outs2 a 
    static member outs2 (a0:Vector<double>) =
        ()
    //outvalue SS 
    static member outvalue (s0:string, s1:string) =
        ()
    //outvalue Si 
    static member outvalue (s0:string, i1:decimal) =
        ()
    //outvalue Sk 
    static member outvalue (s0:string, k1:single) =
        ()
    //outvalue iS 
    static member outvalue (i0:decimal, s1:string) =
        ()
    //outvalue ii 
    static member outvalue (i0:decimal, i1:decimal) =
        ()
    //outvalue ik 
    static member outvalue (i0:decimal, k1:single) =
        ()
    //outx y 
    static member outx (y0:Vector<double>[][]) =
        ()
    //outz k 
    static member outz (k0:single) =
        ()
    //p i i
    static member p (i0:decimal) =
        i0
    //p k k
    static member p (k0:single) =
        k0
    //pan akkioo aaaa
    static member pan (a0:Vector<double>, k1:single, k2:single, i3:decimal, ?o4:decimal, ?o5:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0, a0, a0, a0
    //pan2 axo aa
    static member pan2 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0, a0
    //pareq akkkoo a
    static member pareq (a0:Vector<double>, k1:single, k2:single, k3:single, ?o4:decimal, ?o5:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0
    //partials ffkkki f
    static member partials (S0:_[], S1:_[], k2:single, k3:single, k4:single, i5:decimal) =
        S0
    //partikkel xkiakiiikkkkikkiiaikikkkikkkkkiaaaakkkkioj ammmmmmm
    static member partikkel (x0:CSoundOpcode.ScalarOrArray, k1:single, i2:decimal, a3:Vector<double>, k4:single, i5:decimal, i6:decimal, i7:decimal, k8:single, k9:single, k10:single, k11:single, i12:decimal, k13:single, k14:single, i15:decimal, i16:decimal, a17:Vector<double>, i18:decimal, k19:single, i20:decimal, k21:single, k22:single, k23:single, i24:decimal, k25:single, k26:single, k27:single, k28:single, k29:single, i30:decimal, a31:Vector<double>, a32:Vector<double>, a33:Vector<double>, a34:Vector<double>, k35:single, k36:single, k37:single, k38:single, i39:decimal, ?o40:decimal, ?j41:decimal) =
        let o40 = defaultArg o40 0m
        let j41 = defaultArg j41 -0.1m
        a3, a3, a3, a3, a3, a3, a3, a3
    //partikkelget ki k
    static member partikkelget (k0:single, i1:decimal) =
        k0
    //partikkelset kki 
    static member partikkelset (k0:single, k1:single, i2:decimal) =
        ()
    //partikkelsync i am
    static member partikkelsync (i0:decimal) =
        vector [(double)0.0], vector [(double)0.0]
    //passign p IIIIIIIIIIIIIIIIIIIIIIII
    static member passign (?p0:decimal) =
        let p0 = defaultArg p0 1m
        ()
    //pcauchy<'A> k  -> (a | i | k)

    static member pcauchy<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //pchbend<'A> jp  -> (i | k)

    static member pchbend<'A> (?j0:decimal, ?p1:decimal) =

        let j0 = defaultArg j0 -0.1m

        let p1 = defaultArg p1 1m

        Unchecked.defaultof<'A>
    //pchmidi  i
    static member pchmidi () =
        0.0m
    //pchmidib<'A> o  -> (i | k)

    static member pchmidib<'A> (?o0:decimal) =

        let o0 = defaultArg o0 0m

        Unchecked.defaultof<'A>
    //pchmidinn i i
    static member pchmidinn (i0:decimal) =
        i0
    //pchmidinn k k
    static member pchmidinn (k0:single) =
        k0
    //pchoct i i
    static member pchoct (i0:decimal) =
        i0
    //pchoct k k
    static member pchoct (k0:single) =
        k0
    //pconvolve aSoo mmmm
    static member pconvolve (a0:Vector<double>, s1:string, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0, a0, a0, a0
    //pconvolve aioo mmmm
    static member pconvolve (a0:Vector<double>, i1:decimal, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0, a0, a0, a0
    //pcount  i
    static member pcount () =
        0.0m
    //pdclip akkop a
    static member pdclip (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        a0
    //pdhalf akop a
    static member pdhalf (a0:Vector<double>, k1:single, ?o2:decimal, ?p3:decimal) =
        let o2 = defaultArg o2 0m
        let p3 = defaultArg p3 1m
        a0
    //pdhalfy akop a
    static member pdhalfy (a0:Vector<double>, k1:single, ?o2:decimal, ?p3:decimal) =
        let o2 = defaultArg o2 0m
        let p3 = defaultArg p3 1m
        a0
    //peak a k
    static member peak (a0:Vector<double>) =
        0.0f
    //peak k k
    static member peak (k0:single) =
        k0
    //pgmassign iSo 
    static member pgmassign (i0:decimal, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //pgmassign iio 
    static member pgmassign (i0:decimal, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //pgmchn o kk
    static member pgmchn (?o0:decimal) =
        let o0 = defaultArg o0 0m
        0.0f, 0.0f
    //phaser1 akkko a
    static member phaser1 (a0:Vector<double>, k1:single, k2:single, k3:single, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //phaser2 akkkkkk a
    static member phaser2 (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single) =
        a0
    //phasor xo a
    static member phasor (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //phasor ko k
    static member phasor (k0:single, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        k0
    //phasorbnk<'A> xkio  -> (a | k)

    static member phasorbnk<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, i2:decimal, ?o3:decimal) =

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //phs k[] k[]
    static member phs (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //pindex i i
    static member pindex (i0:decimal) =
        i0
    //pinker  a
    static member pinker () =
        vector [(double)0.0]
    //pinkish xoooo a
    static member pinkish (x0:CSoundOpcode.ScalarOrArray, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //pitch aiiiiqooooojo kk
    static member pitch (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?q5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal, ?j11:decimal, ?o12:decimal) =
        let q5 = defaultArg q5 10m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        let j11 = defaultArg j11 -0.1m
        let o12 = defaultArg o12 0m
        0.0f, 0.0f
    //pitchac akki k
    static member pitchac (a0:Vector<double>, k1:single, k2:single, i3:decimal) =
        k1
    //pitchamdf aiioppoo kk
    static member pitchamdf (a0:Vector<double>, i1:decimal, i2:decimal, ?o3:decimal, ?p4:decimal, ?p5:decimal, ?o6:decimal, ?o7:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        let p5 = defaultArg p5 1m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        0.0f, 0.0f
    //planet kkkiiiiiiioo aaa
    static member planet (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, ?o10:decimal, ?o11:decimal) =
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //platerev iikiiiiy mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member platerev (i0:decimal, i1:decimal, k2:single, i3:decimal, i4:decimal, i5:decimal, i6:decimal, y7:Vector<double>[][]) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //plltrack akOOOOO aa
    static member plltrack (a0:Vector<double>, k1:single, ?O2:single, ?O3:single, ?O4:single, ?O5:single, ?O6:single) =
        let O2 = defaultArg O2 0.f
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        let O6 = defaultArg O6 0.f
        a0, a0
    //pluck kkiiioo a
    static member pluck (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //poisson<'A> k  -> (a | i | k)

    static member poisson<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //pol2rect k[] k[]
    static member pol2rect (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //pol2rect k[]k[] k[]
    static member pol2rect (k0:single, M1:decimal[], k2:single, M3:decimal[]) =
        k0, M1, 0.0
    //polyaft<'A> ioh  -> (i | k)

    static member polyaft<'A> (i0:decimal, ?o1:decimal, ?h2:decimal) =

        let o1 = defaultArg o1 0m

        let h2 = defaultArg h2 127m

        Unchecked.defaultof<'A>
    //polynomial az a
    static member polynomial (a0:Vector<double>, z1:single[]) =
        a0
    //pop  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
    static member pop () =
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    //pop_f  f
    static member pop_f () =
        0.0
    //port kio k
    static member port (k0:single, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //portk kko k
    static member portk (k0:single, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //poscil aajo a
    static member poscil (a0:Vector<double>, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //poscil akjo a
    static member poscil (a0:Vector<double>, k1:single, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //poscil kajo a
    static member poscil (k0:single, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a1
    //poscil<'A> kkjo  -> (a | k)

    static member poscil<'A> (k0:single, k1:single, ?j2:decimal, ?o3:decimal) =

        let j2 = defaultArg j2 -0.1m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //poscil3 aajo a
    static member poscil3 (a0:Vector<double>, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //poscil3 akjo a
    static member poscil3 (a0:Vector<double>, k1:single, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a0
    //poscil3 kajo a
    static member poscil3 (k0:single, a1:Vector<double>, ?j2:decimal, ?o3:decimal) =
        let j2 = defaultArg j2 -0.1m
        let o3 = defaultArg o3 0m
        a1
    //poscil3<'A> kkjo  -> (a | k)

    static member poscil3<'A> (k0:single, k1:single, ?j2:decimal, ?o3:decimal) =

        let j2 = defaultArg j2 -0.1m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //pow akp a
    static member pow (a0:Vector<double>, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        a0
    //pow iip i
    static member pow (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        i0
    //pow kkp k
    static member pow (k0:single, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        k0
    //powershape akp a
    static member powershape (a0:Vector<double>, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        a0
    //powoftwo a a
    static member powoftwo (a0:Vector<double>) =
        a0
    //powoftwo i i
    static member powoftwo (i0:decimal) =
        i0
    //powoftwo k k
    static member powoftwo (k0:single) =
        k0
    //prealloc Sio 
    static member prealloc (s0:string, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //prealloc iio 
    static member prealloc (i0:decimal, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //prepiano iiiiiikkiiiiiiioo mm
    static member prepiano (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, k6:single, k7:single, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, ?o15:decimal, ?o16:decimal) =
        let o15 = defaultArg o15 0m
        let o16 = defaultArg o16 0m
        vector [(double)0.0], vector [(double)0.0]
    //print m 
    static member print (m0:Vector<double>) =
        ()
    //print_type . 
    static member print_type () =
        ()
    //printf SkUN 
    static member printf (s0:string, k1:single, U2:'a, [<ParamArray>] ?N3:Object[]) =
        let N3 = defaultArg N3 
        ()
    //printf_i SiTN 
    static member printf_i (s0:string, i1:decimal, T2:'a, [<ParamArray>] ?N3:Object[]) =
        let N3 = defaultArg N3 
        ()
    //printk iko 
    static member printk (i0:decimal, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //printk2 ko 
    static member printk2 (k0:single, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //printks SiM 
    static member printks (s0:string, i1:decimal, M2:decimal[]) =
        ()
    //printks iiM 
    static member printks (i0:decimal, i1:decimal, M2:decimal[]) =
        ()
    //printks2 Sk 
    static member printks2 (s0:string, k1:single) =
        ()
    //prints SM 
    static member prints (s0:string, M1:decimal[]) =
        ()
    //prints iM 
    static member prints (i0:decimal, M1:decimal[]) =
        ()
    //product y a
    static member product (y0:Vector<double>[][]) =
        vector [(double)0.0]
    //pset m 
    static member pset (m0:Vector<double>) =
        ()
    //ptable<'A> xiooo  -> (a | k)

    static member ptable<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //ptable iiooo i
    static member ptable (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //ptable3<'A> xiooo  -> (a | k)

    static member ptable3<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //ptable3 iiooo i
    static member ptable3 (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //ptablei<'A> xiooo  -> (a | k)

    static member ptablei<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //ptablei iiooo i
    static member ptablei (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //ptableiw iiiooo 
    static member ptableiw (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //ptablew aaiooo 
    static member ptablew (a0:Vector<double>, a1:Vector<double>, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //ptablew kkiooo 
    static member ptablew (k0:single, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //ptrack aio kk
    static member ptrack (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        0.0f, 0.0f
    //push N 
    static member push ([<ParamArray>] ?N0:Object[]) =
        let N0 = defaultArg N0 
        ()
    //push_f f 
    static member push_f (S0:_[]) =
        ()
    //puts Sko 
    static member puts (s0:string, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //pvadd kkSiiopooo a
    static member pvadd (k0:single, k1:single, s2:string, i3:decimal, i4:decimal, ?o5:decimal, ?p6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal) =
        let o5 = defaultArg o5 0m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        vector [(double)0.0]
    //pvadd kkiiiopooo a
    static member pvadd (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?p6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal) =
        let o5 = defaultArg o5 0m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        vector [(double)0.0]
    //pvbufread kS 
    static member pvbufread (k0:single, s1:string) =
        ()
    //pvbufread ki 
    static member pvbufread (k0:single, i1:decimal) =
        ()
    //pvcross kkSkko a
    static member pvcross (k0:single, k1:single, s2:string, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //pvcross kkikko a
    static member pvcross (k0:single, k1:single, i2:decimal, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //pvinterp kkSkkkkkk a
    static member pvinterp (k0:single, k1:single, s2:string, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single) =
        vector [(double)0.0]
    //pvinterp kkikkkkkk a
    static member pvinterp (k0:single, k1:single, i2:decimal, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single) =
        vector [(double)0.0]
    //pvoc kkSoooo a
    static member pvoc (k0:single, k1:single, s2:string, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //pvoc kkioooo a
    static member pvoc (k0:single, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //pvread kSi kk
    static member pvread (k0:single, s1:string, i2:decimal) =
        k0, k0
    //pvread kii kk
    static member pvread (k0:single, i1:decimal, i2:decimal) =
        k0, k0
    //pvs2array k[]f k
    static member pvs2array (k0:single, M1:decimal[], S2:_[]) =
        k0
    //pvs2tab k[]f k
    static member pvs2tab (k0:single, M1:decimal[], S2:_[]) =
        k0
    //pvsadsyn fikopo a
    static member pvsadsyn (S0:_[], i1:decimal, k2:single, ?o3:decimal, ?p4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //pvsanal aiiiioo f
    static member pvsanal (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        0.0
    //pvsarp fkkk f
    static member pvsarp (S0:_[], k1:single, k2:single, k3:single) =
        S0
    //pvsbandp fxxxxO f
    static member pvsbandp (S0:_[], x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, x4:CSoundOpcode.ScalarOrArray, ?O5:single) =
        let O5 = defaultArg O5 0.f
        S0
    //pvsbandr fxxxxO f
    static member pvsbandr (S0:_[], x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, x4:CSoundOpcode.ScalarOrArray, ?O5:single) =
        let O5 = defaultArg O5 0.f
        S0
    //pvsbin fk ss
    static member pvsbin (S0:_[], k1:single) =
        ()
    //pvsblur fki f
    static member pvsblur (S0:_[], k1:single, i2:decimal) =
        S0
    //pvsbuffer fi ik
    static member pvsbuffer (S0:_[], i1:decimal) =
        i1, 0.0f
    //pvsbufread kkOOo f
    static member pvsbufread (k0:single, k1:single, ?O2:single, ?O3:single, ?o4:decimal) =
        let O2 = defaultArg O2 0.f
        let O3 = defaultArg O3 0.f
        let o4 = defaultArg o4 0m
        0.0
    //pvsbufread2 kkkk f
    static member pvsbufread2 (k0:single, k1:single, k2:single, k3:single) =
        0.0
    //pvscale fkOPO f
    static member pvscale (S0:_[], k1:single, ?O2:single) =
        let O2 = defaultArg O2 0.f
        S0
    //pvscale fxOPO f
    static member pvscale (S0:_[], x1:CSoundOpcode.ScalarOrArray, ?O2:single) =
        let O2 = defaultArg O2 0.f
        S0
    //pvscent f s
    static member pvscent (S0:_[]) =
        ()
    //pvsceps fo k[]
    static member pvsceps (S0:_[], ?o1:decimal) =
        let o1 = defaultArg o1 0m
        0.0f, [|0.0m|], 0.0
    //pvscross ffkk f
    static member pvscross (S0:_[], S1:_[], k2:single, k3:single) =
        S0
    //pvsdemix ffkki f
    static member pvsdemix (S0:_[], S1:_[], k2:single, k3:single, i4:decimal) =
        S0
    //pvsdiskin SkkopP f
    static member pvsdiskin (s0:string, k1:single, k2:single, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        0.0
    //pvsdiskin ikkopP f
    static member pvsdiskin (i0:decimal, k1:single, k2:single, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        0.0
    //pvsdisp foo 
    static member pvsdisp (S0:_[], ?o1:decimal, ?o2:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        ()
    //pvsenvftw fkPPO k
    static member pvsenvftw (S0:_[], k1:single) =
        k1
    //pvsfilter fffp f
    static member pvsfilter (S0:_[], S1:_[], S2:_[], ?p3:decimal) =
        let p3 = defaultArg p3 1m
        S0
    //pvsfilter ffxp f
    static member pvsfilter (S0:_[], S1:_[], x2:CSoundOpcode.ScalarOrArray, ?p3:decimal) =
        let p3 = defaultArg p3 1m
        S0
    //pvsfread kSo f
    static member pvsfread (k0:single, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        0.0
    //pvsfread kio f
    static member pvsfread (k0:single, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        0.0
    //pvsfreeze fkk f
    static member pvsfreeze (S0:_[], k1:single, k2:single) =
        S0
    //pvsfromarray k[]oop f
    static member pvsfromarray (k0:single, M1:decimal[], ?o2:decimal, ?o3:decimal, ?p4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        0.0
    //pvsftr fio 
    static member pvsftr (S0:_[], i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //pvsftw fio k
    static member pvsftw (S0:_[], i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        0.0f
    //pvsfwrite fS 
    static member pvsfwrite (S0:_[], s1:string) =
        ()
    //pvsfwrite fi 
    static member pvsfwrite (S0:_[], i1:decimal) =
        ()
    //pvsgain fk f
    static member pvsgain (S0:_[], k1:single) =
        S0
    //pvsgendy fkk f
    static member pvsgendy (S0:_[], k1:single, k2:single) =
        S0
    //pvshift fkkOPO f
    static member pvshift (S0:_[], k1:single, k2:single, ?O3:single) =
        let O3 = defaultArg O3 0.f
        S0
    //pvshift fxkOPO f
    static member pvshift (S0:_[], x1:CSoundOpcode.ScalarOrArray, k2:single, ?O3:single) =
        let O3 = defaultArg O3 0.f
        S0
    //pvsifd aiiip ff
    static member pvsifd (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, ?p4:decimal) =
        let p4 = defaultArg p4 1m
        0.0, 0.0
    //pvsin kooopo f
    static member pvsin (k0:single, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?p4:decimal, ?o5:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        let o5 = defaultArg o5 0m
        0.0
    //pvsinfo f iiii
    static member pvsinfo (S0:_[]) =
        0.0m, 0.0m, 0.0m, 0.0m
    //pvsinit ioopo f
    static member pvsinit (i0:decimal, ?o1:decimal, ?o2:decimal, ?p3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let p3 = defaultArg p3 1m
        let o4 = defaultArg o4 0m
        0.0
    //pvslock fk f
    static member pvslock (S0:_[], k1:single) =
        S0
    //pvsmaska fik f
    static member pvsmaska (S0:_[], i1:decimal, k2:single) =
        S0
    //pvsmix ff f
    static member pvsmix (S0:_[], S1:_[]) =
        S0
    //pvsmooth fxx f
    static member pvsmooth (S0:_[], x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray) =
        S0
    //pvsmorph ffkk f
    static member pvsmorph (S0:_[], S1:_[], k2:single, k3:single) =
        S0
    //pvsosc kkkioopo f
    static member pvsosc (k0:single, k1:single, k2:single, i3:decimal, ?o4:decimal, ?o5:decimal, ?p6:decimal, ?o7:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let p6 = defaultArg p6 1m
        let o7 = defaultArg o7 0m
        0.0
    //pvsout fk 
    static member pvsout (S0:_[], k1:single) =
        ()
    //pvspitch fk kk
    static member pvspitch (S0:_[], k1:single) =
        k1, k1
    //pvstanal kkkkPPoooP FFFFFFFFFFFFFFFF
    static member pvstanal (k0:single, k1:single, k2:single, k3:single) =
        ()
    //pvstencil fkki f
    static member pvstencil (S0:_[], k1:single, k2:single, i3:decimal) =
        S0
    //pvsvoc ffkkO f
    static member pvsvoc (S0:_[], S1:_[], k2:single, k3:single, ?O4:single) =
        let O4 = defaultArg O4 0.f
        S0
    //pvswarp fkkOPPO f
    static member pvswarp (S0:_[], k1:single, k2:single, ?O3:single) =
        let O3 = defaultArg O3 0.f
        S0
    //pvsynth fo a
    static member pvsynth (S0:_[], ?o1:decimal) =
        let o1 = defaultArg o1 0m
        vector [(double)0.0]
    //pwd  S
    static member pwd () =
        0.0
    //pyassign Sz 
    static member pyassign (s0:string, z1:single[]) =
        ()
    //pyassigni Sz 
    static member pyassigni (s0:string, z1:single[]) =
        ()
    //pyassignt Sz 
    static member pyassignt (s0:string, z1:single[]) =
        ()
    //pycall Sz 
    static member pycall (s0:string, z1:single[]) =
        ()
    //pycall1 Sz k
    static member pycall1 (s0:string, z1:single[]) =
        0.0f
    //pycall1i Sm i
    static member pycall1i (s0:string, m1:Vector<double>) =
        0.0m
    //pycall1t kSz k
    static member pycall1t (k0:single, s1:string, z2:single[]) =
        k0
    //pycall2 Sz kk
    static member pycall2 (s0:string, z1:single[]) =
        0.0f, 0.0f
    //pycall2i Sm ii
    static member pycall2i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m
    //pycall2t kSz kk
    static member pycall2t (k0:single, s1:string, z2:single[]) =
        k0, k0
    //pycall3 Sz kkk
    static member pycall3 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f
    //pycall3i Sm iii
    static member pycall3i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m
    //pycall3t kSz kkk
    static member pycall3t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0
    //pycall4 Sz kkkk
    static member pycall4 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f
    //pycall4i Sm iiii
    static member pycall4i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m
    //pycall4t kSz kkkk
    static member pycall4t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0
    //pycall5 Sz kkkkk
    static member pycall5 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pycall5i Sm iiiii
    static member pycall5i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pycall5t kSz kkkkk
    static member pycall5t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0
    //pycall6 Sz kkkkkk
    static member pycall6 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pycall6i Sm iiiiii
    static member pycall6i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pycall6t kSz kkkkkk
    static member pycall6t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0
    //pycall7 Sz kkkkkkk
    static member pycall7 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pycall7i Sm iiiiiii
    static member pycall7i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pycall7t kSz kkkkkkk
    static member pycall7t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0, k0
    //pycall8 Sz kkkkkkkk
    static member pycall8 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pycall8i Sm iiiiiiii
    static member pycall8i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pycall8t kSz kkkkkkkk
    static member pycall8t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0, k0, k0
    //pycalli Sm 
    static member pycalli (s0:string, m1:Vector<double>) =
        ()
    //pycalln Siz 
    static member pycalln (s0:string, i1:decimal, z2:single[]) =
        ()
    //pycallni Sim 
    static member pycallni (s0:string, i1:decimal, m2:Vector<double>) =
        ()
    //pycallt kSz 
    static member pycallt (k0:single, s1:string, z2:single[]) =
        ()
    //pyeval S k
    static member pyeval (s0:string) =
        0.0f
    //pyevali S i
    static member pyevali (s0:string) =
        0.0m
    //pyevalt S k
    static member pyevalt (s0:string) =
        0.0f
    //pyexec S 
    static member pyexec (s0:string) =
        ()
    //pyexeci S 
    static member pyexeci (s0:string) =
        ()
    //pyexect kS 
    static member pyexect (k0:single, s1:string) =
        ()
    //pyinit  
    static member pyinit () =
        ()
    //pylassign Sz 
    static member pylassign (s0:string, z1:single[]) =
        ()
    //pylassigni Sz 
    static member pylassigni (s0:string, z1:single[]) =
        ()
    //pylassignt Sz 
    static member pylassignt (s0:string, z1:single[]) =
        ()
    //pylcall Sz 
    static member pylcall (s0:string, z1:single[]) =
        ()
    //pylcall1 Sz k
    static member pylcall1 (s0:string, z1:single[]) =
        0.0f
    //pylcall1i Sm i
    static member pylcall1i (s0:string, m1:Vector<double>) =
        0.0m
    //pylcall1t kSz k
    static member pylcall1t (k0:single, s1:string, z2:single[]) =
        k0
    //pylcall2 Sz kk
    static member pylcall2 (s0:string, z1:single[]) =
        0.0f, 0.0f
    //pylcall2i Sm ii
    static member pylcall2i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m
    //pylcall2t kSz kk
    static member pylcall2t (k0:single, s1:string, z2:single[]) =
        k0, k0
    //pylcall3 Sz kkk
    static member pylcall3 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f
    //pylcall3i Sm iii
    static member pylcall3i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m
    //pylcall3t kSz kkk
    static member pylcall3t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0
    //pylcall4 Sz kkkk
    static member pylcall4 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f
    //pylcall4i Sm iiii
    static member pylcall4i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m
    //pylcall4t kSz kkkk
    static member pylcall4t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0
    //pylcall5 Sz kkkkk
    static member pylcall5 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pylcall5i Sm iiiii
    static member pylcall5i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pylcall5t kSz kkkkk
    static member pylcall5t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0
    //pylcall6 Sz kkkkkk
    static member pylcall6 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pylcall6i Sm iiiiii
    static member pylcall6i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pylcall6t kSz kkkkkk
    static member pylcall6t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0
    //pylcall7 Sz kkkkkkk
    static member pylcall7 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pylcall7i Sm iiiiiii
    static member pylcall7i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pylcall7t kSz kkkkkkk
    static member pylcall7t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0, k0
    //pylcall8 Sz kkkkkkkk
    static member pylcall8 (s0:string, z1:single[]) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //pylcall8i Sm iiiiiiii
    static member pylcall8i (s0:string, m1:Vector<double>) =
        0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m, 0.0m
    //pylcall8t kSz kkkkkkkk
    static member pylcall8t (k0:single, s1:string, z2:single[]) =
        k0, k0, k0, k0, k0, k0, k0, k0
    //pylcalli Sm 
    static member pylcalli (s0:string, m1:Vector<double>) =
        ()
    //pylcalln Siz 
    static member pylcalln (s0:string, i1:decimal, z2:single[]) =
        ()
    //pylcallni Sim 
    static member pylcallni (s0:string, i1:decimal, m2:Vector<double>) =
        ()
    //pylcallt kSz 
    static member pylcallt (k0:single, s1:string, z2:single[]) =
        ()
    //pyleval S k
    static member pyleval (s0:string) =
        0.0f
    //pylevali S i
    static member pylevali (s0:string) =
        0.0m
    //pylevalt S k
    static member pylevalt (s0:string) =
        0.0f
    //pylexec S 
    static member pylexec (s0:string) =
        ()
    //pylexeci S 
    static member pylexeci (s0:string) =
        ()
    //pylexect kS 
    static member pylexect (k0:single, s1:string) =
        ()
    //pylrun S 
    static member pylrun (s0:string) =
        ()
    //pylruni S 
    static member pylruni (s0:string) =
        ()
    //pylrunt kS 
    static member pylrunt (k0:single, s1:string) =
        ()
    //pyrun S 
    static member pyrun (s0:string) =
        ()
    //pyruni S 
    static member pyruni (s0:string) =
        ()
    //pyrunt kS 
    static member pyrunt (k0:single, s1:string) =
        ()
    //qinf a a
    static member qinf (a0:Vector<double>) =
        a0
    //qinf i i
    static member qinf (i0:decimal) =
        i0
    //qinf k k
    static member qinf (k0:single) =
        k0
    //qnan a a
    static member qnan (a0:Vector<double>) =
        a0
    //qnan i i
    static member qnan (i0:decimal) =
        i0
    //qnan k k
    static member qnan (k0:single) =
        k0
    //r2c k[] k[]
    static member r2c (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //rand<'A> xvoo  -> (a | k)

    static member rand<'A> (x0:CSoundOpcode.ScalarOrArray, ?v1:decimal, ?o2:decimal, ?o3:decimal) =

        let v1 = defaultArg v1 0.5m

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //randh<'A> xxvoo  -> (a | k)

    static member randh<'A> (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, ?v2:decimal, ?o3:decimal, ?o4:decimal) =

        let v2 = defaultArg v2 0.5m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //randi<'A> xxvoo  -> (a | k)

    static member randi<'A> (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, ?v2:decimal, ?o3:decimal, ?o4:decimal) =

        let v2 = defaultArg v2 0.5m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //random<'A> kk  -> (a | k)

    static member random<'A> (k0:single, k1:single) =

        Unchecked.defaultof<'A>
    //random ii i
    static member random (i0:decimal, i1:decimal) =
        i0
    //randomh kkxoo a
    static member randomh (k0:single, k1:single, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //randomh kkkoo k
    static member randomh (k0:single, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        k0
    //randomi kkxoo a
    static member randomi (k0:single, k1:single, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //randomi kkkoo k
    static member randomi (k0:single, k1:single, k2:single, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        k0
    //rbjeq akkkko a
    static member rbjeq (a0:Vector<double>, k1:single, k2:single, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0
    //readclock i i
    static member readclock (i0:decimal) =
        i0
    //readf S Sk
    static member readf (s0:string) =
        s0, 0.0f
    //readf i Sk
    static member readf (i0:decimal) =
        0.0, 0.0f
    //readfi S Si
    static member readfi (s0:string) =
        s0, 0.0m
    //readfi i Si
    static member readfi (i0:decimal) =
        0.0, i0
    //readk Sii k
    static member readk (s0:string, i1:decimal, i2:decimal) =
        0.0f
    //readk iii k
    static member readk (i0:decimal, i1:decimal, i2:decimal) =
        0.0f
    //readk2 Sii kk
    static member readk2 (s0:string, i1:decimal, i2:decimal) =
        0.0f, 0.0f
    //readk2 iii kk
    static member readk2 (i0:decimal, i1:decimal, i2:decimal) =
        0.0f, 0.0f
    //readk3 Sii kkk
    static member readk3 (s0:string, i1:decimal, i2:decimal) =
        0.0f, 0.0f, 0.0f
    //readk3 iii kkk
    static member readk3 (i0:decimal, i1:decimal, i2:decimal) =
        0.0f, 0.0f, 0.0f
    //readk4 Sii kkkk
    static member readk4 (s0:string, i1:decimal, i2:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f
    //readk4 iii kkkk
    static member readk4 (i0:decimal, i1:decimal, i2:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f
    //readks Si S
    static member readks (s0:string, i1:decimal) =
        s0
    //readks ii S
    static member readks (i0:decimal, i1:decimal) =
        0.0
    //readscore S i
    static member readscore (s0:string) =
        0.0m
    //readscratch o i
    static member readscratch (?o0:decimal) =
        let o0 = defaultArg o0 0m
        o0
    //rect2pol k[] k[]
    static member rect2pol (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //reinit l 
    static member reinit (l0:_) =
        ()
    //release  k
    static member release () =
        0.0f
    //remoteport i 
    static member remoteport (i0:decimal) =
        ()
    //remove T 
    static member remove (T0:'a) =
        ()
    //repluck ikikka a
    static member repluck (i0:decimal, k1:single, i2:decimal, k3:single, k4:single, a5:Vector<double>) =
        a5
    //reson axxoo a
    static member reson (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //resonk kkkpo k
    static member resonk (k0:single, k1:single, k2:single, ?p3:decimal, ?o4:decimal) =
        let p3 = defaultArg p3 1m
        let o4 = defaultArg o4 0m
        k0
    //resonr axxoo a
    static member resonr (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //resonx axxooo a
    static member resonx (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0
    //resonxk kkkooo k
    static member resonxk (k0:single, k1:single, k2:single, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        k0
    //resony akkikooo a
    static member resony (a0:Vector<double>, k1:single, k2:single, i3:decimal, k4:single, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        a0
    //resonz axxoo a
    static member resonz (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //resyn fkkki a
    static member resyn (S0:_[], k1:single, k2:single, k3:single, i4:decimal) =
        vector [(double)0.0]
    //reverb ako a
    static member reverb (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //reverb2 akkoojoj a
    static member reverb2 (a0:Vector<double>, k1:single, k2:single, ?o3:decimal, ?o4:decimal, ?j5:decimal, ?o6:decimal, ?j7:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let j5 = defaultArg j5 -0.1m
        let o6 = defaultArg o6 0m
        let j7 = defaultArg j7 -0.1m
        a0
    //reverbsc aakkjpo aa
    static member reverbsc (a0:Vector<double>, a1:Vector<double>, k2:single, k3:single, ?j4:decimal, ?p5:decimal, ?o6:decimal) =
        let j4 = defaultArg j4 -0.1m
        let p5 = defaultArg p5 1m
        let o6 = defaultArg o6 0m
        a0, a0
    //rewindscore  
    static member rewindscore () =
        ()
    //rezzy axxoo a
    static member rezzy (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        a0
    //rfft k[] k[]
    static member rfft (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //rifft k[] k[]
    static member rifft (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //rigoto l 
    static member rigoto (l0:_) =
        ()
    //rireturn  
    static member rireturn () =
        ()
    //rms aqo k
    static member rms (a0:Vector<double>, ?q1:decimal, ?o2:decimal) =
        let q1 = defaultArg q1 10m
        let o2 = defaultArg o2 0m
        0.0f
    //rnd i i
    static member rnd (i0:decimal) =
        i0
    //rnd k k
    static member rnd (k0:single) =
        k0
    //rnd31<'A> kko  -> (a | k)

    static member rnd31<'A> (k0:single, k1:single, ?o2:decimal) =

        let o2 = defaultArg o2 0m

        Unchecked.defaultof<'A>
    //rnd31 iio i
    static member rnd31 (i0:decimal, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        i0
    //round a a
    static member round (a0:Vector<double>) =
        a0
    //round i i
    static member round (i0:decimal) =
        i0
    //round k k
    static member round (k0:single) =
        k0
    //rspline<'A> xxkk  -> (a | k)

    static member rspline<'A> (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, k2:single, k3:single) =

        Unchecked.defaultof<'A>
    //rtclock<'A>   -> (i | k)

    static member rtclock<'A> () =

        Unchecked.defaultof<'A>
    //s16b14 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiii
    static member s16b14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0
    //s16b14 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkk
    static member s16b14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //s32b14 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
    static member s32b14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0
    //s32b14 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
    static member s32b14 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //samphold<'A> xxoo  -> (a | k)

    static member samphold<'A> (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, ?o2:decimal, ?o3:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>
    //sandpaper iiooo a
    static member sandpaper (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //scale kkk k
    static member scale (k0:single, k1:single, k2:single) =
        k0
    //scalearray i[]iiOJ 
    static member scalearray (i0:decimal, M1:decimal[], i2:decimal, i3:decimal, ?O4:single) =
        let O4 = defaultArg O4 0.f
        ()
    //scalearray k[]kkOJ 
    static member scalearray (k0:single, M1:decimal[], k2:single, k3:single, ?O4:single) =
        let O4 = defaultArg O4 0.f
        ()
    //scalet k[]kkOJ 
    static member scalet (k0:single, M1:decimal[], k2:single, k3:single, ?O4:single) =
        let O4 = defaultArg O4 0.f
        ()
    //scanhammer iiii 
    static member scanhammer (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        ()
    //scans kkiio a
    static member scans (k0:single, k1:single, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //scantable kkiiiii a
    static member scantable (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal) =
        vector [(double)0.0]
    //scanu iiiiiiikkkkiikkaii 
    static member scanu (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, k7:single, k8:single, k9:single, k10:single, i11:decimal, i12:decimal, k13:single, k14:single, a15:Vector<double>, i16:decimal, i17:decimal) =
        ()
    //schedkwhen kkkSkz 
    static member schedkwhen (k0:single, k1:single, k2:single, s3:string, k4:single, z5:single[]) =
        ()
    //schedkwhen kkkkkz 
    static member schedkwhen (k0:single, k1:single, k2:single, k3:single, k4:single, z5:single[]) =
        ()
    //schedkwhennamed kkkSkz 
    static member schedkwhennamed (k0:single, k1:single, k2:single, s3:string, k4:single, z5:single[]) =
        ()
    //schedkwhennamed kkkkkz 
    static member schedkwhennamed (k0:single, k1:single, k2:single, k3:single, k4:single, z5:single[]) =
        ()
    //schedule Siim 
    static member schedule (s0:string, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //schedule iiim 
    static member schedule (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //schedwhen kSkkm 
    static member schedwhen (k0:single, s1:string, k2:single, k3:single, m4:Vector<double>) =
        ()
    //schedwhen kkkkm 
    static member schedwhen (k0:single, k1:single, k2:single, k3:single, m4:Vector<double>) =
        ()
    //scoreline Sk 
    static member scoreline (s0:string, k1:single) =
        ()
    //scoreline_i S 
    static member scoreline_i (s0:string) =
        ()
    //seed i 
    static member seed (i0:decimal) =
        ()
    //sekere iiooo a
    static member sekere (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //semitone a a
    static member semitone (a0:Vector<double>) =
        a0
    //semitone i i
    static member semitone (i0:decimal) =
        i0
    //semitone k k
    static member semitone (k0:single) =
        k0
    //sense  kz
    static member sense () =
        0.0f, 0.0
    //sensekey  kz
    static member sensekey () =
        0.0f, 0.0
    //seqtime kkkkk k
    static member seqtime (k0:single, k1:single, k2:single, k3:single, k4:single) =
        k0
    //seqtime2 kkkkkk k
    static member seqtime2 (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single) =
        k0
    //serialBegin So i
    static member serialBegin (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        o1
    //serialEnd i 
    static member serialEnd (i0:decimal) =
        ()
    //serialFlush i 
    static member serialFlush (i0:decimal) =
        ()
    //serialPrint i 
    static member serialPrint (i0:decimal) =
        ()
    //serialRead i k
    static member serialRead (i0:decimal) =
        0.0f
    //serialWrite iS 
    static member serialWrite (i0:decimal, s1:string) =
        ()
    //serialWrite ik 
    static member serialWrite (i0:decimal, k1:single) =
        ()
    //serialWrite_i iS 
    static member serialWrite_i (i0:decimal, s1:string) =
        ()
    //serialWrite_i ii 
    static member serialWrite_i (i0:decimal, i1:decimal) =
        ()
    //setcol k[]k k[]
    static member setcol (k0:single, M1:decimal[], k2:single) =
        k0, M1, 0.0
    //setksmps i 
    static member setksmps (i0:decimal) =
        ()
    //setrow k[]k k[]
    static member setrow (k0:single, M1:decimal[], k2:single) =
        k0, M1, 0.0
    //setscorepos i 
    static member setscorepos (i0:decimal) =
        ()
    //sfilist i 
    static member sfilist (i0:decimal) =
        ()
    //sfinstr iixxiiooo aa
    static member sfinstr (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //sfinstr3 iixxiiooo aa
    static member sfinstr3 (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0], vector [(double)0.0]
    //sfinstr3m iixxiiooo a
    static member sfinstr3m (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0]
    //sfinstrm iixxiiooo a
    static member sfinstrm (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, i5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0]
    //sfload S i
    static member sfload (s0:string) =
        0.0m
    //sfload i i
    static member sfload (i0:decimal) =
        i0
    //sflooper iikkikkkoooo aa
    static member sflooper (i0:decimal, i1:decimal, k2:single, k3:single, i4:decimal, k5:single, k6:single, k7:single, ?o8:decimal, ?o9:decimal, ?o10:decimal, ?o11:decimal) =
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        vector [(double)0.0], vector [(double)0.0]
    //sfpassign iip 
    static member sfpassign (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
    //sfplay iixxiooo aa
    static member sfplay (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0], vector [(double)0.0]
    //sfplay3 iixxiooo aa
    static member sfplay3 (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0], vector [(double)0.0]
    //sfplay3m iixxiooo a
    static member sfplay3m (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //sfplaym iixxiooo a
    static member sfplaym (i0:decimal, i1:decimal, x2:CSoundOpcode.ScalarOrArray, x3:CSoundOpcode.ScalarOrArray, i4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //sfplist i 
    static member sfplist (i0:decimal) =
        ()
    //sfpreset iiii i
    static member sfpreset (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        i0
    //shaker kkkkko a
    static member shaker (k0:single, k1:single, k2:single, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //shiftin a k[]
    static member shiftin (a0:Vector<double>) =
        0.0f, [|0.0m|], 0.0
    //shiftout k[]o a
    static member shiftout (k0:single, M1:decimal[], ?o2:decimal) =
        let o2 = defaultArg o2 0m
        vector [(double)0.0]
    //signum a a
    static member signum (a0:Vector<double>) =
        a0
    //signum i i
    static member signum (i0:decimal) =
        i0
    //signum k k
    static member signum (k0:single) =
        k0
    //sin a a
    static member sin (a0:Vector<double>) =
        a0
    //sin i i
    static member sin (i0:decimal) =
        i0
    //sin k k
    static member sin (k0:single) =
        k0
    //sinh a a
    static member sinh (a0:Vector<double>) =
        a0
    //sinh i i
    static member sinh (i0:decimal) =
        i0
    //sinh k k
    static member sinh (k0:single) =
        k0
    //sininv a a
    static member sininv (a0:Vector<double>) =
        a0
    //sininv i i
    static member sininv (i0:decimal) =
        i0
    //sininv k k
    static member sininv (k0:single) =
        k0
    //sinsyn fkki a
    static member sinsyn (S0:_[], k1:single, k2:single, i3:decimal) =
        vector [(double)0.0]
    //sleighbells kioooooo a
    static member sleighbells (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //slicearray .[]ii .[]
    static member slicearray (M0:decimal[], i1:decimal, i2:decimal) =
        0.0, M0, 0.0
    //slicearray i[]ii i[]
    static member slicearray (i0:decimal, M1:decimal[], i2:decimal, i3:decimal) =
        i0, M1, 0.0
    //slider16 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiii
    static member slider16 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0
    //slider16 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkk
    static member slider16 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider16f iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkk
    static member slider16f (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider16table iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider16table (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal) =
        0.0f
    //slider16tablef iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider16tablef (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal) =
        0.0f
    //slider32 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
    static member slider32 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0
    //slider32 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
    static member slider32 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider32f iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
    static member slider32f (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider32table iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider32table (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal) =
        0.0f
    //slider32tablef iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider32tablef (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal) =
        0.0f
    //slider64 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
    static member slider64 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal, i195:decimal, i196:decimal, i197:decimal, i198:decimal, i199:decimal, i200:decimal, i201:decimal, i202:decimal, i203:decimal, i204:decimal, i205:decimal, i206:decimal, i207:decimal, i208:decimal, i209:decimal, i210:decimal, i211:decimal, i212:decimal, i213:decimal, i214:decimal, i215:decimal, i216:decimal, i217:decimal, i218:decimal, i219:decimal, i220:decimal, i221:decimal, i222:decimal, i223:decimal, i224:decimal, i225:decimal, i226:decimal, i227:decimal, i228:decimal, i229:decimal, i230:decimal, i231:decimal, i232:decimal, i233:decimal, i234:decimal, i235:decimal, i236:decimal, i237:decimal, i238:decimal, i239:decimal, i240:decimal, i241:decimal, i242:decimal, i243:decimal, i244:decimal, i245:decimal, i246:decimal, i247:decimal, i248:decimal, i249:decimal, i250:decimal, i251:decimal, i252:decimal, i253:decimal, i254:decimal, i255:decimal, i256:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0, i0
    //slider64 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
    static member slider64 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal, i195:decimal, i196:decimal, i197:decimal, i198:decimal, i199:decimal, i200:decimal, i201:decimal, i202:decimal, i203:decimal, i204:decimal, i205:decimal, i206:decimal, i207:decimal, i208:decimal, i209:decimal, i210:decimal, i211:decimal, i212:decimal, i213:decimal, i214:decimal, i215:decimal, i216:decimal, i217:decimal, i218:decimal, i219:decimal, i220:decimal, i221:decimal, i222:decimal, i223:decimal, i224:decimal, i225:decimal, i226:decimal, i227:decimal, i228:decimal, i229:decimal, i230:decimal, i231:decimal, i232:decimal, i233:decimal, i234:decimal, i235:decimal, i236:decimal, i237:decimal, i238:decimal, i239:decimal, i240:decimal, i241:decimal, i242:decimal, i243:decimal, i244:decimal, i245:decimal, i246:decimal, i247:decimal, i248:decimal, i249:decimal, i250:decimal, i251:decimal, i252:decimal, i253:decimal, i254:decimal, i255:decimal, i256:decimal, i257:decimal, i258:decimal, i259:decimal, i260:decimal, i261:decimal, i262:decimal, i263:decimal, i264:decimal, i265:decimal, i266:decimal, i267:decimal, i268:decimal, i269:decimal, i270:decimal, i271:decimal, i272:decimal, i273:decimal, i274:decimal, i275:decimal, i276:decimal, i277:decimal, i278:decimal, i279:decimal, i280:decimal, i281:decimal, i282:decimal, i283:decimal, i284:decimal, i285:decimal, i286:decimal, i287:decimal, i288:decimal, i289:decimal, i290:decimal, i291:decimal, i292:decimal, i293:decimal, i294:decimal, i295:decimal, i296:decimal, i297:decimal, i298:decimal, i299:decimal, i300:decimal, i301:decimal, i302:decimal, i303:decimal, i304:decimal, i305:decimal, i306:decimal, i307:decimal, i308:decimal, i309:decimal, i310:decimal, i311:decimal, i312:decimal, i313:decimal, i314:decimal, i315:decimal, i316:decimal, i317:decimal, i318:decimal, i319:decimal, i320:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider64f iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
    static member slider64f (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal, i195:decimal, i196:decimal, i197:decimal, i198:decimal, i199:decimal, i200:decimal, i201:decimal, i202:decimal, i203:decimal, i204:decimal, i205:decimal, i206:decimal, i207:decimal, i208:decimal, i209:decimal, i210:decimal, i211:decimal, i212:decimal, i213:decimal, i214:decimal, i215:decimal, i216:decimal, i217:decimal, i218:decimal, i219:decimal, i220:decimal, i221:decimal, i222:decimal, i223:decimal, i224:decimal, i225:decimal, i226:decimal, i227:decimal, i228:decimal, i229:decimal, i230:decimal, i231:decimal, i232:decimal, i233:decimal, i234:decimal, i235:decimal, i236:decimal, i237:decimal, i238:decimal, i239:decimal, i240:decimal, i241:decimal, i242:decimal, i243:decimal, i244:decimal, i245:decimal, i246:decimal, i247:decimal, i248:decimal, i249:decimal, i250:decimal, i251:decimal, i252:decimal, i253:decimal, i254:decimal, i255:decimal, i256:decimal, i257:decimal, i258:decimal, i259:decimal, i260:decimal, i261:decimal, i262:decimal, i263:decimal, i264:decimal, i265:decimal, i266:decimal, i267:decimal, i268:decimal, i269:decimal, i270:decimal, i271:decimal, i272:decimal, i273:decimal, i274:decimal, i275:decimal, i276:decimal, i277:decimal, i278:decimal, i279:decimal, i280:decimal, i281:decimal, i282:decimal, i283:decimal, i284:decimal, i285:decimal, i286:decimal, i287:decimal, i288:decimal, i289:decimal, i290:decimal, i291:decimal, i292:decimal, i293:decimal, i294:decimal, i295:decimal, i296:decimal, i297:decimal, i298:decimal, i299:decimal, i300:decimal, i301:decimal, i302:decimal, i303:decimal, i304:decimal, i305:decimal, i306:decimal, i307:decimal, i308:decimal, i309:decimal, i310:decimal, i311:decimal, i312:decimal, i313:decimal, i314:decimal, i315:decimal, i316:decimal, i317:decimal, i318:decimal, i319:decimal, i320:decimal, i321:decimal, i322:decimal, i323:decimal, i324:decimal, i325:decimal, i326:decimal, i327:decimal, i328:decimal, i329:decimal, i330:decimal, i331:decimal, i332:decimal, i333:decimal, i334:decimal, i335:decimal, i336:decimal, i337:decimal, i338:decimal, i339:decimal, i340:decimal, i341:decimal, i342:decimal, i343:decimal, i344:decimal, i345:decimal, i346:decimal, i347:decimal, i348:decimal, i349:decimal, i350:decimal, i351:decimal, i352:decimal, i353:decimal, i354:decimal, i355:decimal, i356:decimal, i357:decimal, i358:decimal, i359:decimal, i360:decimal, i361:decimal, i362:decimal, i363:decimal, i364:decimal, i365:decimal, i366:decimal, i367:decimal, i368:decimal, i369:decimal, i370:decimal, i371:decimal, i372:decimal, i373:decimal, i374:decimal, i375:decimal, i376:decimal, i377:decimal, i378:decimal, i379:decimal, i380:decimal, i381:decimal, i382:decimal, i383:decimal, i384:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider64table iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider64table (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal, i195:decimal, i196:decimal, i197:decimal, i198:decimal, i199:decimal, i200:decimal, i201:decimal, i202:decimal, i203:decimal, i204:decimal, i205:decimal, i206:decimal, i207:decimal, i208:decimal, i209:decimal, i210:decimal, i211:decimal, i212:decimal, i213:decimal, i214:decimal, i215:decimal, i216:decimal, i217:decimal, i218:decimal, i219:decimal, i220:decimal, i221:decimal, i222:decimal, i223:decimal, i224:decimal, i225:decimal, i226:decimal, i227:decimal, i228:decimal, i229:decimal, i230:decimal, i231:decimal, i232:decimal, i233:decimal, i234:decimal, i235:decimal, i236:decimal, i237:decimal, i238:decimal, i239:decimal, i240:decimal, i241:decimal, i242:decimal, i243:decimal, i244:decimal, i245:decimal, i246:decimal, i247:decimal, i248:decimal, i249:decimal, i250:decimal, i251:decimal, i252:decimal, i253:decimal, i254:decimal, i255:decimal, i256:decimal, i257:decimal, i258:decimal, i259:decimal, i260:decimal, i261:decimal, i262:decimal, i263:decimal, i264:decimal, i265:decimal, i266:decimal, i267:decimal, i268:decimal, i269:decimal, i270:decimal, i271:decimal, i272:decimal, i273:decimal, i274:decimal, i275:decimal, i276:decimal, i277:decimal, i278:decimal, i279:decimal, i280:decimal, i281:decimal, i282:decimal, i283:decimal, i284:decimal, i285:decimal, i286:decimal, i287:decimal, i288:decimal, i289:decimal, i290:decimal, i291:decimal, i292:decimal, i293:decimal, i294:decimal, i295:decimal, i296:decimal, i297:decimal, i298:decimal, i299:decimal, i300:decimal, i301:decimal, i302:decimal, i303:decimal, i304:decimal, i305:decimal, i306:decimal, i307:decimal, i308:decimal, i309:decimal, i310:decimal, i311:decimal, i312:decimal, i313:decimal, i314:decimal, i315:decimal, i316:decimal, i317:decimal, i318:decimal, i319:decimal, i320:decimal, i321:decimal, i322:decimal) =
        0.0f
    //slider64tablef iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider64tablef (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal, i64:decimal, i65:decimal, i66:decimal, i67:decimal, i68:decimal, i69:decimal, i70:decimal, i71:decimal, i72:decimal, i73:decimal, i74:decimal, i75:decimal, i76:decimal, i77:decimal, i78:decimal, i79:decimal, i80:decimal, i81:decimal, i82:decimal, i83:decimal, i84:decimal, i85:decimal, i86:decimal, i87:decimal, i88:decimal, i89:decimal, i90:decimal, i91:decimal, i92:decimal, i93:decimal, i94:decimal, i95:decimal, i96:decimal, i97:decimal, i98:decimal, i99:decimal, i100:decimal, i101:decimal, i102:decimal, i103:decimal, i104:decimal, i105:decimal, i106:decimal, i107:decimal, i108:decimal, i109:decimal, i110:decimal, i111:decimal, i112:decimal, i113:decimal, i114:decimal, i115:decimal, i116:decimal, i117:decimal, i118:decimal, i119:decimal, i120:decimal, i121:decimal, i122:decimal, i123:decimal, i124:decimal, i125:decimal, i126:decimal, i127:decimal, i128:decimal, i129:decimal, i130:decimal, i131:decimal, i132:decimal, i133:decimal, i134:decimal, i135:decimal, i136:decimal, i137:decimal, i138:decimal, i139:decimal, i140:decimal, i141:decimal, i142:decimal, i143:decimal, i144:decimal, i145:decimal, i146:decimal, i147:decimal, i148:decimal, i149:decimal, i150:decimal, i151:decimal, i152:decimal, i153:decimal, i154:decimal, i155:decimal, i156:decimal, i157:decimal, i158:decimal, i159:decimal, i160:decimal, i161:decimal, i162:decimal, i163:decimal, i164:decimal, i165:decimal, i166:decimal, i167:decimal, i168:decimal, i169:decimal, i170:decimal, i171:decimal, i172:decimal, i173:decimal, i174:decimal, i175:decimal, i176:decimal, i177:decimal, i178:decimal, i179:decimal, i180:decimal, i181:decimal, i182:decimal, i183:decimal, i184:decimal, i185:decimal, i186:decimal, i187:decimal, i188:decimal, i189:decimal, i190:decimal, i191:decimal, i192:decimal, i193:decimal, i194:decimal, i195:decimal, i196:decimal, i197:decimal, i198:decimal, i199:decimal, i200:decimal, i201:decimal, i202:decimal, i203:decimal, i204:decimal, i205:decimal, i206:decimal, i207:decimal, i208:decimal, i209:decimal, i210:decimal, i211:decimal, i212:decimal, i213:decimal, i214:decimal, i215:decimal, i216:decimal, i217:decimal, i218:decimal, i219:decimal, i220:decimal, i221:decimal, i222:decimal, i223:decimal, i224:decimal, i225:decimal, i226:decimal, i227:decimal, i228:decimal, i229:decimal, i230:decimal, i231:decimal, i232:decimal, i233:decimal, i234:decimal, i235:decimal, i236:decimal, i237:decimal, i238:decimal, i239:decimal, i240:decimal, i241:decimal, i242:decimal, i243:decimal, i244:decimal, i245:decimal, i246:decimal, i247:decimal, i248:decimal, i249:decimal, i250:decimal, i251:decimal, i252:decimal, i253:decimal, i254:decimal, i255:decimal, i256:decimal, i257:decimal, i258:decimal, i259:decimal, i260:decimal, i261:decimal, i262:decimal, i263:decimal, i264:decimal, i265:decimal, i266:decimal, i267:decimal, i268:decimal, i269:decimal, i270:decimal, i271:decimal, i272:decimal, i273:decimal, i274:decimal, i275:decimal, i276:decimal, i277:decimal, i278:decimal, i279:decimal, i280:decimal, i281:decimal, i282:decimal, i283:decimal, i284:decimal, i285:decimal, i286:decimal, i287:decimal, i288:decimal, i289:decimal, i290:decimal, i291:decimal, i292:decimal, i293:decimal, i294:decimal, i295:decimal, i296:decimal, i297:decimal, i298:decimal, i299:decimal, i300:decimal, i301:decimal, i302:decimal, i303:decimal, i304:decimal, i305:decimal, i306:decimal, i307:decimal, i308:decimal, i309:decimal, i310:decimal, i311:decimal, i312:decimal, i313:decimal, i314:decimal, i315:decimal, i316:decimal, i317:decimal, i318:decimal, i319:decimal, i320:decimal, i321:decimal, i322:decimal, i323:decimal, i324:decimal, i325:decimal, i326:decimal, i327:decimal, i328:decimal, i329:decimal, i330:decimal, i331:decimal, i332:decimal, i333:decimal, i334:decimal, i335:decimal, i336:decimal, i337:decimal, i338:decimal, i339:decimal, i340:decimal, i341:decimal, i342:decimal, i343:decimal, i344:decimal, i345:decimal, i346:decimal, i347:decimal, i348:decimal, i349:decimal, i350:decimal, i351:decimal, i352:decimal, i353:decimal, i354:decimal, i355:decimal, i356:decimal, i357:decimal, i358:decimal, i359:decimal, i360:decimal, i361:decimal, i362:decimal, i363:decimal, i364:decimal, i365:decimal, i366:decimal, i367:decimal, i368:decimal, i369:decimal, i370:decimal, i371:decimal, i372:decimal, i373:decimal, i374:decimal, i375:decimal, i376:decimal, i377:decimal, i378:decimal, i379:decimal, i380:decimal, i381:decimal, i382:decimal, i383:decimal, i384:decimal, i385:decimal, i386:decimal) =
        0.0f
    //slider8 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii iiiiiiii
    static member slider8 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal) =
        i0, i0, i0, i0, i0, i0, i0, i0
    //slider8 iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkk
    static member slider8 (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider8f iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkk
    static member slider8f (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //slider8table iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider8table (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal) =
        0.0f
    //slider8tablef iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii k
    static member slider8tablef (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal) =
        0.0f
    //sliderKawai iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii kkkkkkkkkkkkkkkk
    static member sliderKawai (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, i10:decimal, i11:decimal, i12:decimal, i13:decimal, i14:decimal, i15:decimal, i16:decimal, i17:decimal, i18:decimal, i19:decimal, i20:decimal, i21:decimal, i22:decimal, i23:decimal, i24:decimal, i25:decimal, i26:decimal, i27:decimal, i28:decimal, i29:decimal, i30:decimal, i31:decimal, i32:decimal, i33:decimal, i34:decimal, i35:decimal, i36:decimal, i37:decimal, i38:decimal, i39:decimal, i40:decimal, i41:decimal, i42:decimal, i43:decimal, i44:decimal, i45:decimal, i46:decimal, i47:decimal, i48:decimal, i49:decimal, i50:decimal, i51:decimal, i52:decimal, i53:decimal, i54:decimal, i55:decimal, i56:decimal, i57:decimal, i58:decimal, i59:decimal, i60:decimal, i61:decimal, i62:decimal, i63:decimal) =
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
    //sndload Sooooojjoo 
    static member sndload (s0:string, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?j6:decimal, ?j7:decimal, ?o8:decimal, ?o9:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        ()
    //sndload iooooojjoo 
    static member sndload (i0:decimal, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?j6:decimal, ?j7:decimal, ?o8:decimal, ?o9:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let j6 = defaultArg j6 -0.1m
        let j7 = defaultArg j7 -0.1m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        ()
    //sndloop akkii ak
    static member sndloop (a0:Vector<double>, k1:single, k2:single, i3:decimal, i4:decimal) =
        a0, k1
    //sndwarp xxxiiiiiii mm
    static member sndwarp (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal) =
        vector [(double)0.0], vector [(double)0.0]
    //sndwarpst xxxiiiiiii mmmm
    static member sndwarpst (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //sockrecv ii a
    static member sockrecv (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //sockrecvs ii aa
    static member sockrecvs (i0:decimal, i1:decimal) =
        vector [(double)0.0], vector [(double)0.0]
    //socksend SSiio 
    static member socksend (s0:string, s1:string, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        ()
    //socksend aSiio 
    static member socksend (a0:Vector<double>, s1:string, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        ()
    //socksend kSiio 
    static member socksend (k0:single, s1:string, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        ()
    //socksends aaSiio 
    static member socksends (a0:Vector<double>, a1:Vector<double>, s2:string, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        ()
    //soundin Soooo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member soundin (s0:string, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //soundin ioooo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member soundin (i0:decimal, ?o1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o1 = defaultArg o1 0m
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //soundout aSo 
    static member soundout (a0:Vector<double>, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //soundout aio 
    static member soundout (a0:Vector<double>, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        ()
    //soundouts aaSo 
    static member soundouts (a0:Vector<double>, a1:Vector<double>, s2:string, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //soundouts aaio 
    static member soundouts (a0:Vector<double>, a1:Vector<double>, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //space aikkkk aaaa
    static member space (a0:Vector<double>, i1:decimal, k2:single, k3:single, k4:single, k5:single) =
        a0, a0, a0, a0
    //spat3d akkkiiiiio aaaa
    static member spat3d (a0:Vector<double>, k1:single, k2:single, k3:single, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, ?o9:decimal) =
        let o9 = defaultArg o9 0m
        a0, a0, a0, a0
    //spat3di aiiiiiio aaaa
    static member spat3di (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0, a0, a0, a0
    //spat3dt iiiiiiiio 
    static member spat3dt (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, ?o8:decimal) =
        let o8 = defaultArg o8 0m
        ()
    //spdist ikkk k
    static member spdist (i0:decimal, k1:single, k2:single, k3:single) =
        k1
    //specaddm wwp w
    static member specaddm () =
        ()
    //specdiff w w
    static member specdiff () =
        ()
    //specdisp wio 
    static member specdisp () =
        ()
    //specfilt wi w
    static member specfilt () =
        ()
    //spechist w w
    static member spechist () =
        ()
    //specptrk wkiiiiiioqooo kk
    static member specptrk () =
        0.0f, 0.0f
    //specscal wii w
    static member specscal () =
        ()
    //specsum wo k
    static member specsum () =
        0.0f
    //spectrum siiiqoooo w
    static member spectrum () =
        ()
    //splitrig kkiiz 
    static member splitrig (k0:single, k1:single, i2:decimal, i3:decimal, z4:single[]) =
        ()
    //sprintf STN S
    static member sprintf (s0:string, T1:'a, [<ParamArray>] ?N2:Object[]) =
        let N2 = defaultArg N2 
        s0
    //sprintfk SUN S
    static member sprintfk (s0:string, U1:'a, [<ParamArray>] ?N2:Object[]) =
        let N2 = defaultArg N2 
        s0
    //spsend  aaaa
    static member spsend () =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //sqrt a a
    static member sqrt (a0:Vector<double>) =
        a0
    //sqrt i i
    static member sqrt (i0:decimal) =
        i0
    //sqrt k k
    static member sqrt (k0:single) =
        k0
    //stack i 
    static member stack (i0:decimal) =
        ()
    //statevar axxop aaaa
    static member statevar (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal, ?p4:decimal) =
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        a0, a0, a0, a0
    //stix iiooo a
    static member stix (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //STKBandedWG iiJJJJJJJJJJJJJJJJ a
    static member STKBandedWG (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKBeeThree iiJJJJJJJJJJJJJJJJ a
    static member STKBeeThree (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKBlowBotl iiJJJJJJJJJJJJJJJJ a
    static member STKBlowBotl (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKBlowHole iiJJJJJJJJJJJJJJJJ a
    static member STKBlowHole (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKBowed iiJJJJJJJJJJJJJJJJ a
    static member STKBowed (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKBrass iiJJJJJJJJJJJJJJJJ a
    static member STKBrass (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKClarinet iiJJJJJJJJJJJJJJJJ a
    static member STKClarinet (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKDrummer iiJJJJJJJJJJJJJJJJ a
    static member STKDrummer (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKFlute iiJJJJJJJJJJJJJJJJ a
    static member STKFlute (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKFMVoices iiJJJJJJJJJJJJJJJJ a
    static member STKFMVoices (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKHevyMetl iiJJJJJJJJJJJJJJJJ a
    static member STKHevyMetl (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKMandolin iiJJJJJJJJJJJJJJJJ a
    static member STKMandolin (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKModalBar iiJJJJJJJJJJJJJJJJ a
    static member STKModalBar (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKMoog iiJJJJJJJJJJJJJJJJ a
    static member STKMoog (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKPercFlut iiJJJJJJJJJJJJJJJJ a
    static member STKPercFlut (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKPlucked iiJJJJJJJJJJJJJJJJ a
    static member STKPlucked (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKResonate iiJJJJJJJJJJJJJJJJ a
    static member STKResonate (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKRhodey iiJJJJJJJJJJJJJJJJ a
    static member STKRhodey (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKSaxofony iiJJJJJJJJJJJJJJJJ a
    static member STKSaxofony (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKShakers iiJJJJJJJJJJJJJJJJ a
    static member STKShakers (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKSimple iiJJJJJJJJJJJJJJJJ a
    static member STKSimple (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKSitar iiJJJJJJJJJJJJJJJJ a
    static member STKSitar (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKStifKarp iiJJJJJJJJJJJJJJJJ a
    static member STKStifKarp (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKTubeBell iiJJJJJJJJJJJJJJJJ a
    static member STKTubeBell (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKVoicForm iiJJJJJJJJJJJJJJJJ a
    static member STKVoicForm (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKWhistle iiJJJJJJJJJJJJJJJJ a
    static member STKWhistle (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //STKWurley iiJJJJJJJJJJJJJJJJ a
    static member STKWurley (i0:decimal, i1:decimal) =
        vector [(double)0.0]
    //strcat SS S
    static member strcat (s0:string, s1:string) =
        s0
    //strcatk SS S
    static member strcatk (s0:string, s1:string) =
        s0
    //strchar So i
    static member strchar (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        o1
    //strchark SO k
    static member strchark (s0:string, ?O1:single) =
        let O1 = defaultArg O1 0.f
        O1
    //strcmp SS i
    static member strcmp (s0:string, s1:string) =
        0.0m
    //strcmpk SS k
    static member strcmpk (s0:string, s1:string) =
        0.0f
    //strcpy S S
    static member strcpy (s0:string) =
        s0
    //strcpy i S
    static member strcpy (i0:decimal) =
        0.0
    //strcpyk S S
    static member strcpyk (s0:string) =
        s0
    //strcpyk k S
    static member strcpyk (k0:single) =
        0.0
    //strecv Si a
    static member strecv (s0:string, i1:decimal) =
        vector [(double)0.0]
    //streson akk a
    static member streson (a0:Vector<double>, k1:single, k2:single) =
        a0
    //strget i S
    static member strget (i0:decimal) =
        0.0
    //strindex SS i
    static member strindex (s0:string, s1:string) =
        0.0m
    //strindexk SS k
    static member strindexk (s0:string, s1:string) =
        0.0f
    //strlen S i
    static member strlen (s0:string) =
        0.0m
    //strlenk S k
    static member strlenk (s0:string) =
        0.0f
    //strlower S S
    static member strlower (s0:string) =
        s0
    //strlowerk S S
    static member strlowerk (s0:string) =
        s0
    //strrindex SS i
    static member strrindex (s0:string, s1:string) =
        0.0m
    //strrindexk SS k
    static member strrindexk (s0:string, s1:string) =
        0.0f
    //strset iS 
    static member strset (i0:decimal, s1:string) =
        ()
    //strsub Soj S
    static member strsub (s0:string, ?o1:decimal, ?j2:decimal) =
        let o1 = defaultArg o1 0m
        let j2 = defaultArg j2 -0.1m
        s0
    //strsubk Skk S
    static member strsubk (s0:string, k1:single, k2:single) =
        s0
    //strtod S i
    static member strtod (s0:string) =
        0.0m
    //strtod i i
    static member strtod (i0:decimal) =
        i0
    //strtodk S k
    static member strtodk (s0:string) =
        0.0f
    //strtol S i
    static member strtol (s0:string) =
        0.0m
    //strtol i i
    static member strtol (i0:decimal) =
        i0
    //strtolk S k
    static member strtolk (s0:string) =
        0.0f
    //strupper S S
    static member strupper (s0:string) =
        s0
    //strupperk S S
    static member strupperk (s0:string) =
        s0
    //stsend aSi 
    static member stsend (a0:Vector<double>, s1:string, i2:decimal) =
        ()
    //subinstr Sm mmmmmmmm
    static member subinstr (s0:string, m1:Vector<double>) =
        m1, m1, m1, m1, m1, m1, m1, m1
    //subinstr im mmmmmmmm
    static member subinstr (i0:decimal, m1:Vector<double>) =
        m1, m1, m1, m1, m1, m1, m1, m1
    //subinstrinit Sm 
    static member subinstrinit (s0:string, m1:Vector<double>) =
        ()
    //subinstrinit im 
    static member subinstrinit (i0:decimal, m1:Vector<double>) =
        ()
    //sum y a
    static member sum (y0:Vector<double>[][]) =
        vector [(double)0.0]
    //sumarray i[] i
    static member sumarray (i0:decimal, M1:decimal[]) =
        i0
    //sumarray k[] k
    static member sumarray (k0:single, M1:decimal[]) =
        k0
    //sumtab k[] k
    static member sumtab (k0:single, M1:decimal[]) =
        k0
    //svfilter axxo aaa
    static member svfilter (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0, a0, a0
    //syncgrain kkkkkiii a
    static member syncgrain (k0:single, k1:single, k2:single, k3:single, k4:single, i5:decimal, i6:decimal, i7:decimal) =
        vector [(double)0.0]
    //syncloop kkkkkkkiiioo a
    static member syncloop (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, i7:decimal, i8:decimal, i9:decimal, ?o10:decimal, ?o11:decimal) =
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        vector [(double)0.0]
    //syncphasor xao aa
    static member syncphasor (x0:CSoundOpcode.ScalarOrArray, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a1, a1
    //system kSO k
    static member system (k0:single, s1:string, ?O2:single) =
        let O2 = defaultArg O2 0.f
        k0
    //system_i iSo i
    static member system_i (i0:decimal, s1:string, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        i0
    //tab xio a
    static member tab (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        vector [(double)0.0]
    //tab kio k
    static member tab (k0:single, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //tab_i iio i
    static member tab_i (i0:decimal, i1:decimal, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        i0
    //tab2pvs k[]oop f
    static member tab2pvs (k0:single, M1:decimal[], ?o2:decimal, ?o3:decimal, ?p4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let p4 = defaultArg p4 1m
        0.0
    //tabgen iip k[]
    static member tabgen (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        0.0f, [|0.0m|], 0.0
    //table<'A> xiooo  -> (a | k)

    static member table<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //table iiooo i
    static member table (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //table3<'A> xiooo  -> (a | k)

    static member table3<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //table3 iiooo i
    static member table3 (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //table3kt<'A> xkooo  -> (a | k)

    static member table3kt<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //tablecopy kk 
    static member tablecopy (k0:single, k1:single) =
        ()
    //tablefilter kkkk k
    static member tablefilter (k0:single, k1:single, k2:single, k3:single) =
        k0
    //tablefilteri iiii i
    static member tablefilteri (i0:decimal, i1:decimal, i2:decimal, i3:decimal) =
        i0
    //tablegpw k 
    static member tablegpw (k0:single) =
        ()
    //tablei<'A> xiooo  -> (a | k)

    static member tablei<'A> (x0:CSoundOpcode.ScalarOrArray, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //tablei iiooo i
    static member tablei (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        i0
    //tableicopy ii 
    static member tableicopy (i0:decimal, i1:decimal) =
        ()
    //tableigpw i 
    static member tableigpw (i0:decimal) =
        ()
    //tableikt<'A> xkooo  -> (a | k)

    static member tableikt<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //tableimix iiiiiiiii 
    static member tableimix (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal) =
        ()
    //tableiw iiiooo 
    static member tableiw (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //tablekt<'A> xkooo  -> (a | k)

    static member tablekt<'A> (x0:CSoundOpcode.ScalarOrArray, k1:single, ?o2:decimal, ?o3:decimal, ?o4:decimal) =

        let o2 = defaultArg o2 0m

        let o3 = defaultArg o3 0m

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //tablemix kkkkkkkkk 
    static member tablemix (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, k8:single) =
        ()
    //tableng i i
    static member tableng (i0:decimal) =
        i0
    //tableng k k
    static member tableng (k0:single) =
        k0
    //tablera kkk a
    static member tablera (k0:single, k1:single, k2:single) =
        vector [(double)0.0]
    //tableseg iin 
    static member tableseg (i0:decimal, i1:decimal, n2:decimal[]) =
        ()
    //tableshuffle k 
    static member tableshuffle (k0:single) =
        ()
    //tableshufflei i 
    static member tableshufflei (i0:decimal) =
        ()
    //tablew aaiooo 
    static member tablew (a0:Vector<double>, a1:Vector<double>, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //tablew kkiooo 
    static member tablew (k0:single, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //tablewa kak k
    static member tablewa (k0:single, a1:Vector<double>, k2:single) =
        k0
    //tablewkt aakooo 
    static member tablewkt (a0:Vector<double>, a1:Vector<double>, k2:single, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //tablewkt kkkooo 
    static member tablewkt (k0:single, k1:single, k2:single, ?o3:decimal, ?o4:decimal, ?o5:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        ()
    //tablexkt xkkiooo a
    static member tablexkt (x0:CSoundOpcode.ScalarOrArray, k1:single, k2:single, i3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0]
    //tablexseg iin 
    static member tablexseg (i0:decimal, i1:decimal, n2:decimal[]) =
        ()
    //tabmap k[]S k[]
    static member tabmap (k0:single, M1:decimal[], s2:string) =
        k0, M1, 0.0
    //tabmap_i k[]S k[]
    static member tabmap_i (k0:single, M1:decimal[], s2:string) =
        k0, M1, 0.0
    //tabmorph kkkkm k
    static member tabmorph (k0:single, k1:single, k2:single, k3:single, m4:Vector<double>) =
        k0
    //tabmorpha aaaam a
    static member tabmorpha (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, a3:Vector<double>, m4:Vector<double>) =
        a0
    //tabmorphak akkkm a
    static member tabmorphak (a0:Vector<double>, k1:single, k2:single, k3:single, m4:Vector<double>) =
        a0
    //tabmorphi kkkkm k
    static member tabmorphi (k0:single, k1:single, k2:single, k3:single, m4:Vector<double>) =
        k0
    //tabplay kkkz 
    static member tabplay (k0:single, k1:single, k2:single, z3:single[]) =
        ()
    //tabrec kkkkz 
    static member tabrec (k0:single, k1:single, k2:single, k3:single, z4:single[]) =
        ()
    //tabslice k[]ii k[]
    static member tabslice (k0:single, M1:decimal[], i2:decimal, i3:decimal) =
        k0, M1, 0.0
    //tabsum iOO k
    static member tabsum (i0:decimal, ?O1:single, ?O2:single) =
        let O1 = defaultArg O1 0.f
        let O2 = defaultArg O2 0.f
        O1
    //tabw xxio 
    static member tabw (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //tabw_i iiio 
    static member tabw_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //tambourine kioooooo a
    static member tambourine (k0:single, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //tan a a
    static member tan (a0:Vector<double>) =
        a0
    //tan i i
    static member tan (i0:decimal) =
        i0
    //tan k k
    static member tan (k0:single) =
        k0
    //tanh a a
    static member tanh (a0:Vector<double>) =
        a0
    //tanh i i
    static member tanh (i0:decimal) =
        i0
    //tanh k k
    static member tanh (k0:single) =
        k0
    //taninv a a
    static member taninv (a0:Vector<double>) =
        a0
    //taninv i i
    static member taninv (i0:decimal) =
        i0
    //taninv k k
    static member taninv (k0:single) =
        k0
    //taninv2 aa a
    static member taninv2 (a0:Vector<double>, a1:Vector<double>) =
        a0
    //taninv2 ii i
    static member taninv2 (i0:decimal, i1:decimal) =
        i0
    //taninv2 kk k
    static member taninv2 (k0:single, k1:single) =
        k0
    //tb0 i i
    static member tb0 (i0:decimal) =
        i0
    //tb0 k k
    static member tb0 (k0:single) =
        k0
    //tb0_init i 
    static member tb0_init (i0:decimal) =
        ()
    //tb1 i i
    static member tb1 (i0:decimal) =
        i0
    //tb1 k k
    static member tb1 (k0:single) =
        k0
    //tb1_init i 
    static member tb1_init (i0:decimal) =
        ()
    //tb10 i i
    static member tb10 (i0:decimal) =
        i0
    //tb10 k k
    static member tb10 (k0:single) =
        k0
    //tb10_init i 
    static member tb10_init (i0:decimal) =
        ()
    //tb11 i i
    static member tb11 (i0:decimal) =
        i0
    //tb11 k k
    static member tb11 (k0:single) =
        k0
    //tb11_init i 
    static member tb11_init (i0:decimal) =
        ()
    //tb12 i i
    static member tb12 (i0:decimal) =
        i0
    //tb12 k k
    static member tb12 (k0:single) =
        k0
    //tb12_init i 
    static member tb12_init (i0:decimal) =
        ()
    //tb13 i i
    static member tb13 (i0:decimal) =
        i0
    //tb13 k k
    static member tb13 (k0:single) =
        k0
    //tb13_init i 
    static member tb13_init (i0:decimal) =
        ()
    //tb14 i i
    static member tb14 (i0:decimal) =
        i0
    //tb14 k k
    static member tb14 (k0:single) =
        k0
    //tb14_init i 
    static member tb14_init (i0:decimal) =
        ()
    //tb15 i i
    static member tb15 (i0:decimal) =
        i0
    //tb15 k k
    static member tb15 (k0:single) =
        k0
    //tb15_init i 
    static member tb15_init (i0:decimal) =
        ()
    //tb2 i i
    static member tb2 (i0:decimal) =
        i0
    //tb2 k k
    static member tb2 (k0:single) =
        k0
    //tb2_init i 
    static member tb2_init (i0:decimal) =
        ()
    //tb3 i i
    static member tb3 (i0:decimal) =
        i0
    //tb3 k k
    static member tb3 (k0:single) =
        k0
    //tb3_init i 
    static member tb3_init (i0:decimal) =
        ()
    //tb4 i i
    static member tb4 (i0:decimal) =
        i0
    //tb4 k k
    static member tb4 (k0:single) =
        k0
    //tb4_init i 
    static member tb4_init (i0:decimal) =
        ()
    //tb5 i i
    static member tb5 (i0:decimal) =
        i0
    //tb5 k k
    static member tb5 (k0:single) =
        k0
    //tb5_init i 
    static member tb5_init (i0:decimal) =
        ()
    //tb6 i i
    static member tb6 (i0:decimal) =
        i0
    //tb6 k k
    static member tb6 (k0:single) =
        k0
    //tb6_init i 
    static member tb6_init (i0:decimal) =
        ()
    //tb7 i i
    static member tb7 (i0:decimal) =
        i0
    //tb7 k k
    static member tb7 (k0:single) =
        k0
    //tb7_init i 
    static member tb7_init (i0:decimal) =
        ()
    //tb8 i i
    static member tb8 (i0:decimal) =
        i0
    //tb8 k k
    static member tb8 (k0:single) =
        k0
    //tb8_init i 
    static member tb8_init (i0:decimal) =
        ()
    //tb9 i i
    static member tb9 (i0:decimal) =
        i0
    //tb9 k k
    static member tb9 (k0:single) =
        k0
    //tb9_init i 
    static member tb9_init (i0:decimal) =
        ()
    //tbvcf axxkkp a
    static member tbvcf (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, k3:single, k4:single, ?p5:decimal) =
        let p5 = defaultArg p5 1m
        a0
    //tempest kiiiiiiiiiop k
    static member tempest (k0:single, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, i7:decimal, i8:decimal, i9:decimal, ?o10:decimal, ?p11:decimal) =
        let o10 = defaultArg o10 0m
        let p11 = defaultArg p11 1m
        k0
    //tempo ki 
    static member tempo (k0:single, i1:decimal) =
        ()
    //temposcal kkkkkooPOP mm
    static member temposcal (k0:single, k1:single, k2:single, k3:single, k4:single, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        vector [(double)0.0], vector [(double)0.0]
    //tempoval  k
    static member tempoval () =
        0.0f
    //tigoto l 
    static member tigoto (l0:_) =
        ()
    //timedseq kiz k
    static member timedseq (k0:single, i1:decimal, z2:single[]) =
        k0
    //timeinstk  k
    static member timeinstk () =
        0.0f
    //timeinsts  k
    static member timeinsts () =
        0.0f
    //timek<'A>   -> (i | k)

    static member timek<'A> () =

        Unchecked.defaultof<'A>
    //times<'A>   -> (i | k)

    static member times<'A> () =

        Unchecked.defaultof<'A>
    //timout iil 
    static member timout (i0:decimal, i1:decimal, l2:_) =
        ()
    //tival  i
    static member tival () =
        0.0m
    //tlineto kkk k
    static member tlineto (k0:single, k1:single, k2:single) =
        k0
    //tone aao a
    static member tone (a0:Vector<double>, a1:Vector<double>, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //tone ako a
    static member tone (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //tonek kko k
    static member tonek (k0:single, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        k0
    //tonex aaoo a
    static member tonex (a0:Vector<double>, a1:Vector<double>, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0
    //tonex akoo a
    static member tonex (a0:Vector<double>, k1:single, ?o2:decimal, ?o3:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        a0
    //tradsyn fkkki a
    static member tradsyn (S0:_[], k1:single, k2:single, k3:single, i4:decimal) =
        vector [(double)0.0]
    //trandom kkk k
    static member trandom (k0:single, k1:single, k2:single) =
        k0
    //transeg<'A> iiim  -> (a | k)

    static member transeg<'A> (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =

        Unchecked.defaultof<'A>
    //transegb<'A> iiim  -> (a | k)

    static member transegb<'A> (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =

        Unchecked.defaultof<'A>
    //transegr<'A> iiim  -> (a | k)

    static member transegr<'A> (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =

        Unchecked.defaultof<'A>
    //trcross ffkz f
    static member trcross (S0:_[], S1:_[], k2:single, z3:single[]) =
        S0
    //trfilter fki f
    static member trfilter (S0:_[], k1:single, i2:decimal) =
        S0
    //trhighest fk fkk
    static member trhighest (S0:_[], k1:single) =
        S0, k1, k1
    //trigger kkk k
    static member trigger (k0:single, k1:single, k2:single) =
        k0
    //trigseq kkkkkz 
    static member trigseq (k0:single, k1:single, k2:single, k3:single, k4:single, z5:single[]) =
        ()
    //trirand<'A> k  -> (a | i | k)

    static member trirand<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //trlowest fk fkk
    static member trlowest (S0:_[], k1:single) =
        S0, k1, k1
    //trmix ff f
    static member trmix (S0:_[], S1:_[]) =
        S0
    //trscale fz f
    static member trscale (S0:_[], z1:single[]) =
        S0
    //trshift fz f
    static member trshift (S0:_[], z1:single[]) =
        S0
    //trsplit fz ff
    static member trsplit (S0:_[], z1:single[]) =
        S0, S0
    //turnoff  
    static member turnoff () =
        ()
    //turnoff i 
    static member turnoff (i0:decimal) =
        ()
    //turnoff k 
    static member turnoff (k0:single) =
        ()
    //turnoff2 Skk 
    static member turnoff2 (s0:string, k1:single, k2:single) =
        ()
    //turnoff2 ikk 
    static member turnoff2 (i0:decimal, k1:single, k2:single) =
        ()
    //turnoff2 kkk 
    static member turnoff2 (k0:single, k1:single, k2:single) =
        ()
    //turnon So 
    static member turnon (s0:string, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //turnon io 
    static member turnon (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //unirand<'A> k  -> (a | i | k)

    static member unirand<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //unwrap k[] k[]
    static member unwrap (k0:single, M1:decimal[]) =
        k0, M1, 0.0
    //upsamp k a
    static member upsamp (k0:single) =
        vector [(double)0.0]
    //urd<'A> k  -> (a | k)

    static member urd<'A> (k0:single) =

        Unchecked.defaultof<'A>
    //urd i i
    static member urd (i0:decimal) =
        i0
    //vactrol ajj a
    static member vactrol (a0:Vector<double>, ?j1:decimal, ?j2:decimal) =
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        a0
    //vadd ikkOO 
    static member vadd (i0:decimal, k1:single, k2:single, ?O3:single, ?O4:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        ()
    //vadd_i iiio 
    static member vadd_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //vaddv iikOOO 
    static member vaddv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vaddv_i iiioo 
    static member vaddv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vaget ka k
    static member vaget (k0:single, a1:Vector<double>) =
        k0
    //valpass akxioo a
    static member valpass (a0:Vector<double>, k1:single, x2:CSoundOpcode.ScalarOrArray, i3:decimal, ?o4:decimal, ?o5:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0
    //vaset kka 
    static member vaset (k0:single, k1:single, a2:Vector<double>) =
        ()
    //vbap<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> akOOo  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member vbap<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> (a0:Vector<double>, k1:single, ?O2:single, ?O3:single, ?o4:decimal) =

        let O2 = defaultArg O2 0.f

        let O3 = defaultArg O3 0.f

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>, Unchecked.defaultof<'o>, Unchecked.defaultof<'p>, Unchecked.defaultof<'q>, Unchecked.defaultof<'r>, Unchecked.defaultof<'s>, Unchecked.defaultof<'t>, Unchecked.defaultof<'u>, Unchecked.defaultof<'v>, Unchecked.defaultof<'w>, Unchecked.defaultof<'x>, Unchecked.defaultof<'y>, Unchecked.defaultof<'z>
    //vbap16 akOOo aaaaaaaaaaaaaaaa
    static member vbap16 (a0:Vector<double>, k1:single, ?O2:single, ?O3:single, ?o4:decimal) =
        let O2 = defaultArg O2 0.f
        let O3 = defaultArg O3 0.f
        let o4 = defaultArg o4 0m
        a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0, a0
    //vbap4 akOOo aaaa
    static member vbap4 (a0:Vector<double>, k1:single, ?O2:single, ?O3:single, ?o4:decimal) =
        let O2 = defaultArg O2 0.f
        let O3 = defaultArg O3 0.f
        let o4 = defaultArg o4 0m
        a0, a0, a0, a0
    //vbap4move aiiim aaaa
    static member vbap4move (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, m4:Vector<double>) =
        a0, a0, a0, a0
    //vbap8 akOOo aaaaaaaa
    static member vbap8 (a0:Vector<double>, k1:single, ?O2:single, ?O3:single, ?o4:decimal) =
        let O2 = defaultArg O2 0.f
        let O3 = defaultArg O3 0.f
        let o4 = defaultArg o4 0m
        a0, a0, a0, a0, a0, a0, a0, a0
    //vbap8move aiiim aaaaaaaa
    static member vbap8move (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, m4:Vector<double>) =
        a0, a0, a0, a0, a0, a0, a0, a0
    //vbapg<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> kOOo  -> (k[] | zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz)

    static member vbapg<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> (k0:single, ?O1:single, ?O2:single, ?o3:decimal) =

        let O1 = defaultArg O1 0.f

        let O2 = defaultArg O2 0.f

        let o3 = defaultArg o3 0m

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>, Unchecked.defaultof<'o>, Unchecked.defaultof<'p>, Unchecked.defaultof<'q>, Unchecked.defaultof<'r>, Unchecked.defaultof<'s>, Unchecked.defaultof<'t>, Unchecked.defaultof<'u>, Unchecked.defaultof<'v>, Unchecked.defaultof<'w>, Unchecked.defaultof<'x>, Unchecked.defaultof<'y>, Unchecked.defaultof<'z>
    //vbapgmove<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> iiim  -> (k[] | zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz)

    static member vbapgmove<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>, Unchecked.defaultof<'o>, Unchecked.defaultof<'p>, Unchecked.defaultof<'q>, Unchecked.defaultof<'r>, Unchecked.defaultof<'s>, Unchecked.defaultof<'t>, Unchecked.defaultof<'u>, Unchecked.defaultof<'v>, Unchecked.defaultof<'w>, Unchecked.defaultof<'x>, Unchecked.defaultof<'y>, Unchecked.defaultof<'z>
    //vbaplsinit iioooooooooooooooooooooooooooooooo 
    static member vbaplsinit (i0:decimal, i1:decimal, ?o2:decimal, ?o3:decimal, ?o4:decimal, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal, ?o9:decimal, ?o10:decimal, ?o11:decimal, ?o12:decimal, ?o13:decimal, ?o14:decimal, ?o15:decimal, ?o16:decimal, ?o17:decimal, ?o18:decimal, ?o19:decimal, ?o20:decimal, ?o21:decimal, ?o22:decimal, ?o23:decimal, ?o24:decimal, ?o25:decimal, ?o26:decimal, ?o27:decimal, ?o28:decimal, ?o29:decimal, ?o30:decimal, ?o31:decimal, ?o32:decimal, ?o33:decimal) =
        let o2 = defaultArg o2 0m
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        let o10 = defaultArg o10 0m
        let o11 = defaultArg o11 0m
        let o12 = defaultArg o12 0m
        let o13 = defaultArg o13 0m
        let o14 = defaultArg o14 0m
        let o15 = defaultArg o15 0m
        let o16 = defaultArg o16 0m
        let o17 = defaultArg o17 0m
        let o18 = defaultArg o18 0m
        let o19 = defaultArg o19 0m
        let o20 = defaultArg o20 0m
        let o21 = defaultArg o21 0m
        let o22 = defaultArg o22 0m
        let o23 = defaultArg o23 0m
        let o24 = defaultArg o24 0m
        let o25 = defaultArg o25 0m
        let o26 = defaultArg o26 0m
        let o27 = defaultArg o27 0m
        let o28 = defaultArg o28 0m
        let o29 = defaultArg o29 0m
        let o30 = defaultArg o30 0m
        let o31 = defaultArg o31 0m
        let o32 = defaultArg o32 0m
        let o33 = defaultArg o33 0m
        ()
    //vbapmove<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> aiiim  -> (a[] | mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm)

    static member vbapmove<'A,'B,'C,'D,'E,'F,'G,'H,'I,'J,'K,'L,'M,'N,'O,'P,'Q,'R,'S,'T,'U,'V,'W,'X,'Y,'Z,'a,'b,'c,'d,'e,'f,'g,'h,'i,'j,'k,'l,'m,'n,'o,'p,'q,'r,'s,'t,'u,'v,'w,'x,'y,'z> (a0:Vector<double>, i1:decimal, i2:decimal, i3:decimal, m4:Vector<double>) =

        Unchecked.defaultof<'A>, Unchecked.defaultof<'B>, Unchecked.defaultof<'C>, Unchecked.defaultof<'D>, Unchecked.defaultof<'E>, Unchecked.defaultof<'F>, Unchecked.defaultof<'G>, Unchecked.defaultof<'H>, Unchecked.defaultof<'I>, Unchecked.defaultof<'J>, Unchecked.defaultof<'K>, Unchecked.defaultof<'L>, Unchecked.defaultof<'M>, Unchecked.defaultof<'N>, Unchecked.defaultof<'O>, Unchecked.defaultof<'P>, Unchecked.defaultof<'Q>, Unchecked.defaultof<'R>, Unchecked.defaultof<'S>, Unchecked.defaultof<'T>, Unchecked.defaultof<'U>, Unchecked.defaultof<'V>, Unchecked.defaultof<'W>, Unchecked.defaultof<'X>, Unchecked.defaultof<'Y>, Unchecked.defaultof<'Z>, Unchecked.defaultof<'a>, Unchecked.defaultof<'b>, Unchecked.defaultof<'c>, Unchecked.defaultof<'d>, Unchecked.defaultof<'e>, Unchecked.defaultof<'f>, Unchecked.defaultof<'g>, Unchecked.defaultof<'h>, Unchecked.defaultof<'i>, Unchecked.defaultof<'j>, Unchecked.defaultof<'k>, Unchecked.defaultof<'l>, Unchecked.defaultof<'m>, Unchecked.defaultof<'n>, Unchecked.defaultof<'o>, Unchecked.defaultof<'p>, Unchecked.defaultof<'q>, Unchecked.defaultof<'r>, Unchecked.defaultof<'s>, Unchecked.defaultof<'t>, Unchecked.defaultof<'u>, Unchecked.defaultof<'v>, Unchecked.defaultof<'w>, Unchecked.defaultof<'x>, Unchecked.defaultof<'y>, Unchecked.defaultof<'z>
    //vbapz iiakOOo 
    static member vbapz (i0:decimal, i1:decimal, a2:Vector<double>, k3:single, ?O4:single, ?O5:single, ?o6:decimal) =
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        let o6 = defaultArg o6 0m
        ()
    //vbapzmove iiaiiim 
    static member vbapzmove (i0:decimal, i1:decimal, a2:Vector<double>, i3:decimal, i4:decimal, i5:decimal, m6:Vector<double>) =
        ()
    //vcella kkiiiiip 
    static member vcella (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, ?p7:decimal) =
        let p7 = defaultArg p7 1m
        ()
    //vco xxiVppovoo a
    static member vco (x0:CSoundOpcode.ScalarOrArray, x1:CSoundOpcode.ScalarOrArray, i2:decimal, ?V3:single, ?p4:decimal, ?p5:decimal, ?o6:decimal, ?v7:decimal, ?o8:decimal, ?o9:decimal) =
        let V3 = defaultArg V3 0.5f
        let p4 = defaultArg p4 1m
        let p5 = defaultArg p5 1m
        let o6 = defaultArg o6 0m
        let v7 = defaultArg v7 0.5m
        let o8 = defaultArg o8 0m
        let o9 = defaultArg o9 0m
        vector [(double)0.0]
    //vco2 kkoOOo a
    static member vco2 (k0:single, k1:single, ?o2:decimal, ?O3:single, ?O4:single, ?o5:decimal) =
        let o2 = defaultArg o2 0m
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let o5 = defaultArg o5 0m
        vector [(double)0.0]
    //vco2ft kov k
    static member vco2ft (k0:single, ?o1:decimal, ?v2:decimal) =
        let o1 = defaultArg o1 0m
        let v2 = defaultArg v2 0.5m
        k0
    //vco2ift iov i
    static member vco2ift (i0:decimal, ?o1:decimal, ?v2:decimal) =
        let o1 = defaultArg o1 0m
        let v2 = defaultArg v2 0.5m
        i0
    //vco2init ijjjjj i
    static member vco2init (i0:decimal, ?j1:decimal, ?j2:decimal, ?j3:decimal, ?j4:decimal, ?j5:decimal) =
        let j1 = defaultArg j1 -0.1m
        let j2 = defaultArg j2 -0.1m
        let j3 = defaultArg j3 -0.1m
        let j4 = defaultArg j4 -0.1m
        let j5 = defaultArg j5 -0.1m
        i0
    //vcomb akxioo a
    static member vcomb (a0:Vector<double>, k1:single, x2:CSoundOpcode.ScalarOrArray, i3:decimal, ?o4:decimal, ?o5:decimal) =
        let o4 = defaultArg o4 0m
        let o5 = defaultArg o5 0m
        a0
    //vcopy iikOOO 
    static member vcopy (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vcopy_i iiioo 
    static member vcopy_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vdel_k kkio k
    static member vdel_k (k0:single, k1:single, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        k0
    //vdelay axio a
    static member vdelay (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //vdelay3 axio a
    static member vdelay3 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        a0
    //vdelayk kkioo k
    static member vdelayk (k0:single, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        k0
    //vdelayx aaiio a
    static member vdelayx (a0:Vector<double>, a1:Vector<double>, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //vdelayxq aaaaaiio aaaa
    static member vdelayxq (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, a3:Vector<double>, a4:Vector<double>, i5:decimal, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0, a0, a0, a0
    //vdelayxs aaaiio aa
    static member vdelayxs (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0, a0
    //vdelayxw aaiio a
    static member vdelayxw (a0:Vector<double>, a1:Vector<double>, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        a0
    //vdelayxwq aaaaaiio aaaa
    static member vdelayxwq (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, a3:Vector<double>, a4:Vector<double>, i5:decimal, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        a0, a0, a0, a0
    //vdelayxws aaaiio aa
    static member vdelayxws (a0:Vector<double>, a1:Vector<double>, a2:Vector<double>, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        a0, a0
    //vdivv iikOOO 
    static member vdivv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vdivv_i iiioo 
    static member vdivv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vecdelay iiiiio 
    static member vecdelay (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        ()
    //veloc oh i
    static member veloc (?o0:decimal, ?h1:decimal) =
        let o0 = defaultArg o0 0m
        let h1 = defaultArg h1 127m
        o0
    //vexp ikkOO 
    static member vexp (i0:decimal, k1:single, k2:single, ?O3:single, ?O4:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        ()
    //vexp_i iiio 
    static member vexp_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //vexpseg iin 
    static member vexpseg (i0:decimal, i1:decimal, n2:decimal[]) =
        ()
    //vexpv iikOOO 
    static member vexpv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vexpv_i iiioo 
    static member vexpv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vibes kkiiikkii a
    static member vibes (k0:single, k1:single, i2:decimal, i3:decimal, i4:decimal, k5:single, k6:single, i7:decimal, i8:decimal) =
        vector [(double)0.0]
    //vibr kki k
    static member vibr (k0:single, k1:single, i2:decimal) =
        k0
    //vibrato kkkkkkkkio k
    static member vibrato (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, k6:single, k7:single, i8:decimal, ?o9:decimal) =
        let o9 = defaultArg o9 0m
        k0
    //vincr aa 
    static member vincr (a0:Vector<double>, a1:Vector<double>) =
        ()
    //vlimit ikki 
    static member vlimit (i0:decimal, k1:single, k2:single, i3:decimal) =
        ()
    //vlinseg iin 
    static member vlinseg (i0:decimal, i1:decimal, n2:decimal[]) =
        ()
    //vlowres akkik a
    static member vlowres (a0:Vector<double>, k1:single, k2:single, i3:decimal, k4:single) =
        a0
    //vmap iiioo 
    static member vmap (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vmirror ikki 
    static member vmirror (i0:decimal, k1:single, k2:single, i3:decimal) =
        ()
    //vmult ikkOO 
    static member vmult (i0:decimal, k1:single, k2:single, ?O3:single, ?O4:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        ()
    //vmult_i iiio 
    static member vmult_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //vmultv iikOOO 
    static member vmultv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vmultv_i iiioo 
    static member vmultv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //voice kkkkkkii a
    static member voice (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal) =
        vector [(double)0.0]
    //vosim kkkkkkio a
    static member vosim (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //vphaseseg kiim 
    static member vphaseseg (k0:single, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //vport ikio 
    static member vport (i0:decimal, k1:single, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //vpow ikkOO 
    static member vpow (i0:decimal, k1:single, k2:single, ?O3:single, ?O4:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        ()
    //vpow_i iiio 
    static member vpow_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        ()
    //vpowv iikOOO 
    static member vpowv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vpowv_i iiioo 
    static member vpowv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vpvoc kkSoo a
    static member vpvoc (k0:single, k1:single, s2:string, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //vpvoc kkioo a
    static member vpvoc (k0:single, k1:single, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //vrandh ikkiovoo 
    static member vrandh (i0:decimal, k1:single, k2:single, i3:decimal, ?o4:decimal, ?v5:decimal, ?o6:decimal, ?o7:decimal) =
        let o4 = defaultArg o4 0m
        let v5 = defaultArg v5 0.5m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        ()
    //vrandi ikkiovoo 
    static member vrandi (i0:decimal, k1:single, k2:single, i3:decimal, ?o4:decimal, ?v5:decimal, ?o6:decimal, ?o7:decimal) =
        let o4 = defaultArg o4 0m
        let v5 = defaultArg v5 0.5m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        ()
    //vstaudio iy mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member vstaudio (i0:decimal, y1:Vector<double>[][]) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //vstaudiog iy mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
    static member vstaudiog (i0:decimal, y1:Vector<double>[][]) =
        vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0], vector [(double)0.0]
    //vstbankload iT 
    static member vstbankload (i0:decimal, T1:'a) =
        ()
    //vstbanksave iT 
    static member vstbanksave (i0:decimal, T1:'a) =
        ()
    //vstedit i 
    static member vstedit (i0:decimal) =
        ()
    //vstinfo i 
    static member vstinfo (i0:decimal) =
        ()
    //vstinit To i
    static member vstinit (T0:'a, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        o1
    //vstmidiout ikkkk 
    static member vstmidiout (i0:decimal, k1:single, k2:single, k3:single, k4:single) =
        ()
    //vstnote iiiii 
    static member vstnote (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal) =
        ()
    //vstparamget ik k
    static member vstparamget (i0:decimal, k1:single) =
        k1
    //vstparamset ikk 
    static member vstparamset (i0:decimal, k1:single, k2:single) =
        ()
    //vstprogset ii 
    static member vstprogset (i0:decimal, i1:decimal) =
        ()
    //vsttempo ki 
    static member vsttempo (k0:single, i1:decimal) =
        ()
    //vsubv iikOOO 
    static member vsubv (i0:decimal, i1:decimal, k2:single, ?O3:single, ?O4:single, ?O5:single) =
        let O3 = defaultArg O3 0.f
        let O4 = defaultArg O4 0.f
        let O5 = defaultArg O5 0.f
        ()
    //vsubv_i iiioo 
    static member vsubv_i (i0:decimal, i1:decimal, i2:decimal, ?o3:decimal, ?o4:decimal) =
        let o3 = defaultArg o3 0m
        let o4 = defaultArg o4 0m
        ()
    //vtaba aiy 
    static member vtaba (a0:Vector<double>, i1:decimal, y2:Vector<double>[][]) =
        ()
    //vtabi iim 
    static member vtabi (i0:decimal, i1:decimal, m2:Vector<double>) =
        ()
    //vtabk kiz 
    static member vtabk (k0:single, i1:decimal, z2:single[]) =
        ()
    //vtable1k kz 
    static member vtable1k (k0:single, z1:single[]) =
        ()
    //vtablea akkiy 
    static member vtablea (a0:Vector<double>, k1:single, k2:single, i3:decimal, y4:Vector<double>[][]) =
        ()
    //vtablei iiiim 
    static member vtablei (i0:decimal, i1:decimal, i2:decimal, i3:decimal, m4:Vector<double>) =
        ()
    //vtablek kkkiz 
    static member vtablek (k0:single, k1:single, k2:single, i3:decimal, z4:single[]) =
        ()
    //vtablewa akiy 
    static member vtablewa (a0:Vector<double>, k1:single, i2:decimal, y3:Vector<double>[][]) =
        ()
    //vtablewi iiim 
    static member vtablewi (i0:decimal, i1:decimal, i2:decimal, m3:Vector<double>) =
        ()
    //vtablewk kkiz 
    static member vtablewk (k0:single, k1:single, i2:decimal, z3:single[]) =
        ()
    //vtabwa aiy 
    static member vtabwa (a0:Vector<double>, i1:decimal, y2:Vector<double>[][]) =
        ()
    //vtabwi iim 
    static member vtabwi (i0:decimal, i1:decimal, m2:Vector<double>) =
        ()
    //vtabwk kiz 
    static member vtabwk (k0:single, i1:decimal, z2:single[]) =
        ()
    //vwrap ikki 
    static member vwrap (i0:decimal, k1:single, k2:single, i3:decimal) =
        ()
    //waveset ako a
    static member waveset (a0:Vector<double>, k1:single, ?o2:decimal) =
        let o2 = defaultArg o2 0m
        a0
    //weibull<'A> kk  -> (a | i | k)

    static member weibull<'A> (k0:single, k1:single) =

        Unchecked.defaultof<'A>
    //wgbow kkkkkkio a
    static member wgbow (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //wgbowedbar kkkkkoooo a
    static member wgbowedbar (k0:single, k1:single, k2:single, k3:single, k4:single, ?o5:decimal, ?o6:decimal, ?o7:decimal, ?o8:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        let o7 = defaultArg o7 0m
        let o8 = defaultArg o8 0m
        vector [(double)0.0]
    //wgbrass kkkikkio a
    static member wgbrass (k0:single, k1:single, k2:single, i3:decimal, k4:single, k5:single, i6:decimal, ?o7:decimal) =
        let o7 = defaultArg o7 0m
        vector [(double)0.0]
    //wgclar kkkiikkkio a
    static member wgclar (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, k5:single, k6:single, k7:single, i8:decimal, ?o9:decimal) =
        let o9 = defaultArg o9 0m
        vector [(double)0.0]
    //wgflute kkkiikkkiovv a
    static member wgflute (k0:single, k1:single, k2:single, i3:decimal, i4:decimal, k5:single, k6:single, k7:single, i8:decimal, ?o9:decimal, ?v10:decimal, ?v11:decimal) =
        let o9 = defaultArg o9 0m
        let v10 = defaultArg v10 0.5m
        let v11 = defaultArg v11 0.5m
        vector [(double)0.0]
    //wgpluck iikiiia a
    static member wgpluck (i0:decimal, i1:decimal, k2:single, i3:decimal, i4:decimal, i5:decimal, a6:Vector<double>) =
        a6
    //wgpluck2 ikikk a
    static member wgpluck2 (i0:decimal, k1:single, i2:decimal, k3:single, k4:single) =
        vector [(double)0.0]
    //wguide1 axkk a
    static member wguide1 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, k2:single, k3:single) =
        a0
    //wguide2 axxkkkk a
    static member wguide2 (a0:Vector<double>, x1:CSoundOpcode.ScalarOrArray, x2:CSoundOpcode.ScalarOrArray, k3:single, k4:single, k5:single, k6:single) =
        a0
    //window k[]Op k[]
    static member window (k0:single, M1:decimal[], ?O2:single, ?p3:decimal) =
        let O2 = defaultArg O2 0.f
        let p3 = defaultArg p3 1m
        k0, M1, 0.0
    //wrap akk a
    static member wrap (a0:Vector<double>, k1:single, k2:single) =
        a0
    //wrap iii i
    static member wrap (i0:decimal, i1:decimal, i2:decimal) =
        i0
    //wrap kkk k
    static member wrap (k0:single, k1:single, k2:single) =
        k0
    //writescratch io 
    static member writescratch (i0:decimal, ?o1:decimal) =
        let o1 = defaultArg o1 0m
        ()
    //wterrain kkkkkkii a
    static member wterrain (k0:single, k1:single, k2:single, k3:single, k4:single, k5:single, i6:decimal, i7:decimal) =
        vector [(double)0.0]
    //xadsr<'A> iiiio  -> (a | k)

    static member xadsr<'A> (i0:decimal, i1:decimal, i2:decimal, i3:decimal, ?o4:decimal) =

        let o4 = defaultArg o4 0m

        Unchecked.defaultof<'A>
    //xin  ****************
    static member xin () =
        ()
    //xout * 
    static member xout () =
        ()
    //xscanmap ikko kk
    static member xscanmap (i0:decimal, k1:single, k2:single, ?o3:decimal) =
        let o3 = defaultArg o3 0m
        k1, k1
    //xscans kkiio a
    static member xscans (k0:single, k1:single, i2:decimal, i3:decimal, ?o4:decimal) =
        let o4 = defaultArg o4 0m
        vector [(double)0.0]
    //xscansmap kkikko 
    static member xscansmap (k0:single, k1:single, i2:decimal, k3:single, k4:single, ?o5:decimal) =
        let o5 = defaultArg o5 0m
        ()
    //xscanu iiiiSiikkkkiikkaii 
    static member xscanu (i0:decimal, i1:decimal, i2:decimal, i3:decimal, s4:string, i5:decimal, i6:decimal, k7:single, k8:single, k9:single, k10:single, i11:decimal, i12:decimal, k13:single, k14:single, a15:Vector<double>, i16:decimal, i17:decimal) =
        ()
    //xscanu iiiiiiikkkkiikkaii 
    static member xscanu (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, i5:decimal, i6:decimal, k7:single, k8:single, k9:single, k10:single, i11:decimal, i12:decimal, k13:single, k14:single, a15:Vector<double>, i16:decimal, i17:decimal) =
        ()
    //xtratim i 
    static member xtratim (i0:decimal) =
        ()
    //xyin iiiiioo kk
    static member xyin (i0:decimal, i1:decimal, i2:decimal, i3:decimal, i4:decimal, ?o5:decimal, ?o6:decimal) =
        let o5 = defaultArg o5 0m
        let o6 = defaultArg o6 0m
        0.0f, 0.0f
    //zacl kk 
    static member zacl (k0:single, k1:single) =
        ()
    //zakinit ii 
    static member zakinit (i0:decimal, i1:decimal) =
        ()
    //zamod ak a
    static member zamod (a0:Vector<double>, k1:single) =
        a0
    //zar k a
    static member zar (k0:single) =
        vector [(double)0.0]
    //zarg kk a
    static member zarg (k0:single, k1:single) =
        vector [(double)0.0]
    //zaw ak 
    static member zaw (a0:Vector<double>, k1:single) =
        ()
    //zawm akp 
    static member zawm (a0:Vector<double>, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
    //zfilter2 akkiim a
    static member zfilter2 (a0:Vector<double>, k1:single, k2:single, i3:decimal, i4:decimal, m5:Vector<double>) =
        a0
    //zir i i
    static member zir (i0:decimal) =
        i0
    //ziw ii 
    static member ziw (i0:decimal, i1:decimal) =
        ()
    //ziwm iip 
    static member ziwm (i0:decimal, i1:decimal, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
    //zkcl kk 
    static member zkcl (k0:single, k1:single) =
        ()
    //zkmod kk k
    static member zkmod (k0:single, k1:single) =
        k0
    //zkr k k
    static member zkr (k0:single) =
        k0
    //zkw kk 
    static member zkw (k0:single, k1:single) =
        ()
    //zkwm kkp 
    static member zkwm (k0:single, k1:single, ?p2:decimal) =
        let p2 = defaultArg p2 1m
        ()
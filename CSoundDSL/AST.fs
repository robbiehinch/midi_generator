﻿module AST



//https://github.com/csound/csound/blob/develop/Engine/entry1.c

type FnParam =
        | InstParam of char             //i
        | ControlParam of char          //k
        | ArrayParam of char            //a
        | ScalarOrArrayParam of char    //x
        | StreamingParam of char        //f	f-rate streaming pvoc fsig type
        | StringParam of char           //S	String
        | BParam of char
        | LParam of char
        | InstParams of char//m	Begins an indefinite list of i-rate arguments (any count)
        | AnyParams of char//M	Begins an indefinite list of arguments (any rate, any count)
        | AnyOptionalParam of char//N	Begins an indefinite list of (optional a-, k-, i-, or S-rate)-rate arguments (any odd count)
        | InstParamsOddParam of char//n	Begins an indefinite list of i-rate arguments (any odd count)
        | ControlOptionParam0 of char//O	Optional k-rate, defaulting to 0
        | InstOptionParam0 of char//o	Optional i-rate, defaulting to 0
        | InstOptionParam1 of char//p	Optional i-rate, defaulting to 1
        | InstOptionParam10 of char//q	Optional i-rate, defaulting to 10
        | ControlOptionParam0_5 of char//V	Optional k-rate, defaulting to 0.5
        | InstOptionParam0_5 of char//v	Optional i-rate, defaulting to 0.5
        | InstOptionParamMinus1 of char//j	Optional i-rate, defaulting to -1
        | InstOptionParam127 of char//h	Optional i-rate, defaulting to 127
        | ArrayListParam of char//y	Begins an indefinite list of a-rate arguments (any count)
        | ControlListParam of char//z	Begins an indefinite list of k-rate arguments (any count)
        | ControlArrayListParam of char//Z	Begins an indefinite list of alternating k-rate and a-rate arguments (kaka...) (any count)
        | StringOrIRateParam of char//T       String or i-rate
        | StringOrIKRateParam of char//U       String or i/k-rate
        | IgnoreParam of char//.

/// See TypeScript Specification Appendix A.9 
/// (http://typescript.codeplex.com/releases/view/105503)
type FnParams = FnParams of FnParam list


﻿
#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#I @"..\packages\FParsec.1.0.2\lib\net40-client\"
#r @"FParsec.dll"
#r @"FParsecCS.dll"

open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

#load "AST.fs"
#load "CSoundOpcode.fs"
#load "CSoundFunctionGenVector.fs"
    
    
open CSoundCGVector
//
//sprintf "  instr %d, %s ; play audio from function table using loscil p4: Amp, p5: Pitch, p6: Pan" instrNum sampleLabel;
//    "kAmp         =         p4/127   ; amplitude
//        kPitch       =         powoftwo((60.0-p5)/12.0)  ; pitch/speed
//        kLoopStart   =         0   ; point where looping begins (in seconds)
//        kPan         =   p6";
//sprintf "printf \"playing: %s %%d, amp: %%f, pitch: %%f, pan: %%f\\n\", 1, %s, kAmp, kPitch, kPan" sampleLabel sampleChoice;
//sprintf "iSampleId = %s[%s]" sampleArray sampleChoice;
//sprintf "%s = (%s + 1) %% lenarray(%s)" sampleChoice sampleChoice sampleArray;
//sprintf "printf \"sample new val: %%d, arr len: %%d\\n\", 1, %s, lenarray(%s)" sampleChoice sampleArray;
//    "kLoopEnd     =         nsamp(iSampleId)/sr; loop end (end of file)
//        kCrossFade   =         0   ; cross-fade time
//        ; read audio from the function table using the flooper2 opcode
//        aSigL, aSigR loscil kAmp, kPitch, iSampleId, 1, 0
//        kPanL min (1.0-kPan), 0.5
//        kPanR min kPan, 0.5
//        kPanL = kPanL * 2
//        kPanR = kPanR * 2
//        printf \"kPanL: %f, kPanR: %f\\n\", 1, kPanL, kPanR
//        aSigLPan = aSigL * kPanL
//        aSigRPan = aSigR * kPanR
//        outs       aSigLPan, aSigRPan ; send audio to output
//            endin
//        "
//
//let sampleArray = [|1m|]
//let sampleChoice = 0
//let sr = 441000m
//
//[<ReflectedDefinition>]
//let myfn p0 p1 p2 p3 p4 p5 p6 =
//    let kAmp = p4/127s   //amplitude
//    let kPitch = Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
//    let kLoopStart = 0   //; point where looping begins (in seconds)
//    let kPan = p6//";
////    Functions.printf "playing: %s %%d, amp: %%f, pitch: %%f, pan: %%f\\n", 1, %s, kAmp, kPitch, kPan sampleLabel sampleChoice
//    let iSampleId = sampleArray.[sampleChoice]//;
//    let sampleChoice = (sampleChoice + 1) % (int)(Functions.lenarray sampleArray)
////    sprintf "printf \"sample new val: %%d, arr len: %%d\\n\", 1, %s, lenarray(%s)" sampleChoice sampleArray;
//    let kLoopEnd = (Functions.nsamp iSampleId)/sr//; loop end (end of file)
//    let kCrossFade = 0//   ; cross-fade time
////            ; read audio from the function table using the flooper2 opcode
//    let aSigL, aSigR = Functions.loscil(kAmp, kPitch, iSampleId, 1m, 0m)
//    let kPanL = Functions.min (1.0f-kPan, [|0.5f|])
//    let kPanR = Functions.min (kPan, [|0.5f|])
//    let kPanL = kPanL * 2.f
//    let kPanR = kPanR * 2.f
////            printf \"kPanL: %f, kPanR: %f\\n\", 1, kPanL, kPanR
//    let aSigLPan = aSigL * kPanL
//    let aSigRPan = aSigR * kPanR
//    aSigLPan, aSigRPan //; send audio to output
////    let outs = aSigLPan, aSigRPan //; send audio to output
////    outs
////                endin

#load "Compiler.fs"
//<@@ myfn @@> |> Compiler.compile
//
//
[<ReflectedDefinition>]
let tuplefn p0 =
    let aSigL, aSigR = Functions.loscil(CSoundOpcode.ScalarOrArray.Scalar 1.0, 1.f, 1m, 1m, 0m)
    aSigL * p0, aSigR * p0 //; send audio to output
<@@ tuplefn 2.f @@> |> Compiler.compile

//
//
//type SampleToCSoundInstrumentSimple () =
//
//    [<ReflectedDefinition>]
//    member x.myfn p0 =
//        let aSigL, aSigR = CSoundCG.Functions.loscil(1s, 1.f, 1m, 1m, 0m)
////        aSigL * p0, aSigR * p0 //; send audio to output
//        CSoundCG.Functions.outs [| [| aSigL; aSigR |] |]//; send audio to output
//
//    member x.qfn() =
//    
//        let mis = x.GetType().GetMembers()
//        let fn = mis |> Array.find (fun m -> m.Name.Contains("myfn"))
//        let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn :?> System.Reflection.MethodBase)
//        let retStr = e.Value |> Compiler.compile
////        let retStr = <@@ x.myfn @@> |> Compiler.compile
//        retStr
//
//
//let cs = SampleToCSoundInstrumentSimple()
//let x = cs.qfn ()
//<@@ cs.myfn @@> |> Compiler.compile
//
//
//type SampleToCSoundInstrumentQuotation (sr, label:string) =
//
//    let mutable giSamples = [||]
//    let mutable gi_SAMPLE_CHOICE = 0
//    let mutable gi_SAMPLE_ARRAY = [||]
//
//    member x.replacePlaceHolders (placeHolderStr:string) =
//        placeHolderStr.Replace("gi_SAMPLE_CHOICE", sprintf "gi%s" label)
//                        .Replace("gi_SAMPLE_ARRAY", sprintf "gi%sArr" label)
//
////    [<ReflectedDefinition>]
////    member x.globalInit =
////        giSamples <- flatGroups |> Seq.map (fun r -> CSoundCG.Functions.ftgen 0m 0m 0m 1m (r.samplePath) 0m) |> Seq.toArray
////        gi_SAMPLE_CHOICE <- CSoundCG.Functions.init 0m
////        gi_SAMPLE_ARRAY <- CSoundCG.Functions.fillarray giSamples
//
//    [<ReflectedDefinition>]
//    member x.myfn p0 p1 p2 p3 p4 p5 p6 =
//        let kAmp = p4/127s   //amplitude
//        let kPitch = CSoundCG.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
//        let kLoopStart = 0   //; point where looping begins (in seconds)
//        let kPan = p6//";
//    //    Functions.printf "playing: %s %%d, amp: %%f, pitch: %%f, pan: %%f\\n", 1, %s, kAmp, kPitch, kPan sampleLabel sampleChoice
//        let iSampleId = gi_SAMPLE_ARRAY.[(int)gi_SAMPLE_CHOICE]//;
//        gi_SAMPLE_CHOICE <- (gi_SAMPLE_CHOICE + 1) % (int)(CSoundCG.Functions.lenarray gi_SAMPLE_ARRAY)
//    //    sprintf "printf \"sample new val: %%d, arr len: %%d\\n\", 1, %s, lenarray(%s)" sampleChoice sampleArray;
//        let kLoopEnd = (CSoundCG.Functions.nsamp iSampleId)/sr//; loop end (end of file)
//        let kCrossFade = 0//   ; cross-fade time
//    //            ; read audio from the function table using the flooper2 opcode
//        let aSigL, aSigR = CSoundCG.Functions.loscil(kAmp, kPitch, iSampleId, 1m, 0m)
//        let kPanL = CSoundCG.Functions.min (1.0f-kPan, [|0.5f|])
//        let kPanR = CSoundCG.Functions.min (kPan, [|0.5f|])
//        let kPanL = kPanL * 2.f
//        let kPanR = kPanR * 2.f
//    //            printf \"kPanL: %f, kPanR: %f\\n\", 1, kPanL, kPanR
//        let aSigLPan = aSigL * kPanL
//        let aSigRPan = aSigR * kPanR
//        CSoundCG.Functions.outs [| [| aSigLPan; aSigRPan |] |]//; send audio to output
//
//    member x.qfn () =
//        let mis = x.GetType().GetMembers()
//        let fn = mis |> Array.find (fun m -> m.Name.Contains("myfn"))
//        let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn :?> System.Reflection.MethodBase)
//        let retStr = e.Value |> Compiler.compile
//        x.replacePlaceHolders retStr
//
//        
//let csq = SampleToCSoundInstrumentQuotation(41000m, "BassDrm")
//csq.qfn ()

#load "Compiler.fs"
type TheType () =
    let mutable giSine = 5m

    [<ReflectedDefinition>]
    member x.kick a1 =
    //    let y = CSoundCGVector.Functions.ftgen(0m, 0m, 4096m, 10m, 1m, [||])//	;SINE WAVE
        giSine <- CSoundCGVector.Functions.ftgen(0m, 0m, 4096m, 10m, 1m, [||])//	;SINE WAVE
        ()

let t = TheType()
let e = t.GetType().GetMember("kick").[0] :?> System.Reflection.MethodBase
let q = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(e)
q.Value |> Compiler.compile


//[<ReflectedDefinition>]
//let rec IndexOf((arr:_[]), searchVal, currIndex) =
//    let foundVal = arr.[currIndex]
//    let iOut = if foundVal > searchVal then
//                    currIndex
//                else
//                    IndexOf(arr, searchVal, currIndex+1)    
//    CSoundCG.Functions.xout iOut
//
//<@@ IndexOf @@> |> Compiler.compile

#load "Compiler.fs"

[<ReflectedDefinition>]
let OpenHiHat () =

    let aenvL = CSoundCGVector.Functions.expon(1m, 0.2m, 0.001m)
    let aenvR = CSoundCGVector.Functions.expon(1m, 0.2m, 0.001m)

    Compiler.OutputHook [| [| aenvL; aenvR |] |]
//    CSoundCGVector.Functions.outs [| [| aSigLPan; aSigRPan |] |]//; send audio to output


let outsHook (exprList:Quotations.Expr list) =
//    let expressionReduce = exprList |> List.fold (fun acc elem -> <@ %acc %elem @>) Quotations.Expr.
    let exprHead =  Quotations.Expr.Cast<Vector<double>[][]> exprList.Head
    <@@ CSoundCGVector.Functions.outs %exprHead @@> |> Compiler.compileInstrument (fun es -> System.String.Empty, { Compiler.CompileState.IsMultiParamFn = false }), { Compiler.CompileState.IsMultiParamFn = false }
//    <@@ CSoundCGVector.Functions.outs %%exprHead @@> |> Compiler.compileInstrument (fun es -> System.String.Empty, { Compiler.CompileState.IsMultiParamFn = false })


<@@ OpenHiHat @@> |> Compiler.compileInstrument outsHook

//<@@ OpenHiHatMixed @@> |> Compiler.compile
//<@@ OpenHiHat CSoundCGVector.Functions.outs @@> |> Compiler.compile





[<ReflectedDefinition>]
let rec IndexOf (iAin:Vector<double>) iValSearch iCurrIndex =
    let mutable iOut = 0
    let iFoundVal = iAin.[iCurrIndex]
    if iFoundVal > iValSearch then
//        iCurrIndex
        iOut <- iCurrIndex
    else
//        IndexOf iAin iValSearch (iCurrIndex+1)
        iOut <- IndexOf iAin iValSearch (iCurrIndex+1)

    iOut


//opcode IndexOf, i, i[]ii

//iAin[], iValSearch, iCurrIndex xin             ; read input parameters
//iOut = 0
//
//	iFoundVal = iAin[iCurrIndex]
//	if (iFoundVal > iValSearch) then
//		iOut = iCurrIndex
//	else
//		iOut IndexOf iAin, iValSearch, (iCurrIndex+1)
//	endif
//
//
//xout iOut
//endop

﻿module VarExtract

open Microsoft.FSharp.Quotations
open QuotationActivePatterns

open MathNet.Numerics.LinearAlgebra.Double
//let vec = vector [0.0]

let findVars exprIn =
    
    let rec findVarsRec exprMatch =
        match exprMatch with
        | Patterns.Application(expr1, expr2) ->
            // Function application.
            let r1 = findVarsRec expr1
            let r2 = findVarsRec expr2
            List.append r1 r2
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.int @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.decimal @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.float32 @> (_, _, exprList)
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.Operators.float @> (_, _, exprList) ->
            exprList |> List.map findVarsRec |> List.concat
        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.LanguagePrimitives.IntrinsicFunctions.GetArray @> (_, _, exprList) ->
            let r1 = findVarsRec exprList.Head
            let r2 = findVarsRec exprList.Tail.Head
            [ sprintf "%s[%s]" (r1.Head) (r2.Head) ]
//        | DerivedPatterns.SpecificCall <@ Microsoft.FSharp.Core.LanguagePrimitives.IntrinsicFunctions.SetArray @> (_, _, exprList) ->
//            let r1 = findVarsRec exprList.Head
//            let r2 = findVarsRec exprList.Tail.Head
//            let r3 = findVarsRec exprList.Tail.Tail.Head
//            sprintf "%s[%s] = %s" (fst r1) (fst r2) (fst r3), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ (+) @> (_, _, exprList) ->
//            sprintf "( %s )" (exprList |> List.map findVarsRec |> mapFstArray " + "), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ (-) @> (_, _, exprList) ->
//            sprintf "( %s )" (exprList |> List.map findVarsRec |> mapFstArray " - "), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ (*) @> (_, _, exprList) ->
//            sprintf "( %s )" (exprList |> List.map findVarsRec |> mapFstArray " * "), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ (/) @> (exprOpt, methodInfo, exprList) ->
//            sprintf "( %s )" (exprList |> List.map findVarsRec |> mapFstArray " / "), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ (%) @> (exprOpt, methodInfo, exprList) ->
//            sprintf "( %s )" (exprList |> List.map findVarsRec |> mapFstArray " % "), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.SpecificCall <@ vec.PointwiseMultiply @> (instanceExprOption, _, exprList) ->
//            let objVar, _ = match instanceExprOption with
//                            | Some instanceExpr -> findVarsRec instanceExpr
//                            | _ -> System.String.Empty, { CompileState.IsMultiParamFn = false }
//            sprintf "( %s * %s )" objVar (exprList |> List.map findVarsRec |> mapFstArray " * "), { CompileState.IsMultiParamFn = false }
        | Patterns.NewArray(_, exprList)
        | Patterns.NewTuple(exprList)
        | Patterns.NewUnionCase(_, exprList)
        | DerivedPatterns.SpecificCall <@ vector @> (_, _, exprList) ->
            exprList |> List.map findVarsRec |> List.concat
//        | DerivedPatterns.Decimal(n) ->
//            [ n.ToString() ]
//        | DerivedPatterns.Int32(n) ->
//            [ n.ToString() ]
        | Patterns.Call(None, DerivedPatterns.MethodWithReflectedDefinition(n), expList) ->
//            printf "MethodWithReflectedDefinition"
            findVarsRec n
//        | Patterns.Call(exprOpt, methodInfo, exprList) ->
//            // Method or module function call.
//            let opt = match exprOpt with
//                                | Some e -> fst (findVarsRec e)
//                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
//            let callParams = exprList |> List.map findVarsRec
//                                    |> List.map fst
//                                    |> List.filter (fun e -> System.String.IsNullOrWhiteSpace(e) |> not)
//                                    |> List.toArray
//            let callParmStr = callParams |> String.concat ", "
//            let methodName = methodInfo.Name
//            if methodName <> "chnget" && callParmStr.Contains(", ") |> not then
//                sprintf "%s %s( %s )" opt methodName callParmStr, { CompileState.IsMultiParamFn = false }
//            else
//                sprintf "%s %s %s" opt methodName callParmStr, { CompileState.IsMultiParamFn = true }
//
        | Patterns.Lambda(param, body) ->
            // Lambda expression.
//            printf "fun (%s:%s) -> " param.Name (param.Type.ToString())
            findVarsRec body
        | TupleLet(vars, expr1, expr2) ->
            //if any are mutable, they are all mutable
            let varNames = vars |> List.map (function | Some(v) -> v.Name | None -> "_")
            varNames
        | Patterns.Let(var, expr1, expr2) ->
            let e1Compiled = findVarsRec expr1
            let e2Compiled = findVarsRec expr2
            (var.Name)::(e1Compiled |> List.append e2Compiled)
//        | Patterns.TupleGet(expr, i) ->
//            sprintf "%s.[%d]" (findVarsRec expr |> fst) i, { CompileState.IsMultiParamFn = false }
//        | Patterns.PropertyGet(_, propOrValInfo, _) ->
//            propOrValInfo.Name, { CompileState.IsMultiParamFn = false }
//        | Patterns.PropertySet(_, propOrValInfo, _, e) ->
//            sprintf "%s %s" (propOrValInfo.Name) (findVarsRec e |> fst), { CompileState.IsMultiParamFn = false }
//        | DerivedPatterns.String(str) ->
//            sprintf "\"%s\"" str, { CompileState.IsMultiParamFn = false }
//        | Patterns.Value(value, typ) ->
//            (if null <> value then value.ToString() else System.String.Empty), { CompileState.IsMultiParamFn = false }
//        | Patterns.Sequential(expr1, expr2) ->
//            let e1, _ = findVarsRec expr1
//            let e2, _ = findVarsRec expr2
//            sprintf "%s %s %s" e1 System.Environment.NewLine e2, { CompileState.IsMultiParamFn = false }
//        | Patterns.FieldSet(exprOpt, fi, expr) ->
//            let opt = match exprOpt with
//                                | Some expr -> fst (findVarsRec expr)
//                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
//            let rhs, rhsState = findVarsRec expr
//            let assignExpr = if rhsState.IsMultiParamFn then " " else " = "
//            sprintf "%s%s%s" fi.Name assignExpr rhs, { CompileState.IsMultiParamFn = false }
//        | Patterns.FieldGet(exprOpt, fi) ->
//            let opt = match exprOpt with
//                                | Some expr -> fst (findVarsRec expr)
//                                | None -> ""//printf "%s" methodInfo.DeclaringType.Name
//            sprintf "%s" fi.Name, { CompileState.IsMultiParamFn = false }
        | Patterns.Var(var) ->
            [ var.Name ]
//        | Patterns.IfThenElse(condExpr, tbExpr, fbExpr) ->
//            let cond, _ = findVarsRec condExpr
//            let tb, _ = findVarsRec tbExpr
//            let fb, _ = findVarsRec fbExpr
//            sprintf "if ( %s ) then
//                        %s
//                        else
//                        %s
//                        endif" cond tb fb, { CompileState.IsMultiParamFn = false }
//        | Patterns.Coerce(expr, _) ->
//            findVarsRec expr
        | _ -> [  ]
//        | _ -> [ sprintf "%s ;Unhandled" (exprMatch.ToString()) ]
    findVarsRec exprIn


    
let getVars fn =
    let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn)
    e.Value |> findVars
    

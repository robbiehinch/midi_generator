﻿module QuotationActivePatterns

open Microsoft.FSharp.Quotations

////////Copied from Unquote because not public functions of the module

    
//suprisingly, this is actually used twice.
///Test whether the Expr is a Var and equals the given Var property-wise
let private isVarOfExpr (x:Var) = function
    | Patterns.Var y | Patterns.Coerce(Patterns.Var y,_) -> x.Name = y.Name && x.Type = y.Type && x.IsMutable = y.IsMutable
    | _ -> false


///Test whether the given expression represents a tuple let binding: e.g. let x,y = 1,2.
///Must come before Let pattern and after IncompleteLambdaCall pattern.
let (|TupleLet|_|) x =
    //N.B. breaking out the two TupleLetStart variations allows us to using | pattern match with start and body binding.

    ///TupleLet start variation 1) let a = TupleGet(tupleProperty, index) in let b = TupleGet(tupleProperty, index) in ...
    let (|PropertyTuple|_|) = function
        | (Patterns.Let(_,Patterns.TupleGet(propertyTuple, _),_) as bindings) ->
            Some(bindings, propertyTuple)
        | _ -> None

    ///TupleLet start variation 2) let patternInput = expression in let a = TupleGet(patternInput, index) in ...
    let (|PatternInputTuple|_|) = function
        //this is getting a little crazy, but it is the observed pattern, and pi = piAgain is a necessary restriction
        //so as to not have too wide a net.
        | Patterns.Let(var, patternInputTuple, (Patterns.Let(_,Patterns.TupleGet(varExpr,_),_) as bindings)) when isVarOfExpr var varExpr ->
            Some(bindings, patternInputTuple)
        | _ -> None

    match x with
    | PropertyTuple(bindings,tuple) | PatternInputTuple(bindings,tuple) ->
        //e.g., the "var index list" for let (a,_,b,_,_,c,_,_,_) would be [(a,0);(b,2);(c,5)]
        //order can either be forward (lambda expressions) or in reverse (normal tuple let bindings)
        let tupledVars = Array.create (Microsoft.FSharp.Reflection.FSharpType.GetTupleElements(tuple.Type).Length) (None:Var option)
        let rec fillVarsAndGetBody = function
            | Patterns.Let(var,Patterns.TupleGet(_,index),next) ->
                tupledVars.[index] <- Some(var)
                fillVarsAndGetBody next
            | final -> final
        
        let body = fillVarsAndGetBody bindings

        Some(tupledVars |> Array.toList, tuple, body)
    | _ -> None
///////////////////////////////////////////////////

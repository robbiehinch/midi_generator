﻿
#r @"..\packages\FSharp.Compiler.CodeDom.0.9.3.0\lib\net40\FSharp.Compiler.CodeDom.dll"
#r @"F:\src\music\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#I @"..\packages\FParsec.1.0.2\lib\net40-client\"
#r @"FParsec.dll"
#r @"FParsecCS.dll"

#load "AST.fs"
#load "CSoundOpcode.fs"
#load "CSoundCodeDomGen.fs"


/// wgpluck iikiiia a
let opcodes = csound6netlib.OpcodeArgumentTypes()
opcodes.intypes <- "iikiiia"
opcodes.outypes <- "a"
let opcodeList = [opcodes]
let opcodeDict = [("wgpluck", opcodeList)] |> dict

let parsed = CSoundCodeDomGen.createFunction "wgpluck" opcodes
let code = CSoundCodeDomGen.generateTypes [parsed]


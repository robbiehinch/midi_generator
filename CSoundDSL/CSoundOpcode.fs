﻿module CSoundOpcode

open FParsec
open AST
open System.CodeDom
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

let test p str =
    match run p str with
    | Success(result, _, _)   -> printfn "Success: %A" result
    | Failure(errorMsg, _, _) -> printfn "Failure: %s" errorMsg

let declarationElement =
    choice [
        pchar '[' |>> AST.AnyParams    //hacked in by me to cover ones that look like eg lenarray .[]p
        pchar ']' |>> AST.IgnoreParam    //hacked in by me to cover ones that look like eg lenarray .[]p
        pchar 'a' |>> AST.ArrayParam
        pchar 'k' |>> AST.ControlParam
        pchar 'i' |>> AST.InstParam
        pchar 'x' |>> AST.ScalarOrArrayParam
        pchar 'f' |>> AST.StreamingParam
        pchar 'S' |>> AST.StringParam
        pchar 'B' |>> AST.BParam
        pchar 'l' |>> AST.LParam
        pchar 'm' |>> AST.InstParams
        pchar 'M' |>> AST.AnyParams
        pchar 'N' |>> AST.AnyOptionalParam
        pchar 'n' |>> AST.InstParamsOddParam
        pchar 'O' |>> AST.ControlOptionParam0
        pchar 'o' |>> AST.InstOptionParam0
        pchar 'p' |>> AST.InstOptionParam1
        pchar 'q' |>> AST.InstOptionParam10
        pchar 'V' |>> AST.ControlOptionParam0_5
        pchar 'v' |>> AST.InstOptionParam0_5
        pchar 'j' |>> AST.InstOptionParamMinus1
        pchar 'h' |>> AST.InstOptionParam127
        pchar 'y' |>> AST.ArrayListParam
        pchar 'z' |>> AST.ControlListParam
        pchar 'Z' |>> AST.ControlArrayListParam
        pchar 'T' |>> AST.StringOrIRateParam
        pchar 'U' |>> AST.StringOrIKRateParam
        pchar '.' |>> AST.IgnoreParam
    ]

let fnParams : Parser<_, unit> = 
    many declarationElement
    |>> AST.FnParams

type ScalarOrArray = 
    | Scalar of float
    | Array of Vector<float>

let x (y:ScalarOrArray) = match y with
                            | Scalar s -> s.ToString()
                            | Array a -> a.ToString()

let paramMappings = [|
    "Vector<double>", "a", []
    "single", "k", []
    "decimal", "i", []
    "CSoundOpcode.ScalarOrArray", "x", []
    "_[]", "S", []
    "string", "s", []
    "_", "B", []
    "_", "l", []
//    "decimal[]", "m", [] //this looks like what it should be according to spec
    "Vector<double>", "m", []
    "decimal[]", "M", []
//    "'a[]", "M", []
    "Object[]", "[<ParamArray>] N", [""]
    "decimal[]", "n", []
    "single", "O", ["0.f"]
    "decimal", "o", ["0m"]
    "decimal", "p", ["1m"]
    "decimal", "q", ["10m"]
    "single", "V", ["0.5f"]
    "decimal", "v", ["0.5m"]
    "decimal", "j", ["-0.1m"]
    "decimal", "h", ["127m"]
    "Vector<double>[][]", "y", []
    "single[]", "z", []
    "single[][]", "Z", []
    "'a", "T", []
    "'a", "U", []
    null, ".", []
|]


let paramMappingASTDict = paramMappings |> Array.map (fun (fst, cst, d) -> cst, (fst, d))
                                        |> Map.ofArray

let fsharpTypeToCSoundTypeMap = paramMappings |> Array.map (fun (fst, cst, d) -> fst, cst)
                                                |> Map.ofArray

let mappedParams cst =
    let fst, d = paramMappingASTDict.[cst]
    fst, cst, d
    
let mapParamStrVector p = match p with
                            | AST.ArrayParam _ -> mappedParams "a"
                            | AST.ControlParam _ -> mappedParams "k"
                            | AST.InstParam _ -> mappedParams "i"
                            | AST.ScalarOrArrayParam _ -> mappedParams "x"
                            | AST.StreamingParam _ -> mappedParams "S"
                            | AST.StringParam _ -> mappedParams "s"
                            | AST.BParam _ -> mappedParams "B"
                            | AST.LParam _ -> mappedParams "l"
                            | AST.InstParams _ -> mappedParams "m"
                            | AST.AnyParams _ -> mappedParams "M"
                            | AST.AnyOptionalParam _ -> mappedParams "[<ParamArray>] N"
                            | AST.InstParamsOddParam _ -> mappedParams "n"
                            | AST.ControlOptionParam0 _ -> mappedParams "O"
                            | AST.InstOptionParam0 _ -> mappedParams "o"
                            | AST.InstOptionParam1 _ -> mappedParams "p"
                            | AST.InstOptionParam10 _ -> mappedParams "q"
                            | AST.ControlOptionParam0_5 _ -> mappedParams "V"
                            | AST.InstOptionParam0_5 _ -> mappedParams "v"
                            | AST.InstOptionParamMinus1 _ -> mappedParams "j"
                            | AST.InstOptionParam127 _ -> mappedParams "h"
                            | AST.ArrayListParam _ -> mappedParams "y"
                            | AST.ControlListParam _ -> mappedParams "z"
                            | AST.ControlArrayListParam _ -> mappedParams "Z"
                            | AST.StringOrIRateParam _ -> mappedParams "T"
                            | AST.StringOrIKRateParam _ -> mappedParams "U"
                            | AST.IgnoreParam _ -> mappedParams "."


let fsTypeToCsType fst = 
    let cst, d = paramMappingASTDict.[fst]
    fst, cst, d

let successHandler mapFn fParams = match fParams with
                                    | AST.FnParams p -> p |> List.map mapFn

let parseArgumentDefinition mapFn str = 
    match run fnParams str with
    | ParserResult.Success(r,_,_) -> r |> successHandler mapFn
    | ParserResult.Failure(msg,err,_) -> List.Empty

type InstControlRet =
        | InstParam of decimal             //i
        | ControlParam of single          //k


﻿module ChordEvolver.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting


[<TestClass>]
type FunctionTest() = 
    [<TestMethod>]
    member x.TestGenChordMessagesFromBassLine() = 
        let re = new RhythmEvent.RhythmEvent(1.0f, 1.0f, 1)
        Assert.IsNotNull(re)
        
        
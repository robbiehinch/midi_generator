﻿namespace PitchGenerator.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting
open System.Linq

[<TestClass>]
type SoloistTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let re = new PitchGenerator.Soloist(new Midi.Scale(new Midi.Note('C'), Midi.Scale.Major), Midi.Pitch.C3, Midi.Pitch.A1, Midi.Pitch.A5)
        Assert.IsNotNull(re)
        
[<TestClass>]
type PitchChangeTest() = 
    [<TestMethod>]
    member x.CMajor() = 
        let scale = new Midi.Scale(new Midi.Note('C'), Midi.Scale.Major)
        let startPitch = Midi.Pitch.C3
        let second = PitchGenerator.PitchAtInterval 2 scale startPitch 
        Assert.AreEqual(Midi.Pitch.E3, second)
        let fourth = PitchGenerator.PitchAtInterval 4 scale startPitch 
        Assert.AreEqual(Midi.Pitch.G3, fourth)

    [<TestMethod>]
    member x.PitchChangeStep() = 
        let scale = new Midi.Scale(new Midi.Note('C'), Midi.Scale.Major)
        Assert.AreEqual(Midi.Pitch.D3, PitchGenerator.PitchChange Midi.Pitch.C3 1 1 scale)
        Assert.AreEqual(Midi.Pitch.E3, PitchGenerator.PitchChange Midi.Pitch.C3 1 8 scale)
        Assert.AreEqual(Midi.Pitch.B2, PitchGenerator.PitchChange Midi.Pitch.C3 -1 1 scale)
        Assert.AreEqual(Midi.Pitch.A2, PitchGenerator.PitchChange Midi.Pitch.C3 -1 8 scale)
        

        
﻿module MidiToCSoundScore.Test


open System
open Microsoft.VisualStudio.TestTools.UnitTesting


[<TestClass>]
type FunctionTest() = 
    [<TestMethod>]
    member x.TestCSoundNoteFromMidiNote() = 
        let device = Foq.Mock<Midi.DeviceBase>().Create()
        let clock = Foq.Mock<Midi.Clock>().Create()
        let msg = new Midi.NoteOnOffMessage(device, Midi.Channel.Channel1, Midi.Pitch.C3, 100, 10.0f, clock, 3.0f)
        let csoundStr = CSoundNoteFromMidiNote msg 0.5 1.f
        Assert.AreEqual("i 1 10.00 3.00 100 48 0.50", csoundStr)
        
    [<TestMethod>]
    member x.TestGenCSD() = 
        let expected = "<CsoundSynthesizer>
<CsOptions>
test_options
</CsOptions>
<CsInstruments>
test_instruments
</CsInstruments>
<CsScore>
test_score
</CsScore>
</CsoundSynthesizer>"
        let actual = GenCSD "test_options" "test_instruments" "test_score"
        Assert.AreEqual(expected, actual)
        



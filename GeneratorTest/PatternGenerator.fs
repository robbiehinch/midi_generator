﻿namespace PatternGenerator.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting

[<TestClass>]
type BinarySeqToPositionsTest() = 
    [<TestMethod>]
    member x.TestSnare () = 
        let testVal = [| 0; 0; 1; 0 |]
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toArray
        CollectionAssert.AreEquivalent([| 2 |], ret)
        
    [<TestMethod>]
    member x.Test10 () = 
        let testVal = seq { for i in 0..10 do yield i % 3 }
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toArray
        CollectionAssert.AreEquivalent([| 1; 2; 4; 5; 7; 8; 10 |], ret)
        
    [<TestMethod>]
    member x.TestSimple () = 
        let testVal = { 0..1 }
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toList
        Assert.AreEqual([ 1 ], ret)
        
    [<TestMethod>]
    member x.TestEmpty () = 
        let testVal = Seq.empty
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toList
        Assert.AreEqual(List.empty<int>, ret)
        

    [<TestMethod>]
    member x.Test1 () = 
        let testVal = { 1..1 }
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toList
        Assert.AreEqual( [ 0 ] , ret)


    [<TestMethod>]
    member x.Test0 () = 
        let testVal = { 0..0 }
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toList
        Assert.AreEqual( List.empty<int> , ret)


    [<TestMethod>]
    member x.Test0101 () = 
        let testVal = [| 0; 1; 0; 1 |]
        let ret = PatternGenerator.BinarySeqToPositions testVal
                    |> Seq.toArray
        CollectionAssert.AreEquivalent( [|1; 3|] , ret)

        
[<TestClass>]
type EventSequenceToEventNotesTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let testVal = [| 0.0f; 3.0f; 7.0f; 20.0f |]
        let ret = testVal |> PatternGenerator.EventSequenceToEventNotes 4.0f
                            |> Seq.toArray
        CollectionAssert.AreEquivalent([| (0.0f, 3.0f); (3.0f, 4.0f); (7.0f, 4.0f); (20.0f, 4.0f) |], ret)
        
    [<TestMethod>]
    member x.Test13 () = 
        let testVal = [| 1.0f; 3.0f |]
        let ret = testVal |> PatternGenerator.EventSequenceToEventNotes 4.0f
                            |> Seq.toArray
        CollectionAssert.AreEquivalent([| (1.0f, 2.0f); (3.0f, 4.0f) |], ret)
        

[<TestClass>]
type GenNoteLengthsTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let testVal = { 0..1 }
        let ret = testVal |> PatternGenerator.GenNoteLengths
                            |> Seq.toArray
        CollectionAssert.AreEquivalent([| (1.0f, 4.0f) |], ret)
        
    [<TestMethod>]
    member x.Test13 () = 
        let testVal = [|0; 1; 0 ;1|]
        let ret = testVal |> PatternGenerator.GenNoteLengths
                            |> Seq.toArray
        CollectionAssert.AreEquivalent([| (1.0f, 2.0f); (3.0f, 4.0f) |], ret)
        
        
﻿namespace Pan.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting


[<TestClass>]
type PanTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let notes = [| 1; 2|]
        let panFn = (fun x -> 0.5)
        let re = Pan.PanNotes notes panFn
        Assert.IsNotNull(re)
        
        
[<TestClass>]
type AlternatePanTest() = 

    member x.Scope() =
        let gen = Pan.AlternatePanCreate
        let vals = Seq.init 4 (fun i -> gen i) 
                    |> Seq.toArray
        vals

    [<TestMethod>]
    member x.TestSimple () = 
        let vals = x.Scope()
        CollectionAssert.AreEqual([|0.; 1.; 0.; 1.|], vals)
        
        
[<TestClass>]
type PanWindowTest() = 

    [<TestMethod>]
    member x.TestSimple () = 
        let gen = Pan.PanWindowCreate 0.5 0.2 0.1
        let vals = Seq.init 4 (fun i -> gen i) 
                    |> Seq.toArray
        CollectionAssert.AreEqual([|0.6; 0.6; 0.5; 0.4|], vals)
        
        
[<TestClass>]
type SinePanTest() = 

    [<TestMethod>]
    member x.TestSimple () = 
        let gen = Pan.SinePanCreate 440. 44100. 1. 0.
        let vals = Seq.init 400 (fun i -> gen i) 
                    |> Seq.toArray
        Assert.IsTrue((vals |> Seq.max) < 1.)
        Assert.IsTrue((vals |> Seq.min) > -1.)
        
        
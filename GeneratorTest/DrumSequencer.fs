﻿namespace DrumSequencer.Test

open System
open Microsoft.VisualStudio.TestTools.UnitTesting


[<TestClass>]
type DrumSequencerTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let re = DrumSequencer.PatternsToPattern [| [| 0 |] |]
                    |> Seq.toArray
        CollectionAssert.AreEquivalent([| 0 |], re)
        
    [<TestMethod>]
    member x.TestSimple2by2 () = 
        let re = DrumSequencer.PatternsToPattern [| [| 0; 1 |]; [| 1; 0 |] |]
                    |> Seq.toArray
        CollectionAssert.AreEquivalent([| 0; 1; 1; 0 |], re)
        
    [<TestMethod>]
    member x.TestSimple1by4 () = 
        let re = DrumSequencer.PatternsToPattern [| [| 0; 1; 0; 1 |] |]
                    |> Seq.toArray
        CollectionAssert.AreEquivalent([| 0; 1; 0; 1 |], re)
        

[<TestClass>]
type PatternsToNoteLengthsTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let re = DrumSequencer.PatternsToNoteLengths [| [| 0; 1; 0; 1 |] |]
                    |> Seq.toArray
        Assert.AreEqual(1.0f, fst re.[0])
        Assert.AreEqual(2.0f, snd re.[0])
        Assert.AreEqual(3.0f, fst re.[1])
        Assert.AreEqual(4.0f, snd re.[1])
        
[<TestClass>]
type CreateRhythmTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let re = DrumSequencer.CreateRhythm [| [| 0; 1; 0; 1 |] |] [| 1 |] 4 4
                    |> Seq.toArray
        Assert.AreEqual(1.0f, re.[0].StartTime)
        Assert.AreEqual(2.0f, re.[0].Duration)
        Assert.AreEqual(3.0f, re.[1].StartTime)
        Assert.AreEqual(4.0f, re.[1].Duration)


[<TestClass>]
type LoopRhythmTest() = 
    [<TestMethod>]
    member x.TestSimple () = 
        let re = DrumSequencer.LoopRhythm 2 4 [| new RhythmEvent.RhythmEvent(1.0f, 2.0f, 127); new RhythmEvent.RhythmEvent(3.0f, 2.0f, 127) |]
                    |> Seq.toArray
        let expected = [|
                            new RhythmEvent.RhythmEvent(1.0f, 2.0f, 127) :> RhythmEvent.IRhythmEvent;
                            new RhythmEvent.RhythmEvent(3.0f, 2.0f, 127) :> RhythmEvent.IRhythmEvent;
                            new RhythmEvent.RhythmEvent(5.0f, 2.0f, 127) :> RhythmEvent.IRhythmEvent;
                            new RhythmEvent.RhythmEvent(7.0f, 2.0f, 127) :> RhythmEvent.IRhythmEvent |]
        Assert.AreEqual(expected.[3].StartTime, re.[3].StartTime )


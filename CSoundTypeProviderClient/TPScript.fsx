﻿

#r @"../CSoundDSL/bin/Debug/CSoundDSL.dll"
    

open Microsoft.FSharp.Quotations

type CSound = CSoundDSL.CSoundWrapper

2 |> CSound.a

[<ReflectedDefinition>]
let myfn i j =
    let k = i * j
    let x = k + 2
    let f = CSound.a x
    let ret = f + 2
    ret


let println expr =
    let rec print expr = match expr with
//        | DerivedPatterns.Applications(expr1, expr2) ->
        | Patterns.Application(expr1, expr2) ->
            // Function application.
            print expr1
            printf " "
            print expr2
        | Patterns.Call(None, DerivedPatterns.MethodWithReflectedDefinition(n), expList) ->
//            printf "MethodWithReflectedDefinition"
            print n
        | Patterns.Call(exprOpt, methodInfo, exprList) ->
            // Method or module function call.
            match exprOpt with
            | Some expr -> print expr
            | None -> printf "%s" methodInfo.DeclaringType.Name
            printf ".%s(" methodInfo.Name
            if (exprList.IsEmpty) then printf ")" else
            print exprList.Head
            for expr in exprList.Tail do
                printf ","
                print expr
            printf ")"
        | DerivedPatterns.Int32(n) ->
            printf "%d" n
        | Patterns.Lambda(param, body) ->
            // Lambda expression.
            printf "fun (%s:%s) -> " param.Name (param.Type.ToString())
            print body
        | Patterns.Let(var, expr1, expr2) ->
            // Let binding.
            if (var.IsMutable) then
                printf "let mutable %s = " var.Name
            else
                printf "let %s = " var.Name
            print expr1
            printf " in "
            print expr2
        | Patterns.PropertyGet(_, propOrValInfo, _) ->
            printf "%s" propOrValInfo.Name
        | DerivedPatterns.String(str) ->
            printf "%s" str
        | Patterns.Value(value, typ) ->
            printf "%s" (value.ToString())
        | Patterns.Var(var) ->
            printf "%s" var.Name
        | _ -> printf "%s" (expr.ToString())
    print expr


<@ myfn @> |> println

//let tdd = 
//    match quotref with 
//        | AnyTopDefnUse(td) -> td 
//        | _ -> failwith "Failed to match with top defn"
//let expr =
//    match (ResolveTopDefinition tdd) with
//        | Some e -> e
//        | None -> failwith "Failed to get quotation!"
//print "" expr


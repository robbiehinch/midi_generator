﻿
#r @"F:\src\music\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"

open System.Linq

let c = new csound6netlib.Csound6Net()
let a = c.GetOpcodeListAsync()
a.Wait() |> ignore
let res = a.Result
let ra = res.["a"]
let oc = ra.[0]
oc.intypes
oc.outypes


let routs = res.["outs"]
routs.Count
routs.[0].intypes
routs.[0].outypes

let rabs = res.["abs"]
rabs.Count
rabs.[0].intypes
rabs.[0].outypes
rabs.[1].intypes
rabs.[1].outypes
rabs.[2].intypes
rabs.[2].outypes

let counts = res |> Seq.map (fun kvp -> kvp.Key, kvp.Value.Count)
                |> Seq.groupBy (fun (k, c) -> c)
                |> Seq.toArray

let paramStrs = res |> Seq.map (fun kvp -> (kvp.Value.Select( (fun oc -> [| oc.intypes; oc.outypes |]))))
                    |> Seq.collect (fun x ->)
                    |> Seq.distinct
                    |> Seq.toArray


res.["pan"].[0].intypes
res.["pan"].[0].outypes
res.["out"].[0].outypes


res.["xin"].[0].intypes
res.["xin"].[0].outypes


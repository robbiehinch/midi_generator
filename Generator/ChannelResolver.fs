﻿module ChannelResolver

let labelPairs (pairs:seq<'A * 'B>) =
    let aas = pairs |> Seq.map (fun (a, b) -> a)
                    |> Seq.sort
                    |> Seq.distinct
                    |> Seq.toArray

    let labels = { 0 .. aas.Length } |> Seq.map (fun l -> l.ToString())
    Seq.zip aas labels |> Map.ofSeq



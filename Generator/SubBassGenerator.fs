﻿module SubBassGenerator

let pitchVals = Midi.Pitch.GetValues(Midi.Pitch.A0.GetType()) |> Seq.cast<Midi.Pitch>
                                                                |> Seq.toArray

let CreateChordForNote clock channel (scale:Midi.Scale) (note:Midi.NoteOnOffMessage) =
    let rootNote = note.Pitch
    let newPitch = 48 + ((int)rootNote % 12)
    new Midi.NoteOnOffMessage(note.Device, channel, pitchVals.[newPitch], note.Velocity, note.Time, clock, note.Duration)


let GenSubBassFromBassLine clock channel scale bass =
    bass |> Seq.map (fun note -> CreateChordForNote clock channel scale note)




﻿module Pan

let PanNotes notes panFn =
    notes |> Seq.map (fun note -> new Note.PannedNote<'a>(note, (panFn note)))


let GetNext<'T> (enumerator:System.Collections.Generic.IEnumerator<'T>) =
    let n = enumerator.MoveNext()
    enumerator.Current


let AlternatePanCreate<'T> =
    let s = 0.0 |> Seq.unfold (fun i -> Some(i, if 0.0 = i then 1.0 else 0.0))
    let enumerator = s.GetEnumerator()
    let fn = (fun (note:'T) -> GetNext enumerator )
    fn


let PanWindowCreate<'T> centre width rate =
    assert (rate < 0.5)
    let _ub = centre + (width / 2.0)
    let _lb = centre - (width / 2.0)
    assert (_ub <= 1.)
    assert (_lb >= 0.)
    let mutable _rate = rate
    let mutable _current = centre
    fun (_:'T) -> 
       _current <- _current + _rate
       if _current > _ub || _current < _lb then
            _rate <- -_rate
            _current <- _current + _rate
       _current


let SinePanCreate<'T> frequency sampleRate scale offset =
    let requiredSamples = Seq.initInfinite (fun i -> (float)i)
    let s = requiredSamples |> Seq.map (fun x -> (sin (x * 2. * System.Math.PI * frequency / sampleRate) * scale) + offset) 
    let enumerator = s.GetEnumerator()
    let fn = (fun (note:'T) -> GetNext enumerator )
    fn
                
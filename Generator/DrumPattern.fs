﻿module DrumPattern

open PitchGenerator
open RhythmEvent

let GeneratePattern repeats startTime phraseLength (rootPattern:IRhythmEvent[]) =
    let phraseTimes = rootPattern |> Array.map (fun iRhythmEvent -> iRhythmEvent.StartTime, iRhythmEvent.StartTime + iRhythmEvent.Duration)
    let phraseStart = phraseTimes |> Array.minBy fst |> fst
    let phraseEnd = phraseTimes |> Array.maxBy snd |> snd
    let phraseLength = Operators.max (phraseEnd - phraseStart) phraseLength
    seq { 0..repeats } |> Seq.map (fun repeatNum -> rootPattern |> Array.map (fun iRhythmEvent -> iRhythmEvent.Copy(startTime + iRhythmEvent.StartTime + ((float32)repeatNum*phraseLength), iRhythmEvent.Duration, iRhythmEvent.Velocity)))
                        |> Seq.concat
                        |> Seq.toArray


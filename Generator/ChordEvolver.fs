﻿module ChordEvolver

open PitchGenerator

let CopyNoteWithNewPitchAndChannel clock channel newPitch (note:Midi.NoteOnOffMessage) = 
    new Midi.NoteOnOffMessage(note.Device, channel, newPitch, note.Velocity, note.Time, clock, note.Duration)

let CreateChordForNote clock channel (scale:Midi.Scale) (note:Midi.NoteOnOffMessage) =
    let rnd = new System.Random()
    let rootNote = note.Pitch
    let secondInterval = PitchAtInterval 2 scale rootNote
    let thirdInterval = PitchAtInterval 4 scale rootNote
    [|
        CopyNoteWithNewPitchAndChannel clock channel secondInterval note
        CopyNoteWithNewPitchAndChannel clock channel thirdInterval note
    |]


let GenChordMessagesFromBassLine clock channel scale bass =
    bass |> Seq.map (fun note -> CreateChordForNote clock channel scale note)
            |> Seq.collect (fun m -> m)


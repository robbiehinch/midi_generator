﻿

#r @"F:\src\music\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"
#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#r @"..\CSoundDSL\bin\Debug\CSoundDSL.dll"

open Midi
open System.Linq
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

#load "SfzParser.fs"
#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "Pan.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"
#load "MidiToCSoundScore.fs"
#load "ElectronicDrumMachine.fs"

let midiDevice = OutputDevice.InstalledDevices.FirstOrDefault()
let drumClock = Clock(240.0f)


let aroundC3PitchGens =  [|
    new PitchGenerator.NotLast([| Pitch.C3; Pitch.CSharp3; Pitch.B3 |]) :> PitchGenerator.IPitchGenerator
    |]

let kickHits = [|
                [| 1; 0; 0; 1 |];
                |]
let hiHatHits = [|
                [| 1; 1; 1; 1 |];
                |]

let snareHits = [|
                [| 0; 1; 0; 0 |]
                [| 0; 0; 1; 0 |]
                |]


let crashHits = [|
//                [| 0; 1; 0; 0 |]
                [| 0; 0; 1; 0 |]
                |]

//let kickPitchGens =  [|
//    new PitchGenerator.RoundRobin([| Pitch.A0; Pitch.A1; Pitch.A2; Pitch.A3; Pitch.A4; Pitch.A5; Pitch.A6; Pitch.A7; Pitch.A8 |]) :> PitchGenerator.IPitchGenerator
//    |]

let noteGen pitchGen hits channel = 
    pitchGen |> DrumSequencer.GenMidiNotesOnChannels hits [| 0 |] 4 4 16 midiDevice 0.0f drumClock channel
                                    |> Seq.collect ( fun s -> s)
                                    |> Seq.toArray

let kickNotes = noteGen aroundC3PitchGens kickHits 0
let hiHatNotes = noteGen aroundC3PitchGens hiHatHits 8
let snareNotes = noteGen aroundC3PitchGens snareHits 6
//let crashNotes = noteGen aroundC3PitchGens crashHits 23

let allNotes =  [|
//                    kickNotes
                    hiHatNotes
//                    snareNotes
//                    crashNotes
                    |] 

let usedChannels = allNotes |> Array.collect (fun a -> a)
                            |> Array.map (fun n -> n.Channel)
                            |> Seq.distinct
                            |> Seq.map (fun c -> sprintf "instr %d" (System.Convert.ToInt32(c) + 1))
                            |> Seq.toArray

//let panner = Pan.AlternatePanCreate
//let pannedCrashNotes = Pan.PanNotes crashNotes panner
//                    |> Seq.toArray
//let crashScore = MidiToCSoundScore.Convert (MidiToCSoundScore.PannedMidiNotes pannedCrashNotes) 0.5 0.4f

let tempoScale = 0.1f
let kickScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes kickNotes) 0.5 tempoScale
let hiHatScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes hiHatNotes) 0.4 tempoScale
let snareScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes snareNotes) 0.6 tempoScale

let allScores = [|
                    kickScore
                    hiHatScore
                    snareScore
//                    crashScore
                    |]

let drumScoreLines = allScores |> Seq.collect (fun a -> a) |> String.concat "\n"

let score = sprintf "
            %s
            " drumScoreLines


//let drumFolder = "H:\Media\VSTs\Drums\SalamanderDrumkit\salamanderDrumkit"
let drumFolder = "F:\Media\VSTs\Drums\SalamanderDrumkit\salamanderDrumkit"
let drumSfz = "ALL.sfz"
let drumCSoundInsts = SfzParser.SfzToCSoundFtgen drumFolder drumSfz
                            |> Seq.toArray

SfzParser.channelLabels drumFolder drumSfz |> Array.mapi (fun i x -> i, x)

let drumCSoundInstsRefed = drumCSoundInsts
                            |> Seq.filter (fun (t, i) -> usedChannels |> Array.exists (fun uc -> (i.Split([|System.Environment.NewLine|], System.StringSplitOptions.None) |> Seq.exists (fun (il:string) -> il.EndsWith(uc)) )))
                            |> Seq.toArray

let electronicKick = (ElectronicDrumMachine.KickDrum().qfn 1).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicSnare = (ElectronicDrumMachine.SnareDrum().qfn 6).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicHiHat = (ElectronicDrumMachine.ClosedHiHat().qfn 8).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)

let wavs = drumCSoundInstsRefed |> Array.map (fun (w, i) -> w)
//let insts = drumCSoundInstsRefed |> Array.map (fun (w, i) -> i)
//                                    |> Array.append electronicKick
//                                    |> Array.append electronicSnare
let insts = [| electronicKick; electronicSnare; electronicHiHat |] |> Array.collect (fun x -> x)

System.Environment.CurrentDirectory

let orchestra = sprintf "
                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1


opcode IndexOf, i, i[]ii

iAin[], iValSearch, iCurrIndex xin             ; read input parameters
iOut = 0

	iFoundVal = iAin[iCurrIndex]
	if (iFoundVal > iValSearch) then
		iOut = iCurrIndex
	else
		iOut IndexOf iAin, iValSearch, (iCurrIndex+1)
	endif


xout iOut
endop

giSine		ftgen		0,0,4096,10,1	;SINE WAVE

                %s

                %s
                instr 99
                endin" (wavs |> String.concat "\n" ) (insts |> String.concat "\n" )

let option = "-odac"

let ofilePath = System.IO.Path.Combine(__SOURCE_DIRECTORY__, __SOURCE_FILE__ + ".csd")
System.IO.File.WriteAllText(ofilePath, (MidiToCSoundScore.GenCSD option orchestra score))
printfn "Wrote to: %s"ofilePath


let ExecuteInCSound option orchestra score p ct =
    use c = new csound6netlib.Csound6Net()
    c.SetOption(option) |> ignore
    c.CompileOrc(orchestra) |> ignore
    c.ReadScore(score) |> ignore
    c.Start() |> ignore
//    c.PerformAsync(p, ct)
    c.Perform() |> ignore
    c.Stop()
//
//printfn "Play?"
//let run = System.Console.In.ReadLine()
//
//if run.ToLower().StartsWith("y") then (ExecuteInCSound option orchestra score) 

let p = new System.Progress<float32>()
let ct = new System.Threading.CancellationToken()
let t = ExecuteInCSound option orchestra score p ct
//t.Wait()


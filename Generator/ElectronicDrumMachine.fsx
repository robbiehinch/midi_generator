﻿

#r @"..\..\..\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"
#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#r @"..\CSoundDSL\bin\Debug\CSoundDSL.dll"

open Midi
open System.Linq
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

#load "CompilerExtensions.fs"
#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "Pan.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"
#load "MidiToCSoundScore.fs"
#load "ElectronicDrumMachine.fs"
#load "Routing.fs"

let midiDevice = OutputDevice.InstalledDevices.FirstOrDefault()
let drumClock = Clock(240.0f)


let aroundC3PitchGens =  [|
    new PitchGenerator.NotLast([| Pitch.C3; Pitch.CSharp3; Pitch.B3 |]) :> PitchGenerator.IPitchGenerator
    |]

let kickHits = [|
                [| 1; 0; 0; 1 |];
                |]
let hiHatHits = [|
                [| 1; 1; 1; 1 |];
                |]

let snareHits = [|
                [| 0; 1; 0; 0 |]
                [| 0; 0; 1; 0 |]
                |]


let crashHits = [|
//                [| 0; 1; 0; 0 |]
                [| 0; 0; 1; 0 |]
                |]

//let kickPitchGens =  [|
//    new PitchGenerator.RoundRobin([| Pitch.A0; Pitch.A1; Pitch.A2; Pitch.A3; Pitch.A4; Pitch.A5; Pitch.A6; Pitch.A7; Pitch.A8 |]) :> PitchGenerator.IPitchGenerator
//    |]

let noteGen pitchGen hits channel = 
    pitchGen |> DrumSequencer.GenMidiNotesOnChannels hits [| 0 |] 4 4 16 midiDevice 0.0f drumClock channel
                                    |> Seq.collect ( fun s -> s)
                                    |> Seq.toArray

let kickNotes = noteGen aroundC3PitchGens kickHits 0
let hiHatNotes = noteGen aroundC3PitchGens hiHatHits 8
let snareNotes = noteGen aroundC3PitchGens snareHits 6
//let crashNotes = noteGen aroundC3PitchGens crashHits 23

let allNotes =  [|
                    kickNotes
                    hiHatNotes
                    snareNotes
//                    crashNotes
                    |] 


let tempoScale = 0.5f
let kickScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes kickNotes) 0.5 tempoScale
let hiHatScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes hiHatNotes) 0.4 tempoScale
let snareScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes snareNotes) 0.6 tempoScale

let allScores = [|
                    kickScore
                    hiHatScore
                    snareScore
//                    crashScore
                    |]

let drumScoreLines = allScores |> Seq.collect (fun a -> a) |> String.concat "\n"

let score = sprintf "
            %s
            " drumScoreLines



let drumMachine = ElectronicDrumMachine.DrumInstruments ()
let genFn = CompilerExtensions.genInstByMemberName drumMachine
let globals = (genFn "GlobalVars" 0 CompilerExtensions.outsHook).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicKick = (genFn "Kick" 1 CompilerExtensions.outsHook).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicSnare = (genFn "Snare" 6 CompilerExtensions.outsHook).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicHiHat = (genFn "ClosedHiHat" 8 CompilerExtensions.outsHook).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)

let insts = [| electronicKick; electronicSnare; electronicHiHat |] |> Array.collect (fun x -> x)

System.Environment.CurrentDirectory

let orchestra = sprintf "
                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1

                %s

                %s
                instr 99
                endin" (globals |> String.concat "\n" ) (insts |> String.concat "\n" )

let option = "-odac"

let ofilePath = System.IO.Path.Combine(__SOURCE_DIRECTORY__, __SOURCE_FILE__ + ".csd")
System.IO.File.WriteAllText(ofilePath, (MidiToCSoundScore.GenCSD option orchestra score))
printfn "Wrote to: %s"ofilePath


let ExecuteInCSound option orchestra score p ct =
    use c = new csound6netlib.Csound6Net()
    c.SetOption(option) |> ignore
    c.CompileOrc(orchestra) |> ignore
    c.ReadScore(score) |> ignore
    c.Start() |> ignore
//    c.PerformAsync(p, ct)
    c.Perform() |> ignore
    c.Stop()
//
//printfn "Play?"
//let run = System.Console.In.ReadLine()
//
//if run.ToLower().StartsWith("y") then (ExecuteInCSound option orchestra score) 

let p = new System.Progress<float32>()
let ct = new System.Threading.CancellationToken()
let t = ExecuteInCSound option orchestra score p ct
//t.Wait()


﻿
#load "ChannelResolver.fs"


let nodes = [| "a"; "b"; "c"; "d" |]
let myPairs = [| 
                (nodes.[0], nodes.[1])
                (nodes.[0], nodes.[2])
                (nodes.[0], nodes.[3])
                (nodes.[1], nodes.[2])
                (nodes.[1], nodes.[3])
                (nodes.[2], nodes.[3])
                |]

let labels = ChannelResolver.labelPairs myPairs

printfn "%A" labels


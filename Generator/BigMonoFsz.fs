﻿module BigMonoFsz

open Midi

type BigMonoFsz() = class
        member x.Kick = Pitch.C2
        member x.SnareLH = Pitch.DSharp2
        member x.SnareRH = Pitch.E2
        member x.SnareHrdLH = Pitch.F2
        member x.SnareHrdRH = Pitch.FSharp2
        member x.SnareRim = Pitch.G2
        member x.Tom1LH = Pitch.A2
        member x.Tom1RH = Pitch.ASharp2
        member x.Tom2LH = Pitch.B2
        member x.Tom2RH = Pitch.C3
        member x.HiHat1 = Pitch.F3
        member x.HiHat2 = Pitch.FSharp3
        member x.HiHat3 = Pitch.G3
        member x.HiHatOpenClose = Pitch.GSharp3
        member x.HiHatOpen = Pitch.A3
        member x.HiHatClose = Pitch.ASharp3
        member x.Crash1 = Pitch.D4
        member x.Crash2 = Pitch.DSharp4
        member x.Ride = Pitch.A4
        member x.RideBell = Pitch.ASharp4


end

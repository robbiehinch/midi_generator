﻿
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"

open Midi
open System.Linq

let loopBe1Out = OutputDevice.InstalledDevices.FirstOrDefault(fun d -> d.Name.Contains("LoopBe"))

loopBe1Out.Open()

//drums
//drumClock.Stop();
//drumClock.Reset();
//loopBe1Out.SilenceAllNotes()

#load "BigMonoFsz.fs"
#load "MTPDK2.fs"
#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"


let bigMono = new BigMonoFsz.BigMonoFsz()
let mtpdk2 = new MTPDK2.MTPDK2()
let drumClock = Clock(240.0f)

let snareHits = [|
                [| 0; 0; 1; 0 |];
                |]
let snarePitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.SnareLH; bigMono.SnareRH |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.Snare1; mtpdk2.Snare2; mtpdk2.Snare3 |]) :> PitchGenerator.IPitchGenerator
    |]

snarePitchGens |> DrumSequencer.SequenceNotesOnChannels snareHits [| 0 |] 4 4 16 loopBe1Out 0.0f drumClock 0

let kickHits = [|
                [| 1; 0; 0; 0 |];
                [| 1; 1; 0; 0 |]
                |]
let kickPitchGens =  [|
    new PitchGenerator.Unit(bigMono.Kick) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.Kick; mtpdk2.Kick2; mtpdk2.Kick3 |]) :> PitchGenerator.IPitchGenerator
    |]

kickPitchGens |> DrumSequencer.SequenceNotesOnChannels kickHits [| 0 |] 4 8 8 loopBe1Out 0.0f drumClock 0

let hiHatPitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.HiHat1; bigMono.HiHat2; bigMono.HiHat3 |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.HiHatClosed; mtpdk2.HiHatHalfClosed; mtpdk2.HiHatPedal |]) :> PitchGenerator.IPitchGenerator
    |]

hiHatPitchGens |> DrumSequencer.SequenceNotesOnChannels [| [| 1; 1; 1; 1 |] |] [| 0 |] 4 4 16 loopBe1Out 0.0f drumClock 0


let crashHits = [|
                [| 0; 0; 0; 0 |];
                [| 0; 1; 0; 0 |];
                [| 1; 0; 0; 1 |]
                [| 1; 1; 0; 0 |]
                |]
let crashPitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.Crash1; bigMono.Crash2; bigMono.HiHatOpen |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.China; mtpdk2.CrashLeft; mtpdk2.CrashRight; mtpdk2.Splash |]) :> PitchGenerator.IPitchGenerator
    |]
crashPitchGens |> DrumSequencer.SequenceNotesOnChannels crashHits [| 0; 2 |] 4 16 4 loopBe1Out 0.0f drumClock 0


drumClock.Start();



//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 8192, 0.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 5000, 1.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 4000, 2.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 3000, 3.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 2000, 4.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 1000, 5.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 12000, 6.0f))
//drumClock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 8192, 10.0f))


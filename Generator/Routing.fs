﻿module Routing

open Microsoft.FSharp.Quotations
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic


let channRecvHook channName expr =
    match expr with
        | Patterns.Value (o, t) ->
            Some (sprintf "\"%s\"" channName, { Compiler.CompileState.IsMultiParamFn = false })
        | _ -> None


let decomposeSignalArrayElems (channNames:seq<string>) (e:Quotations.Expr<Vector<double>[]>) =
    match e with
        | Patterns.NewArray(_, el) -> el |> List.map (fun e -> Quotations.Expr.Cast<Vector<double>>(e))
                                            |> List.map (fun v -> <@ CSoundCGVector.Functions.chnset (%v, "ChannPlaceholder") @>)
                                            |> List.map (fun e -> Compiler.compileInstrument CompilerExtensions.nonHook e)
                                            |> Seq.zip channNames
                                            |> Seq.map (fun (chanName, compiledCode) -> compiledCode.Replace("ChannPlaceholder", chanName))
                                            |> Seq.toList
        | _ -> []


let decomposeSignalArray (exprList: Expr list) channName =
    match exprList.Head with
        | Patterns.NewArray(_, el) -> el |> List.map (fun e -> Quotations.Expr.Cast<Vector<double>[]>(e))
                                            |> List.map (decomposeSignalArrayElems channName)
        | _ -> []


let channSendHook (channName:string) expr =
    match expr with
        | DerivedPatterns.SpecificCall <@ CSoundCGVector.Functions.outs  @> (_, _, exprList) ->
            let outVars = decomposeSignalArray exprList ([| channName + "L"; channName + "R" |]) |> List.collect (fun x -> x)
            Some (outVars |> String.concat System.Environment.NewLine, { Compiler.CompileState.IsMultiParamFn = false })
//        | DerivedPatterns.SpecificCall <@ CompilerExtensions.OutputHook @> (_, _, exprList) ->
//            let outVars = decomposeSignalArray exprList ([| channName + "L"; channName + "R" |]) |> List.collect (fun x -> x)
//            Some (outVars |> String.concat System.Environment.NewLine, { Compiler.CompileState.IsMultiParamFn = false })
        | _ -> None



type StereoRoutedInstrument (channelNameL, channelNameR) =
    let _channelNameL = channelNameL
    let _channelNameR = channelNameR

    [<ReflectedDefinition>]
    member x.GlobalVars () =
//        giSine <- CSoundCGVector.Functions.ftgen(0m, 0m, 4096m, 10m, 1m, vector [])//	;SINE WAVE
        ()


    [<ReflectedDefinition>]
    member x.Recv p0 p1 p2 p3 p4 p5 p6 =
        let aSigL = CSoundCGVector.Functions.chnget<Vector<double>> "channPlaceholderL"
        let aSigR = CSoundCGVector.Functions.chnget<Vector<double>> "channPlaceholderR"
        CSoundCGVector.Functions.outs  [| [| aSigL; aSigR |] |]//; send audio to output
    
    member x.PostProcess (def:string) =
        def.Replace("channPlaceholderL", _channelNameL).Replace("channPlaceholderR", _channelNameR)


﻿module ElectronicDrumMachine

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic


type DrumInstruments () =
    let mutable giSine = 5m

    [<ReflectedDefinition>]
    member x.GlobalVars () =
        giSine <- CSoundCGVector.Functions.ftgen(0m, 0m, 4096m, 10m, 1m, vector [])//	;SINE WAVE
        ()


    [<ReflectedDefinition>]
    member x.Kick p0 p1 p2 p3 p4 p5 p6 =
//    	p3	=	0.2		;DEFINE DURATION FOR THIS SOUND
//	aenv	expon	1,p3,0.001	;AMPLITUDE ENVELOPE - PERCUSSIVE DECAY                    
//	kcps	expon	200,p3,20	;PITCH GLISSANDO
//	;OUTPUT	OPCODE	AMPLITUDE                               | FREQUENCY | FUNCTION_TABLE
//	asig	oscil	aenv*i(gk1Gain)*i(gkMasterGain)*1.6*p4,     kcps,       gisine 		;OSCILLATOR
//		outs	asig, asig	;SEND AUDIO TO OUTPUTS

        let iAmp = p4/127s   //amplitude
        let iPitch = CSoundCGVector.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
        let iPan = p6//";

        CSoundCGVector.Functions.printf("electronic kick -> iPitch: %f, iAmp: %f\\n", 1.f, iPitch, [|iAmp|])

        let iDrumDur = 0.2m  //p3
        let aenv = CSoundCGVector.Functions.expon(1m, iDrumDur, 0.001m)
        let kcps = CSoundCGVector.Functions.expon<single>(100m*(decimal)iPitch, iDrumDur, 30m)

        let aEnvAmped = aenv * (float32)iAmp
        let asig = CSoundCGVector.Functions.oscil(aEnvAmped, kcps, giSine)
        let aSigLPan, aSigRPan = CSoundCGVector.Functions.pan2(asig, iPan)
        CSoundCGVector.Functions.outs [| [| aSigLPan; aSigRPan |] |]//; send audio to output
//        CompilerExtensions.OutputHook [| [| aSigLPan; aSigRPan |] |]//; send audio to output


    [<ReflectedDefinition>]
    member x.Snare p0 p1 p2 p3 p4 p5 p6 =
//	    p3	=	0.3		;DEFINE DURATION FOR THIS SOUND
//	    aenv	expon	1,p3,0.001	;AMPLITUDE ENVELOPE - PERCUSSIVE DECAY
//	    anse	noise	1, 0 	;CREATE NOISE COMPONENT FOR SNARE DRUM SOUND
//	    kcps	expon	400,p3,20				;CREATE TONE COMPONENT FREQUENCY GLISSANDO FOR SNARE DRUM SOUND
//	    ajit	randomi	0.2,1.8,10000				;JITTER ON FREQUENCY FOR TONE COMPONENT
//	    atne	oscil	aenv*i(gk3Gain)*i(gkMasterGain),kcps*ajit,gisine	;CREATE TONE COMPONENT
//	    asig	sum	anse*0.5, atne*5	;MIX NOISE AND TONE SOUND COMPONENTS
//	    ares 	vcomb 	asig, 0.02, 0.0035, .1	;PASS SIGNAL THROUGH ACOMB FILTER TO CREATE STATIC HARMONIC RESONANCE
//		outs	ares*aenv*i(gk3Gain)*i(gkMasterGain)*p4, ares*aenv*i(gk3Gain)*i(gkMasterGain)*p4	;SEND AUDIO TO OUTPUTS, APPLY ENVELOPE AND ATTENTUATE USING GAIN CONTROLS 

        let iAmp = p4/127s   //amplitude
        let iPitch = CSoundCGVector.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
        let iPan = p6//";

        CSoundCGVector.Functions.printf("snare -> iPitch: %f, iAmp: %f\\n", 1.f, iPitch, [|iAmp|])

        let iDrumDur = 0.3m  //p3
        let aenv = CSoundCGVector.Functions.expon<Vector<float>>(1m, iDrumDur, 0.001m)
        let anse = CSoundCGVector.Functions.noise(CSoundOpcode.ScalarOrArray.Scalar 1.0, 0.0f)
        let kcps = CSoundCGVector.Functions.expon<Vector<float>>(200m*(decimal)iPitch, iDrumDur, 30m)
        let ajit = CSoundCGVector.Functions.randomi(0.2f, 1.8f, CSoundOpcode.ScalarOrArray.Scalar 10000.0)

        let aEnvAmped = aenv * (float)iAmp
        let aJittercps = kcps.PointwiseMultiply(ajit)
        let atne = CSoundCGVector.Functions.oscil(aEnvAmped, aJittercps, giSine)

        let asig = CSoundCGVector.Functions.sum([|[|atne * 0.5; anse * 0.5 |] |])
        let ares = CSoundCGVector.Functions.vcomb(asig, 0.02f, CSoundOpcode.ScalarOrArray.Scalar 0.0035, 0.1m)
        let aout = ares.PointwiseMultiply(aenv) * (float)iAmp
        let aSigLPan, aSigRPan = CSoundCGVector.Functions.pan2(aout, iPan)
        CompilerExtensions.OutputHook [| [| aSigLPan; aSigRPan |] |]//; send audio to output


    [<ReflectedDefinition>]
    member x.ClosedHiHat p0 p1 p2 p3 p4 p5 p6 =
//        	ktime	timeinstk
//	if ktime<=1 then
//	  turnoff2 7,0,0	;TURN OFF ALL INSTANCES OF instr 7 (OPEN HI-HAT)
//	endif
//	p3	=	0.1		;DEFINE DURATION FOR THIS SOUND
//	aenv	expon	1,p3,0.001	;AMPLITUDE ENVELOPE - PERCUSSIVE DECAY
//	asig	noise	aenv*i(gk4Gain)*i(gkMasterGain)*p4, 0	;CREATE SOUND FOR CLOSED HI-HAT
//	asig	buthp	asig, 7000					;HIGHPASS FILTER SOUND
//		outs	asig, asig	;SEND AUDIO TO OUTPUTS

        let iAmp = p4/127.0   //amplitude
        let iPitch = CSoundCGVector.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
        let iPan = p6//";

        CSoundCGVector.Functions.printf("closed hihat -> iPitch: %f, iAmp: %f\\n", 1.f, iPitch, [|iAmp|])

        let iDrumDur = 0.1m  //p3
        let aenv = CSoundCGVector.Functions.expon(1m, iDrumDur, 0.001m)
        let anse = CSoundCGVector.Functions.noise(CSoundOpcode.ScalarOrArray.Scalar iAmp, 0.0f)
        let asig = anse.PointwiseMultiply(aenv)
        let asighp = CSoundCGVector.Functions.buthp(asig, 7000.0f * iPitch)

        let aSigLPan, aSigRPan = CSoundCGVector.Functions.pan2(asighp, iPan)
        CompilerExtensions.OutputHook [| [| aSigLPan; aSigRPan |] |]//; send audio to output


    [<ReflectedDefinition>]
    member x.OpenHiHat p0 p1 p2 p3 p4 p5 p6 =
//	p3	=	1		;DEFINE DURATION FOR THIS SOUND
//	aenv	expon	1,p3,0.001	;AMPLITUDE ENVELOPE - PERCUSSIVE DECAY
//	asig	noise	aenv*i(gk5Gain)*i(gkMasterGain)*p4, 0	;CREATE SOUND FOR CLOSED HI-HAT
//	asig	buthp	asig, 7000					;HIGHPASS FILTER SOUND	
//		outs	asig, asig	;SEND AUDIO TO OUTPUTS

        let iAmp = p4/127.0   //amplitude
        let iPitch = CSoundCGVector.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
        let iPan = p6//";

        CSoundCGVector.Functions.printf("closed hihat -> iPitch: %f, iAmp: %f\\n", 1.f, iPitch, [|CSoundOpcode.ScalarOrArray.Scalar iAmp|])

        let iDrumDur = 1m  //p3
        let aenv = CSoundCGVector.Functions.expon(1m, iDrumDur, 0.001m)
        let anse = CSoundCGVector.Functions.noise(CSoundOpcode.ScalarOrArray.Scalar iAmp, 0.0f)
        let asig = CSoundCGVector.Functions.buthp(anse, 7000.0f * iPitch)

        let aSigLPan, aSigRPan = CSoundCGVector.Functions.pan2(asig, iPan)
        CompilerExtensions.OutputHook [| [| aSigLPan; aSigRPan |] |]//; send audio to output



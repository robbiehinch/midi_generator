﻿module MixerChannel


let genInst fn instNum =
    let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn)
    let retStr = e.Value |> Compiler.compile
    if 0 = instNum then
        retStr
    else
        let ret = sprintf @"instr %d
                            %s
                            endin" instNum retStr
        ret
        

let genInstByMemberName obj fnName instNum =
    let mis = obj.GetType().GetMembers()
    let fn = mis |> Array.find (fun m -> m.Name.Contains(fnName))
    genInst (fn :?> System.Reflection.MethodBase) instNum


type MixerChannel () =

    [<ReflectedDefinition>]
    member x.Recv () =
        let ain = CSoundCGVector.Functions.chnget "channelName"
        CSoundCGVector.Functions.chnclear ain
        CSoundCGVector.Functions.out [| [| ain |] |]

    [<ReflectedDefinition>]
    member x.Send aSig =
        CSoundCGVector.Functions.chnmix(aSig, "channelName")



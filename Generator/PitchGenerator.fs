﻿module PitchGenerator

open Midi

type IPitchGenerator = 
    abstract member Next: unit -> Midi.Pitch
    
type Unit(pitch:Midi.Pitch) = class
    let pitch = pitch
    interface IPitchGenerator with
        member x.Next() = pitch
end

type RoundRobin(pitches:Midi.Pitch[]) = class
    let mutable currentPitch = 0
    let pitchLength = pitches.Length
    let pitches = pitches
    member private x.increment() =
        let lastPitch = currentPitch
        currentPitch <- (currentPitch + 1) % pitchLength
        pitches.[lastPitch]

    interface IPitchGenerator with
        member x.Next() = x.increment()
end

type NotLast(pitches:Midi.Pitch[]) = class
    let mutable currentPitch = 0
    let choices = pitches.Length - 1
    let pitches = pitches
    let rnd = new System.Random()
    member private x.choose(newPitch) = 
        currentPitch <- newPitch
//        printfn "currentPitch -> %d" currentPitch
        currentPitch |> Array.get pitches

    interface IPitchGenerator with
        member x.Next() = match rnd.Next(choices) with
                            | p when p >= currentPitch -> x.choose(p + 1)
                            | p -> x.choose(p)
end

let (|Equals|_|) arg x = 
  if (arg = x) then Some() else None


let PitchAtInterval interval (scale:Midi.Scale) (pitch:Midi.Pitch) =
    let currentDegree = scale.ScaleDegree(pitch) - 1
    let currentOctave = pitch.Octave()
    let scaleEnd = scale.NoteSequence.Length - 1
    let totalMove = currentDegree + interval
    let noteInSequence = totalMove % scaleEnd
    let octave = (totalMove + currentOctave * scaleEnd) / scaleEnd
    scale.NoteSequence.[noteInSequence].PitchInOctave(octave)

let gRnd = new System.Random()
let PitchChange currentPitch change changeMagnitude (scale:Midi.Scale) =
    try
        let currentDegree = scale.ScaleDegree(currentPitch) - 1
        let changeAmount = match changeMagnitude with
                            | x when x = 10 -> 3
                            | x when x > 7 -> 2
                            | _ -> 1
        let currentOctave = currentPitch.Octave()
        let degreeChange = change * changeAmount
        let scaleEnd = scale.NoteSequence.Length - 1
        let ret = match degreeChange with
                        | 0 -> currentPitch
                        | x when x < 0 -> scale.NoteSequence.[scale.NoteSequence.Length + x].PitchInOctave(currentOctave-1)
                        | x when x > 12 -> scale.NoteSequence.[x%12].PitchInOctave(currentOctave+1)
                        | x -> scale.NoteSequence.[currentDegree + x].PitchInOctave(currentOctave)
        printfn "current degree: %d, current octave: %d, change: %d, degreeChange: %d, ret: %s" currentDegree currentOctave change degreeChange (ret.ToString())
        ret
    with
        | :? System.IndexOutOfRangeException -> currentPitch

type Soloist(scale:Midi.Scale, startPitch:Midi.Pitch, bottomNote:Midi.Pitch, topNote:Midi.Pitch) = class
    do if bottomNote >= topNote then failwithf "bottom note %s must be lower than top note %s" (bottomNote.ToString()) (topNote.ToString())
    let mutable currentPitch = startPitch
    let scale = scale
    let rnd = new System.Random()

    member x.CurrentPitch with public get() = currentPitch

    member private x.choose(newPitch) = 
//        printfn "current pitch: %s, new pitch: %s" (currentPitch.ToString()) (newPitch.ToString())
        let ret = currentPitch
        currentPitch <- newPitch
        ret

    interface IPitchGenerator with
        member x.Next() =
            let newPitch = PitchChange currentPitch (rnd.Next(-1, 2)) (rnd.Next(10)) scale
            x.choose(if newPitch <= bottomNote then bottomNote else if newPitch >= topNote then topNote else newPitch)
end

type WalkUpAndDown(scale:Midi.Scale, startPitch:Midi.Pitch, bottomNote:Midi.Pitch, topNote:Midi.Pitch) = class
    let mutable currentPitch = startPitch
    let mutable movingUp = true
    let scale = scale

    member private x.choose(newPitch) = 
//        printfn "current pitch: %s, new pitch: %s" (currentPitch.ToString()) (newPitch.ToString())
        let ret = currentPitch
        currentPitch <- newPitch
        ret

    interface IPitchGenerator with
        member x.Next() =
            if currentPitch >= topNote then movingUp <- false else if currentPitch <= bottomNote then movingUp <- true
            let change = if movingUp then 1 else -1
            let newPitch = PitchChange currentPitch change 1 scale
            x.choose(if newPitch <= bottomNote then bottomNote else if newPitch >= topNote then topNote else newPitch)
end

type Chords(scale:Midi.Scale, bass:Soloist) = class
//    let scale = scale
//    let bassPart:Soloist = bass
    let rnd = new System.Random()

    interface IPitchGenerator with
        member x.Next() =
            let rootNote = bass.CurrentPitch
            let secondInterval = PitchAtInterval 2 scale rootNote
            let thirdInterval = PitchAtInterval 4 scale rootNote
            if rnd.Next(2) = 1 then thirdInterval else secondInterval
//            let chod
//            let chords = Chord.FindMatchingChords([| rootNote; secondInterval; thirdInterval |])
//            chords |> List.
end

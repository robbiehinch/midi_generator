﻿
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"

open Midi
open System.Linq

let loopBe1Out = OutputDevice.InstalledDevices.FirstOrDefault(fun d -> d.Name.Contains("LoopBe"))

loopBe1Out.Open()

//drums

//clock.Reset();
//loopBe1Out.SilenceAllNotes()

#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"


let clock = Clock(60.0f)

let noteRhythm = [|
                [| 1; 0; 1; 0 |];
                [| 1; 0; 1; 1 |];
                [| 0; 1; 1; 0 |];
                [| 0; 0; 0; 1 |];
//                [| 1; 0; 1; 1 |];
                |]

let scale = new Scale(new Note('C'), Scale.Major)
let bassGen = new PitchGenerator.Soloist(scale, Pitch.C3, Pitch.A1, Pitch.GSharp4)
let chordGen = new PitchGenerator.Chords(scale, bassGen)

let bassPitchGens =  [|
//    new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
//    new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
     bassGen :> PitchGenerator.IPitchGenerator
    |]

let chordPitchGens =  [|
//    new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
//    new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
     chordGen :> PitchGenerator.IPitchGenerator
    |]

let allPitchGens =  [|
     new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
     new PitchGenerator.Unit(Pitch.C0) :> PitchGenerator.IPitchGenerator
     bassGen :> PitchGenerator.IPitchGenerator
     chordGen :> PitchGenerator.IPitchGenerator
    |]

allPitchGens |> DrumSequencer.SequenceNotesOnChannels noteRhythm [| 0 |] 4 16 4 loopBe1Out 0.0f clock 0


clock.Start();

clock.Stop();
loopBe1Out.SilenceAllNotes()


//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 8192, 0.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 5000, 1.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 4000, 2.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 3000, 3.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 2000, 4.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 1000, 5.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 12000, 6.0f))
//clock.Schedule(new Midi.PitchBendMessage(loopBe1Out, Channel.Channel1, 8192, 10.0f))


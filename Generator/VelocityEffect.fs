﻿module VelocityEffect

open RhythmEvent

let rnd = System.Random()

let _isOnBeat beatTime beatToEmphasize barBeats =
    beatToEmphasize |> Array.exists (fun beatToEmphasize -> 0 = ((beatTime - beatToEmphasize) % barBeats))

let _emphasizeBeat beatsToEmphasize lowVal hiVal barBeats (iRhythmEvent:IRhythmEvent) =
    let hiVal5pct = hiVal / 20
    let lowVal10pct = lowVal / 20
    let startTime = (int)iRhythmEvent.StartTime
    match _isOnBeat startTime beatsToEmphasize barBeats with
            | true -> iRhythmEvent.Copy(iRhythmEvent.StartTime, iRhythmEvent.Duration, hiVal - hiVal5pct + rnd.Next(hiVal5pct))
            | _ -> iRhythmEvent.Copy(iRhythmEvent.StartTime, iRhythmEvent.Duration, lowVal - lowVal10pct + (rnd.Next(lowVal10pct) * 2))

let EmphasizeBeat beatsToEmphasize lowVal hiVal barBeats (hits:seq<IRhythmEvent>) = 
    hits |> Seq.map (fun hit -> _emphasizeBeat beatsToEmphasize lowVal hiVal barBeats hit)

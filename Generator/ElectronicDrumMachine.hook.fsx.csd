<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1

                giSine  ftgen 0, 0, 4096, 10, 1 

 

                instr 1

                            iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "electronic kick -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.2 

 aenv    expon 1, iDrumDur, 0.001 

 kcps    expon ( 100 * iPitch ), iDrumDur, 30 

 aEnvAmped  =  ( aenv * iAmp ) 

 asig    oscil aEnvAmped, kcps, giSine 

 aSigLPan, aSigRPan    pan2 asig, iPan 

  outs aSigLPan, aSigRPan

                            endin
instr 6

                            iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "snare -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.3 

 aenv    expon 1, iDrumDur, 0.001 

 anse    noise 1, 0 

 kcps    expon ( 200 * iPitch ), iDrumDur, 30 

 ajit    randomi 0.2, 1.8, 10000 

 aEnvAmped  =  ( aenv * iAmp ) 

 aJittercps  =  ( kcps * ajit ) 

 atne    oscil aEnvAmped, aJittercps, giSine 

 asig    sum ( atne * 0.5 ), ( anse * 0.5 ) 

 ares    vcomb asig, 0.02, 0.0035, 0.1 

 aout  =  ( ( ares * aenv ) * iAmp ) 

 aSigLPan, aSigRPan    pan2 asig, iPan 

  outs aSigLPan, aSigRPan

                            endin
instr 8

                            iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "closed hihat -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.1 

 aenv    expon 1, iDrumDur, 0.001 

 anse    noise iAmp, 0 

 asig  =  ( anse * aenv ) 

 asighp    buthp asig, ( 7000 * iPitch ) 

 aSigLPan, aSigRPan    pan2 asighp, iPan 

  outs aSigLPan, aSigRPan

                            endin
                instr 99
                endin
</CsInstruments>
<CsScore>

            i 1 0.00 1.50 115 59 0.50
i 1 1.50 2.00 82 49 0.50
i 1 2.00 1.50 116 48 0.50
i 1 3.50 2.00 76 59 0.50
i 1 4.00 1.50 114 48 0.50
i 1 5.50 2.00 76 49 0.50
i 1 6.00 1.50 117 59 0.50
i 1 7.50 2.00 78 48 0.50
i 1 8.00 1.50 119 49 0.50
i 1 9.50 2.00 78 59 0.50
i 1 10.00 1.50 116 48 0.50
i 1 11.50 2.00 78 49 0.50
i 1 12.00 1.50 116 59 0.50
i 1 13.50 2.00 78 49 0.50
i 1 14.00 1.50 116 48 0.50
i 1 15.50 2.00 82 49 0.50
i 1 16.00 1.50 117 48 0.50
i 1 17.50 2.00 76 49 0.50
i 1 18.00 1.50 116 48 0.50
i 1 19.50 2.00 76 59 0.50
i 1 20.00 1.50 115 49 0.50
i 1 21.50 2.00 80 48 0.50
i 1 22.00 1.50 115 49 0.50
i 1 23.50 2.00 82 48 0.50
i 1 24.00 1.50 118 49 0.50
i 1 25.50 2.00 82 59 0.50
i 1 26.00 1.50 114 49 0.50
i 1 27.50 2.00 82 59 0.50
i 1 28.00 1.50 119 48 0.50
i 1 29.50 2.00 76 49 0.50
i 1 30.00 1.50 118 59 0.50
i 1 31.50 2.00 78 48 0.50
i 6 0.50 2.00 76 48 0.60
i 6 3.00 2.00 82 49 0.60
i 6 2.50 2.00 76 48 0.60
i 6 5.00 2.00 82 59 0.60
i 6 4.50 2.00 82 48 0.60
i 6 7.00 2.00 80 49 0.60
i 6 6.50 2.00 82 59 0.60
i 6 9.00 2.00 78 49 0.60
i 6 8.50 2.00 80 48 0.60
i 6 11.00 2.00 76 59 0.60
i 6 10.50 2.00 78 48 0.60
i 6 13.00 2.00 78 59 0.60
i 6 12.50 2.00 82 49 0.60
i 6 15.00 2.00 76 59 0.60
i 6 14.50 2.00 80 48 0.60
i 6 17.00 2.00 82 49 0.60
i 6 16.50 2.00 78 59 0.60
i 6 19.00 2.00 78 49 0.60
i 6 18.50 2.00 82 59 0.60
i 6 21.00 2.00 80 49 0.60
i 6 20.50 2.00 80 59 0.60
i 6 23.00 2.00 80 49 0.60
i 6 22.50 2.00 80 59 0.60
i 6 25.00 2.00 82 49 0.60
i 6 24.50 2.00 82 59 0.60
i 6 27.00 2.00 82 48 0.60
i 6 26.50 2.00 76 49 0.60
i 6 29.00 2.00 80 59 0.60
i 6 28.50 2.00 82 49 0.60
i 6 31.00 2.00 80 59 0.60
i 6 30.50 2.00 82 49 0.60
i 6 33.00 2.00 76 48 0.60
            
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>

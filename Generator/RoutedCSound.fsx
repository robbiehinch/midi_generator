﻿

#r @"..\..\..\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"
#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#r @"..\CSoundDSL\bin\Debug\CSoundDSL.dll"

open Midi
open System.Linq
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

#load "CompilerExtensions.fs"
#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "Pan.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"
#load "MidiToCSoundScore.fs"
#load "ElectronicDrumMachine.fs"
#load "Routing.fs"


let nl = [|System.Environment.NewLine|]
let splitOpts = System.StringSplitOptions.None


let sendRouter = Routing.channSendHook "theChann"
let drumMachine = ElectronicDrumMachine.DrumInstruments ()
let drumGenFn = CompilerExtensions.genInstByMemberName drumMachine
let globals = (drumGenFn "GlobalVars" 0 CompilerExtensions.nonHook).Split(nl, splitOpts)
let electronicKick = (drumGenFn "Kick" 1 sendRouter).Split(nl, splitOpts)

        

let routedInstrument = Routing.StereoRoutedInstrument ("theChannL", "theChannR")
let routeGenFn = CompilerExtensions.genInstByMemberName routedInstrument
let routeText = (routedInstrument.PostProcess (routeGenFn "Recv" 2 CompilerExtensions.nonHook)).Split(nl, splitOpts)

let insts = [| globals; electronicKick; routeText |] |> Array.collect (fun x -> x)

printfn "%s" (insts |> String.concat System.Environment.NewLine)


let routing = drumMachine "Kick" 1 |> routedInstrument "Recv"


let midiDevice = OutputDevice.InstalledDevices.FirstOrDefault()
let drumClock = Clock(240.0f)


let aroundC3PitchGens =  [|
    new PitchGenerator.NotLast([| Pitch.C3; Pitch.CSharp3; Pitch.B3 |]) :> PitchGenerator.IPitchGenerator
    |]

let kickHits = [|
                [| 1; 0; 0; 1 |];
                |]


let noteGen pitchGen hits channel = 
    pitchGen |> DrumSequencer.GenMidiNotesOnChannels hits [| 0 |] 4 4 16 midiDevice 0.0f drumClock channel
                                    |> Seq.collect ( fun s -> s)
                                    |> Seq.toArray

let kickNotes = noteGen aroundC3PitchGens kickHits 0
let tempoScale = 0.5f
let kickScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes kickNotes) 0.5 tempoScale
let lastKickNote = kickNotes |> Array.maxBy (fun n -> n.Time + n.Duration)
let routeKickNote = NoteOnOffMessage(midiDevice, Channel.Channel2, lastKickNote.Pitch, lastKickNote.Velocity, 0.f, drumClock, lastKickNote.Time + lastKickNote.Duration)
let routeScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes [|routeKickNote|]) 0.5 tempoScale

let allScores = [|
                    kickScore
                    routeScore
                |]

let drumScoreLines = allScores |> Seq.collect (fun a -> a) |> String.concat "\n"

let score = sprintf "
            %s
            " drumScoreLines

System.Environment.CurrentDirectory

let orchestra = sprintf "
                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1

                %s

                %s
                instr 99
                endin" (globals |> String.concat "\n" ) (insts |> String.concat "\n" )

let option = "-odac"

let ofilePath = System.IO.Path.Combine(__SOURCE_DIRECTORY__, __SOURCE_FILE__ + ".csd")
System.IO.File.WriteAllText(ofilePath, (MidiToCSoundScore.GenCSD option orchestra score))
printfn "Wrote to: %s"ofilePath


let ExecuteInCSound option orchestra score p ct =
    use c = new csound6netlib.Csound6Net()
    c.SetOption(option) |> ignore
    c.CompileOrc(orchestra) |> ignore
    c.ReadScore(score) |> ignore
    c.Start() |> ignore
//    c.PerformAsync(p, ct)
    c.Perform() |> ignore
    c.Stop()
//
//printfn "Play?"
//let run = System.Console.In.ReadLine()
//
//if run.ToLower().StartsWith("y") then (ExecuteInCSound option orchestra score) 

let p = new System.Progress<float32>()
let ct = new System.Threading.CancellationToken()
let t = ExecuteInCSound option orchestra score p ct
//t.Wait()

    
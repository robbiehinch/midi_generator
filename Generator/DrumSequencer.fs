﻿module DrumSequencer

open Midi
open PatternGenerator
open RhythmEvent
open Note
open VelocityEffect
open System.Linq


let NotesToMidiMessages device channel offset clock (hits:seq<Note.INote>) = 
    hits |> Seq.map(fun drumHit -> new NoteOnOffMessage(device, channel, drumHit.Pitch, drumHit.Velocity, drumHit.StartTime + offset, clock, drumHit.Duration))

let PatternsToPattern patterns = patterns |> Seq.collect (fun a -> a)

let PatternsToNoteLengths patterns =
    let patternsFlat = patterns |> PatternsToPattern
//                                        |> Seq.toArray
    patternsFlat |> PatternGenerator.GenNoteLengths

let CreateRhythm patterns beatEmphasis maxNotLength sequenceBeats =
    let noteLengths = patterns |> PatternsToNoteLengths 
    noteLengths |> Seq.map (fun (start, dur) -> new RhythmEvent(start, dur, 127) :> IRhythmEvent)
//                                |> Seq.toArray

let ApplyPitchGenToRhythm rhythm pitchGen = 
    rhythm |> Seq.map (fun vr -> new RhythmNote(pitchGen, vr) :> INote)


let CreateMidiMessagesFromNoteGen rhythm pitchGen device channel offset clock =
    let sequence = ApplyPitchGenToRhythm rhythm pitchGen
    sequence |> NotesToMidiMessages device channel offset clock

let LoopRhythm repeats loopLength (rhythm:seq<IRhythmEvent>)  =
    Enumerable.Repeat(rhythm, repeats) |> Seq.mapi (fun i r -> (float32 (i*loopLength), r))
                                        |> Seq.map (fun (offset, rhythm) -> rhythm |> Seq.map (fun note -> note.Copy(note.StartTime + offset, note.Duration, note.Velocity)) )
                                        |> Seq.collect (fun r -> r)


let channelVals = Channel.GetValues(Channel.Channel1.GetType())

let GenLoopAndChannels patterns beatEmphasis maxNotLength sequenceBeats repeats firstChannel = 
    let rhythm = CreateRhythm patterns beatEmphasis maxNotLength sequenceBeats
    let rhythmLoop = rhythm |> LoopRhythm repeats sequenceBeats
                            |> VelocityEffect.EmphasizeBeat beatEmphasis 80 120 sequenceBeats
    let channels = channelVals |> Seq.cast<Channel>
                               |> Seq.skip (firstChannel-1)
    (rhythmLoop, channels)

let GenMidiNotesOnChannels patterns beatEmphasis maxNotLength sequenceBeats repeats device offset clock firstChannel pitchGenerators =
    let (rhythmLoop, channels) = GenLoopAndChannels patterns beatEmphasis maxNotLength sequenceBeats repeats firstChannel
    pitchGenerators |> Seq.zip channels
                    |> Seq.map (fun (channel, pitchGen) -> CreateMidiMessagesFromNoteGen rhythmLoop pitchGen device channel offset clock )

let SequenceNotesOnChannels patterns beatEmphasis maxNotLength sequenceBeats repeats device offset clock firstChannel pitchGenerators =
    pitchGenerators |> GenMidiNotesOnChannels patterns beatEmphasis maxNotLength sequenceBeats repeats device offset clock firstChannel
                    |> Seq.iter (fun channNotes -> channNotes |> Seq.iter (fun m -> clock.Schedule(m)))



﻿module CompilerExtensions

open Microsoft.FSharp.Quotations
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic


let OutputHook outputVar =
    ()

let nonHook expr =
    None


let outsHook expr =
    match expr with
        | DerivedPatterns.SpecificCall <@ OutputHook @> (_, _, exprList) ->
            let exprHead =  Quotations.Expr.Cast<Vector<double>[][]> exprList.Head
            let newExpr = <@ CSoundCGVector.Functions.outs %exprHead @>
            let compiledExpression = newExpr |> Compiler.compileInstrument nonHook
            Some (compiledExpression, { Compiler.CompileState.IsMultiParamFn = false })
        | _ -> None


let genInst fn instNum outsHook =
    let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn)
    let retStr = e.Value |> Compiler.compileInstrument outsHook
    if 0 = instNum then
        retStr
    else
        let ret = sprintf @"instr %d
                            %s
                            endin" instNum retStr
        ret
        

let memberToMethodBase obj fnName = 
    let mis = obj.GetType().GetMembers()
    let fn = mis |> Array.find (fun m -> m.Name.Contains(fnName))
    (fn :?> System.Reflection.MethodBase)

    
let genInstByMemberName obj fnName instNum outsHook =
    let fn = memberToMethodBase obj fnName
    genInst fn instNum outsHook


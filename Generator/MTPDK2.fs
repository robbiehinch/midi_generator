﻿module MTPDK2

open Midi

type MTPDK2() = class
        member x.Kick = Pitch.A1
        member x.Kick2 = Pitch.B1
        member x.Kick3 = Pitch.C2
        member x.SnareStick = Pitch.CSharp2
        member x.Snare1 = Pitch.D2
        member x.Snare2 = Pitch.DSharp2
        member x.Snare3 = Pitch.E2
        member x.TomLow1 = Pitch.F2
        member x.HiHatClosed = Pitch.FSharp2
        member x.TomLow2 = Pitch.G2
        member x.HiHatHalfClosed = Pitch.GSharp2
        member x.TomMid1 = Pitch.A2
        member x.HiHatOpen = Pitch.ASharp2
        member x.TomMid2 = Pitch.B2
        member x.TomHi1 = Pitch.C3
        member x.CrashLeft = Pitch.CSharp3
        member x.TomHi2 = Pitch.D3
        member x.Ride = Pitch.DSharp3
        member x.China = Pitch.E3
        member x.RideBell = Pitch.F3
        member x.Splash = Pitch.G3
        member x.CrashRight = Pitch.A3
        member x.HiHatPedal = Pitch.F4


end

<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1

                giSine  ftgen 0, 0, 4096, 10, 1 
 

                giSine  ftgen 0, 0, 4096, 10, 1 
 
instr 1
                            iAmp  =  ( p4 / 127 ) 
 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 
 iPan  =  p6 
  printf "electronic kick -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 
 iDrumDur  =  0.2 
 aenv    expon 1, iDrumDur, 0.001 
 kcps    expon ( 100 * iPitch ), iDrumDur, 30 
 aEnvAmped  =  ( aenv * iAmp ) 
 asig    oscil aEnvAmped, kcps, giSine 
 aSigLPan, aSigRPan    pan2 asig, iPan 
  chnset aSigLPan, "theChannL"
 chnset aSigRPan, "theChannR"
                            endin
instr 2
                            aSigL    chnget "theChannL" 
 aSigR    chnget "theChannR" 
  outs aSigL, aSigR
                            endin
                instr 99
                endin
</CsInstruments>
<CsScore>

            i 1 0.00 1.50 116 49 0.50
i 1 1.50 2.00 82 48 0.50
i 1 2.00 1.50 118 49 0.50
i 1 3.50 2.00 78 59 0.50
i 1 4.00 1.50 117 49 0.50
i 1 5.50 2.00 76 59 0.50
i 1 6.00 1.50 116 49 0.50
i 1 7.50 2.00 78 48 0.50
i 1 8.00 1.50 114 59 0.50
i 1 9.50 2.00 80 48 0.50
i 1 10.00 1.50 114 49 0.50
i 1 11.50 2.00 78 48 0.50
i 1 12.00 1.50 116 49 0.50
i 1 13.50 2.00 78 48 0.50
i 1 14.00 1.50 116 59 0.50
i 1 15.50 2.00 76 49 0.50
i 1 16.00 1.50 117 48 0.50
i 1 17.50 2.00 82 59 0.50
i 1 18.00 1.50 115 48 0.50
i 1 19.50 2.00 82 59 0.50
i 1 20.00 1.50 119 49 0.50
i 1 21.50 2.00 82 48 0.50
i 1 22.00 1.50 115 59 0.50
i 1 23.50 2.00 78 48 0.50
i 1 24.00 1.50 117 49 0.50
i 1 25.50 2.00 76 48 0.50
i 1 26.00 1.50 118 59 0.50
i 1 27.50 2.00 78 48 0.50
i 1 28.00 1.50 115 59 0.50
i 1 29.50 2.00 76 49 0.50
i 1 30.00 1.50 117 59 0.50
i 1 31.50 2.00 78 49 0.50
i 2 0.00 33.50 78 49 0.50
            
</CsScore>
</CsoundSynthesizer>
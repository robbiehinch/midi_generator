﻿module MidiToCSoundScore

type ScoreInput =
//    | MidiNotes of int
    | MidiNotes of seq<Midi.NoteOnOffMessage>
    | PannedMidiNotes of seq<Note.PannedNote<Midi.NoteOnOffMessage>>

let CSoundNoteFromPannedMidiNote (pannedNote:Note.PannedNote<Midi.NoteOnOffMessage>) tempoScale = 
    let note = pannedNote.note
    sprintf "i %d %.2f %.2f %d %d %.2f" (((int)note.Channel)+1) (note.Time * tempoScale) (note.Duration * tempoScale) ((int)note.Velocity) ((int)note.Pitch) pannedNote.pan


let CSoundNoteFromMidiNote (note:Midi.NoteOnOffMessage) pan tempoScale = 
    let pannedNote = new Note.PannedNote<Midi.NoteOnOffMessage>(note, pan)
    CSoundNoteFromPannedMidiNote pannedNote tempoScale


let Convert (notes:ScoreInput) pan tempoScale = match notes with
                                                | MidiNotes n -> n |> Seq.map (fun note -> CSoundNoteFromMidiNote note pan tempoScale)
                                                | PannedMidiNotes n -> n |> Seq.map (fun note -> CSoundNoteFromPannedMidiNote note tempoScale)

let GenCSD options orchestra score =
    sprintf "<CsoundSynthesizer>
<CsOptions>
%s
</CsOptions>
<CsInstruments>
%s
</CsInstruments>
<CsScore>
%s
</CsScore>
</CsoundSynthesizer>" options orchestra score



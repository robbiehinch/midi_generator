﻿
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"

open Midi
open System.Linq

let loopBe1Out = OutputDevice.InstalledDevices.FirstOrDefault(fun d -> d.Name.Contains("LoopBe"))

loopBe1Out.Open()



//chords

let cChord = Chord("C")
let CMaj7 = Chord("C#7")

let scheduleChord device channel velocity timeOn duration (clockS:Clock) (chord:Chord) = for note in chord.NoteSequence do
                                                                                            clockS.Schedule(new NoteOnOffMessage(device, channel, note.PitchInOctave(3), velocity, timeOn, clockS, duration))


let synthClock = Clock(120.0f)
cChord |> scheduleChord loopBe1Out Channel.Channel1 80 0.0f 3.0f synthClock
CMaj7 |> scheduleChord loopBe1Out Channel.Channel1 80 4.0f 3.0f synthClock
cChord |> scheduleChord loopBe1Out Channel.Channel2 80 8.0f 3.0f synthClock
CMaj7 |> scheduleChord loopBe1Out Channel.Channel2 80 12.0f 3.0f synthClock
cChord |> scheduleChord loopBe1Out Channel.Channel1 80 16.0f 3.0f synthClock
CMaj7 |> scheduleChord loopBe1Out Channel.Channel1 80 20.0f 3.0f synthClock
cChord |> scheduleChord loopBe1Out Channel.Channel2 80 16.0f 3.0f synthClock
CMaj7 |> scheduleChord loopBe1Out Channel.Channel2 80 20.0f 3.0f synthClock
synthClock.Start();

synthClock.Stop();
synthClock.Reset();


loopBe1Out.SilenceAllNotes()


﻿module SfzParser

open System.Linq
open System.Text.RegularExpressions

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic


type Region (samplePath:string, lorand:double, hirand:double) = class
    member x.samplePath = samplePath
    member x.lorand = lorand
    member x.hirand = hirand
end

type Group(regions:Region[], volume:int, amp_veltrack:int, key:int, lovel:int, hivel:int, locc64:string option, hicc64:string option) = class
    member x.regions = regions
    member x.volume = volume
    member x.amp_veltrack = amp_veltrack
    member x.key = key
    member x.lovel = lovel
    member x.hivel = hivel
    member x.locc64 = locc64
    member x.hicc64 = hicc64

end

let CreateFirstGroup (kvps:Map<string, string>) =
    let g = new Group([||], System.Int32.Parse(kvps.["volume"]), System.Int32.Parse(kvps.["amp_veltrack"]), System.Int32.Parse(kvps.["key"]), System.Int32.Parse(kvps.["lovel"]), System.Int32.Parse(kvps.["hivel"]), kvps.TryFind "locc64", kvps.TryFind "hicc64" )
    let r = []
    let ng:option<Group> = None
    (Some(g), r , ng)

let TryFindOrDefault (kvps:Map<'a,'b>) k d = match kvps.TryFind(k) with
                                                | Some x -> x
                                                | _ -> d

let FinishGroup (kvps:Map<string, string>) currentRegions (currentGroup:Group) =
    let volume = System.Int32.Parse(TryFindOrDefault kvps "volume" "100")
    let amp_veltrack = System.Int32.Parse(TryFindOrDefault kvps "amp_veltrack" "0")
    let key = System.Int32.Parse(TryFindOrDefault kvps "key" "0")
    let lovel = System.Int32.Parse(TryFindOrDefault kvps "lovel" "0")
    let hivel = System.Int32.Parse(TryFindOrDefault kvps "hivel" "127")
    let locc64 = kvps.TryFind "locc64"
    let hicc64 = kvps.TryFind "hicc64"
    let g = new Group(currentRegions |> List.toArray, currentGroup.volume, currentGroup.amp_veltrack, currentGroup.key, currentGroup.lovel, currentGroup.hivel, currentGroup.locc64, currentGroup.hicc64 )
    let r = []
    let ng = new Group([||], volume, amp_veltrack, key, lovel, hivel, locc64, hicc64 )
    (Some(ng), r , Some(g))


let doubleRE = new Regex("\d+(\.\d+)?")
let HandleDoubleField str =
    let doubleChars = doubleRE.Match(str).Value
    System.Double.Parse(doubleChars)

let GetField (kvps:Map<string, string>) field defaultVal =
    match kvps.TryFind(field) with
        | Some x -> HandleDoubleField x
        | _ -> defaultVal

let AppendRegion (kvps:Map<string, string>) currentRegions currentGroup pathRoot =
    let g = currentGroup
    let sample = kvps.["sample"]//.Replace("\\", "\\\\")
    let absPathSample = (String.concat "\\" [| pathRoot; sample |]).Replace("\\", "\\\\")
    let lorand = GetField kvps "lorand" 0.0
    let hirand = GetField kvps "hirand" 1.0
    let r = List.append currentRegions [ new Region(absPathSample, lorand, hirand) ]
    let ng:option<Group> = None
    (g, r , ng)
//sample=OH\kick_OH_FF_12.wav lorand=0.90 hirand=1

let ParseSfzText fileRoot (fileLines:seq<string>) =
    seq {
        let currentGroup:Ref< option<Group> > = ref None
        let currentRegions:Ref< List<Region> > = ref []
        for line in fileLines do
            let lineSplit = line.Split([| ' '; '\t' |])
            let kvpSplit = lineSplit.Skip(1) |> Seq.map (fun s -> s.Split([| '=' |]))
                                                |> Seq.toArray
            let allKVPs = kvpSplit |> Array.filter (fun e -> 2 = e.Length)
            let kvps = if allKVPs.Length >= 1 then Some( allKVPs |> Seq.map (fun split -> (split.[0], split.[1])) |> Map.ofSeq) else None
            let retCurrentGroup, retCurrentRegion, retYieldGroup = match kvps, lineSplit.[0], !currentGroup with
                                                                    | Some kvps, "<group>", None   ->  CreateFirstGroup kvps
                                                                    | Some kvps, "<group>", Some x ->  FinishGroup kvps !currentRegions x
                                                                    | Some kvps, "<region>", _     ->  AppendRegion kvps !currentRegions !currentGroup fileRoot
                                                                    | _                 ->  (!currentGroup, !currentRegions, None)

            currentGroup := retCurrentGroup
            currentRegions := retCurrentRegion
            match retYieldGroup with
                | Some x -> yield x
                | _ -> ()

        let cgv = currentGroup.Value.Value
        yield new Group(!currentRegions |> List.toArray, cgv.volume, cgv.amp_veltrack, cgv.key, cgv.lovel, cgv.hivel, cgv.locc64, cgv.hicc64 )
    }


let ParseSfzFileFromPath fileRoot filePath =
    let fileLines = System.IO.Path.Combine(fileRoot, filePath) |> System.IO.File.ReadAllLines
    ParseSfzText fileRoot fileLines


let SampleToCSoundTable label i (r:Region) =
    sprintf "giSoundFile%s%d   ftgen   0, 0, 0, 1, \"%s\", 0, 0, 0" label i r.samplePath


let LabelledGroupsToCSoundTables (labelledGroups:Group[]*string) =
    let groups = fst labelledGroups
    let label = snd labelledGroups
    let flatGroups = groups |> Seq.collect (fun g -> g.regions)
    let ret = flatGroups |> Seq.mapi (fun i g -> SampleToCSoundTable label (i+1) g)
                            |> Seq.toArray
    let tables = flatGroups |> Seq.mapi (fun i g -> sprintf "giSoundFile%s%d" label (i+1))
    let sampleChoice = sprintf "gi%s_current[] fillarray %s" label (String.concat ", " (groups |> Array.map (fun g -> "0")))
    let sampleArray = sprintf "gi%sArr[] fillarray %s" label (String.concat ", " tables)
    let velocityGroups = groups |> Array.sortBy (fun g -> g.hivel)
    let groupLengths = velocityGroups |> Array.map (fun g -> g.regions.Length.ToString())
    let groupLengthsDef = sprintf "gi%s_lengths[] fillarray %s" label (String.concat ", " groupLengths)
    let groupOffsets = velocityGroups |> Array.map (fun g -> g.regions.Length) |> Array.scan (+) 0 |> Array.map (fun i -> i.ToString())
    let groupOffsetsDef = sprintf "gi%s_offsets[] fillarray %s" label (String.concat ", " groupOffsets)
    let velocityArray = velocityGroups |> Array.map (fun g -> g.hivel.ToString()) |> String.concat ", "
    let velocityVars = sprintf "gi%s_vels[] fillarray %s" label velocityArray
    String.concat "\n" [|
                        (String.concat "\n" ret)
                        sampleChoice
                        sampleArray
                        velocityVars
                        groupLengthsDef
                        groupOffsetsDef
                        |]

//[<ReflectedDefinition>]
let rec IndexOf((arr:_[]), searchVal, currIndex) =
    let foundVal = arr.[currIndex]
    if foundVal > searchVal then
        currIndex
    else
        IndexOf(arr, searchVal, currIndex+1)    


type SampleToCSoundInstrumentQuotation (sr, labelledGroups:Group[]*string) =
    let groups = fst labelledGroups
    let label = snd labelledGroups
    let flatGroups = groups |> Seq.collect (fun g -> g.regions)

    let mutable giSamples = [||]
    let mutable gi_SAMPLE_CHOICE = [||]
    let mutable gi_SAMPLE_ARRAY = [||]
    let mutable gi_SAMPLE_VELOCITIES = [||]
    let mutable gi_SAMPLE_LENGTHS = [||]
    let mutable gi_SAMPLE_OFFSETS = [||]

    member x.replacePlaceHolders (placeHolderStr:string) =
        placeHolderStr.Replace("gi_SAMPLE_CHOICE", sprintf "gi%s_current" label)
                        .Replace("gi_SAMPLE_ARRAY", sprintf "gi%sArr" label)
                        .Replace("gi_SAMPLE_LENGTHS", sprintf "gi%s_lengths" label)
                        .Replace("gi_SAMPLE_OFFSETS", sprintf "gi%s_offsets" label)
                        .Replace("gi_SAMPLE_VELOCITIES", sprintf "gi%s_vels" label)

    [<ReflectedDefinition>]
    member x.myfn p0 p1 p2 p3 p4 p5 p6 =
        let kAmp = p4/127.0   //amplitude
        let kPitch = CSoundCGVector.Functions.powoftwo((60.0f-p5)/12.0f)//  ; pitch/speed
        let kLoopStart = 0   //; point where looping begins (in seconds)
        let kPan = p6//";
    //    Functions.printf "playing: %s %%d, amp: %%f, pitch: %%f, pan: %%f\\n", 1, %s, kAmp, kPitch, kPan sampleLabel sampleChoice
        let iSampleArrayChoice = IndexOf(gi_SAMPLE_VELOCITIES, p4, 0)
        let iSampleOffset = gi_SAMPLE_OFFSETS.[iSampleArrayChoice]
        let iSampleChoiceInArray = gi_SAMPLE_CHOICE.[iSampleArrayChoice]
        let iSampleChoice = iSampleOffset + iSampleChoiceInArray
        let iSampleId = gi_SAMPLE_ARRAY.[iSampleChoiceInArray]
        let iVelocityLengths = gi_SAMPLE_LENGTHS.[iSampleArrayChoice]
        gi_SAMPLE_CHOICE.[iSampleArrayChoice] <- ( ( iSampleChoiceInArray + 1 ) %  iVelocityLengths ) 

        let sampleArrayLen = CSoundCGVector.Functions.lenarray gi_SAMPLE_ARRAY
        CSoundCGVector.Functions.printf("sample new val: %d, arr len: %d\\n", 1.f, iSampleChoice, [| sampleArrayLen |])
    //    sprintf "printf \"sample new val: %%d, arr len: %%d\\n\", 1, %s, lenarray(%s)" sampleChoice sampleArray;
        let kLoopEnd = (CSoundCGVector.Functions.nsamp iSampleId)/sr//; loop end (end of file)
        let kCrossFade = 0//   ; cross-fade time
    //            ; read audio from the function table using the flooper2 opcode
        let aSigL, aSigR = CSoundCGVector.Functions.loscil(CSoundOpcode.ScalarOrArray.Scalar kAmp, kPitch, iSampleId, 1m, 0m)
        let kPanL = CSoundCGVector.Functions.min (1.0f-kPan, [|0.5f|])
        let kPanR = CSoundCGVector.Functions.min (kPan, [|0.5f|])
        let kPanL = kPanL * 2.f
        let kPanR = kPanR * 2.f
        CSoundCGVector.Functions.printf("kPanL: %f, kPanR: %f\\n", 1.f, kPanL, [|kPanR|])
        let aSigLPan = aSigL * (float)kPanL
        let aSigRPan = aSigR * (float)kPanR
        CSoundCGVector.Functions.outs [| [| aSigLPan; aSigRPan |] |]//; send audio to output
    //    let outs = aSigLPan, aSigRPan //; send audio to output
    //    outs
    //                endin

    member x.qfn =
        let mis = x.GetType().GetMembers()
        let fn = mis |> Array.find (fun m -> m.Name.Contains("myfn"))
        let e = Microsoft.FSharp.Quotations.Expr.TryGetReflectedDefinition(fn :?> System.Reflection.MethodBase)
        let retStr = e.Value |> Compiler.compileInstrument CompilerExtensions.nonHook
        x.replacePlaceHolders retStr


let createInstrumentDefn instNum body =
        sprintf "instr %d
            %s
        endin" instNum body

//let SampleToCSoundInstrument instrNum sampleI sampleLabel =
//    let sampleLabelIndex = sprintf "%s%d" sampleLabel sampleI
//    let sampleChoice = sprintf "gi%s" sampleLabel
//    let sampleArray = sprintf "gi%sArr" sampleLabel
//
//    let strs = [|
//        sprintf "  instr %d, %s ; play audio from function table using loscil p4: Amp, p5: Pitch, p6: Pan" instrNum sampleLabel;
//            "kAmp         =         p4/127   ; amplitude
//                kPitch       =         powoftwo((60.0-p5)/12.0)  ; pitch/speed
//                kLoopStart   =         0   ; point where looping begins (in seconds)
//                kPan         =   p6";
//        sprintf "printf \"playing: %s %%d, amp: %%f, pitch: %%f, pan: %%f\\n\", 1, %s, kAmp, kPitch, kPan" sampleLabel sampleChoice;
//        sprintf "iSampleId = %s[%s]" sampleArray sampleChoice;
//        sprintf "%s = (%s + 1) %% lenarray(%s)" sampleChoice sampleChoice sampleArray;
//        sprintf "printf \"sample new val: %%d, arr len: %%d\\n\", 1, %s, lenarray(%s)" sampleChoice sampleArray;
//            "kLoopEnd     =         nsamp(iSampleId)/sr; loop end (end of file)
//                kCrossFade   =         0   ; cross-fade time
//                ; read audio from the function table using the flooper2 opcode
//                aSigL, aSigR loscil kAmp, kPitch, iSampleId, 1, 0
//                kPanL min (1.0-kPan), 0.5
//                kPanR min kPan, 0.5
//                kPanL = kPanL * 2
//                kPanR = kPanR * 2
//                printf \"kPanL: %f, kPanR: %f\\n\", 1, kPanL, kPanR
//                aSigLPan = aSigL * kPanL
//                aSigRPan = aSigR * kPanR
//                outs       aSigLPan, aSigRPan ; send audio to output
//                  endin
//                "
//    |]
//    String.concat "\n" strs


let LabelledGroupsToCSoundFtgen (groups:seq<Group[]*string>) =
    let sr = 41000m
    let gRet = groups |> Seq.mapi (fun i sp -> (LabelledGroupsToCSoundTables sp, (SampleToCSoundInstrumentQuotation(sr, sp).qfn |> createInstrumentDefn (i+1))))// (i+1) (1) (snd sp) )))
                        |> Seq.toArray
    gRet


let GroupsToKeys (groups:seq<Group>) =
    groups |> Seq.groupBy (fun g -> g.key, g.locc64, g.hicc64)
                |> Map.ofSeq


let get (strings:seq<string>) =
    let first' = strings |> Seq.tryFind (fun _ -> true)
    let isWhiteSpace = System.String.IsNullOrWhiteSpace
    
    let mapper substringLength (first:string) currentStrings offset =
        if substringLength + offset < first.Length then
            let currentSubstring = first.Substring(offset, substringLength)
            if not(isWhiteSpace(currentSubstring)) &&
               not(currentStrings |> List.exists(fun f -> f = currentSubstring)) then
                currentSubstring :: currentStrings
            else currentStrings
        else
            currentStrings

    match first' with
    | Some(first) ->
        [first.Length - 1 .. -1 .. 0]
        |> List.map(fun substringLength ->
            [0 .. first.Length]
            |> List.fold (mapper substringLength first) List.Empty)
        |> List.concat
        |> Seq.sortBy(fun s -> s)
        |> Seq.sortBy(fun s -> -s.Length)
        |> Seq.filter(fun s -> strings |> Seq.forall(fun c -> c.Contains(s)))
    | None -> Seq.empty

let getFromStart (strings:seq<string>) =
    let possibleSubStrings (str:string) =
        [str.Length-1 .. -1 .. 0] |> List.map(fun i -> str.Substring(0, i))

    strings
    |> List.ofSeq
    |> List.map possibleSubStrings
    |> List.concat
    |> Seq.distinct
    |> Seq.find(fun s -> strings |> Seq.forall(fun c -> c.StartsWith(s)))


let CommonLabel (groups:Group[]) =
    let labels = groups |> Seq.map ( fun g -> g.regions )
                        |> Seq.collect ( fun r -> r )
                        |> Seq.map ( fun r -> System.IO.Path.GetFileName( r.samplePath ) )
    let common = (labels |> getFromStart).Split([|'.'|]).[0]
    groups, common


let LabelGroups (groups:seq<Group[]>) =
    groups |> Seq.map (fun gs -> CommonLabel gs)


let groupsToLabelledGroups groups =
    let keyedGroups = GroupsToKeys groups
    let labelledGroups = keyedGroups |> Seq.map (fun k -> k.Value |> Seq.toArray )
                                        |> LabelGroups
                                        |> Seq.toArray
    labelledGroups

let GroupsToCSoundFtgen groups = 
    let labelledGroups = groupsToLabelledGroups groups
    LabelledGroupsToCSoundFtgen labelledGroups


let SfzToCSoundFtgen folder sfzFile =
    let groups = (ParseSfzFileFromPath folder sfzFile) |> Seq.toArray
    GroupsToCSoundFtgen groups 


let channelLabels folder sfzFile =
    let groups = (ParseSfzFileFromPath folder sfzFile) |> Seq.toArray
    let labelledGroups = groupsToLabelledGroups groups
    labelledGroups |> Array.map (fun (_, l) -> l)


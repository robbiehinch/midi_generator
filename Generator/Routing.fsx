﻿

#r @"..\packages\MathNet.Numerics.2.5.0\lib\net40\MathNet.Numerics.dll"
#r @"..\packages\MathNet.Numerics.FSharp.2.5.0\lib\net40\MathNet.Numerics.FSharp.dll"
#r @"..\CSoundDSL\bin\Debug\CSoundDSL.dll"

open System.Linq
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.LinearAlgebra.Generic

#load "CompilerExtensions.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "VelocityEffect.fs"
#load "ElectronicDrumMachine.fs"
#load "Routing.fs"


let sendRouter = Routing.channSendHook "theChann"
let drumMachine = ElectronicDrumMachine.DrumInstruments ()
let drumGenFn = CompilerExtensions.genInstByMemberName drumMachine
let globals = (drumGenFn "GlobalVars" 0 CompilerExtensions.nonHook).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)
let electronicKick = (drumGenFn "Kick" 1 sendRouter).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)


let routedInstrument = Routing.StereoRoutedInstrument ("theChannL", "theChannR")
let genFn = CompilerExtensions.genInstByMemberName routedInstrument
let routeText = (routedInstrument.PostProcess (genFn "Recv" 1 CompilerExtensions.nonHook)).Split([|System.Environment.NewLine|], System.StringSplitOptions.None)

let insts = [| globals; electronicKick; routeText |] |> Array.collect (fun x -> x)

printfn "%s" (insts |> String.concat System.Environment.NewLine)

let instVars = VarExtract.getVars (CompilerExtensions.memberToMethodBase routedInstrument "Recv")


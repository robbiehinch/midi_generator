﻿module PatternGenerator

let BinarySeqToPositions binSeq =
    binSeq |> Seq.mapi (fun i b -> if b > 0 then i else -1)
            |> Seq.filter (fun i -> i >= 0)


let EventSequenceToEventNotes (maxNoteLength:float32) (eventSequence:float32[]) =
    let lastItem = [| ( ( eventSequence.[eventSequence.Length-1] ), maxNoteLength ) |]
    let followingSequences = eventSequence |> Seq.skip 1
    let zipped = followingSequences |> Seq.zip eventSequence
    let timeAndLength = zipped |> Seq.map ( fun (eCurr, eNext) -> (eCurr, Operators.min (eNext-eCurr) maxNoteLength) )
    lastItem |> Seq.append timeAndLength
//                        |> Seq.toArray
//    ret
                


let GenNoteLengths binSeqArray = 
    binSeqArray |> BinarySeqToPositions
                |> Seq.map float32
                |> Seq.toArray
                |> EventSequenceToEventNotes 4.0f


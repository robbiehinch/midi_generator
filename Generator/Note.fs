﻿module Note

open Midi
open PitchGenerator
open RhythmEvent


type INote =
    abstract Pitch : Pitch with get
    abstract StartTime : float32 with get
    abstract Duration : float32 with get
    abstract Velocity : int with get
    abstract member CopyWithNewVelocity : int -> INote
    abstract member CopyWithNewPitchGen : IPitchGenerator -> INote


type ConcreteNote(pitchGen:IPitchGenerator, startTime:float32, duration:float32, velocity:int) = class
    let pitchGen = pitchGen

    interface INote with
        member x.Pitch with get () = pitchGen.Next()
        member x.StartTime = startTime
        member x.Duration = duration
        member x.Velocity = velocity

        member x.CopyWithNewVelocity newVelocity =
            new ConcreteNote(pitchGen, startTime, duration, newVelocity) :> INote

        member x.CopyWithNewPitchGen pitchGen =
            new ConcreteNote(pitchGen, startTime, duration, velocity) :> INote

end


type RhythmNote(pitchGen:IPitchGenerator, rhythmEvent:IRhythmEvent) = class
    let pitchGen = pitchGen
    let rhythmEvent = rhythmEvent

    interface INote with
        member x.Pitch with get () = pitchGen.Next()
        member x.StartTime = rhythmEvent.StartTime
        member x.Duration = rhythmEvent.Duration
        member x.Velocity = rhythmEvent.Velocity

        member x.CopyWithNewVelocity newVelocity =
            new RhythmNote(pitchGen, rhythmEvent.Copy(rhythmEvent.StartTime, rhythmEvent.Duration, newVelocity)) :> INote

        member x.CopyWithNewPitchGen pitchGen =
            new RhythmNote(pitchGen, rhythmEvent) :> INote

end


type PannedNote<'T>(note:'T, pan:float) = class
    member x.note = note
    member x.pan = pan
end


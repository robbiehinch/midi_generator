<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1


opcode IndexOf, i, i[]ii

iAin[], iValSearch, iCurrIndex xin             ; read input parameters
iOut = 0

	iFoundVal = iAin[iCurrIndex]
	if (iFoundVal > iValSearch) then
		iOut = iCurrIndex
	else
		iOut IndexOf iAin, iValSearch, (iCurrIndex+1)
	endif


xout iOut
endop

giSine		ftgen		0,0,4096,10,1	;SINE WAVE

                giSoundFilehihatClosed_OH_1   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_1.wav", 0, 0, 0
giSoundFilehihatClosed_OH_2   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_2.wav", 0, 0, 0
giSoundFilehihatClosed_OH_3   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_3.wav", 0, 0, 0
giSoundFilehihatClosed_OH_4   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_4.wav", 0, 0, 0
giSoundFilehihatClosed_OH_5   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_5.wav", 0, 0, 0
giSoundFilehihatClosed_OH_6   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_6.wav", 0, 0, 0
giSoundFilehihatClosed_OH_7   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_7.wav", 0, 0, 0
giSoundFilehihatClosed_OH_8   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_8.wav", 0, 0, 0
giSoundFilehihatClosed_OH_9   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_9.wav", 0, 0, 0
giSoundFilehihatClosed_OH_10   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_10.wav", 0, 0, 0
giSoundFilehihatClosed_OH_11   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_11.wav", 0, 0, 0
giSoundFilehihatClosed_OH_12   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_12.wav", 0, 0, 0
giSoundFilehihatClosed_OH_13   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_13.wav", 0, 0, 0
giSoundFilehihatClosed_OH_14   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_14.wav", 0, 0, 0
giSoundFilehihatClosed_OH_15   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_15.wav", 0, 0, 0
giSoundFilehihatClosed_OH_16   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_16.wav", 0, 0, 0
giSoundFilehihatClosed_OH_17   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_17.wav", 0, 0, 0
giSoundFilehihatClosed_OH_18   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_18.wav", 0, 0, 0
giSoundFilehihatClosed_OH_19   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_19.wav", 0, 0, 0
giSoundFilehihatClosed_OH_20   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_F_20.wav", 0, 0, 0
giSoundFilehihatClosed_OH_21   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_1.wav", 0, 0, 0
giSoundFilehihatClosed_OH_22   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_2.wav", 0, 0, 0
giSoundFilehihatClosed_OH_23   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_3.wav", 0, 0, 0
giSoundFilehihatClosed_OH_24   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_4.wav", 0, 0, 0
giSoundFilehihatClosed_OH_25   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_5.wav", 0, 0, 0
giSoundFilehihatClosed_OH_26   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_6.wav", 0, 0, 0
giSoundFilehihatClosed_OH_27   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_7.wav", 0, 0, 0
giSoundFilehihatClosed_OH_28   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_8.wav", 0, 0, 0
giSoundFilehihatClosed_OH_29   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_9.wav", 0, 0, 0
giSoundFilehihatClosed_OH_30   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_10.wav", 0, 0, 0
giSoundFilehihatClosed_OH_31   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_11.wav", 0, 0, 0
giSoundFilehihatClosed_OH_32   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_12.wav", 0, 0, 0
giSoundFilehihatClosed_OH_33   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_13.wav", 0, 0, 0
giSoundFilehihatClosed_OH_34   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_14.wav", 0, 0, 0
giSoundFilehihatClosed_OH_35   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_15.wav", 0, 0, 0
giSoundFilehihatClosed_OH_36   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_16.wav", 0, 0, 0
giSoundFilehihatClosed_OH_37   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_17.wav", 0, 0, 0
giSoundFilehihatClosed_OH_38   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_18.wav", 0, 0, 0
giSoundFilehihatClosed_OH_39   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_19.wav", 0, 0, 0
giSoundFilehihatClosed_OH_40   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\hihatClosed_OH_P_20.wav", 0, 0, 0
gihihatClosed_OH__current[] fillarray 0, 0
gihihatClosed_OH_Arr[] fillarray giSoundFilehihatClosed_OH_1, giSoundFilehihatClosed_OH_2, giSoundFilehihatClosed_OH_3, giSoundFilehihatClosed_OH_4, giSoundFilehihatClosed_OH_5, giSoundFilehihatClosed_OH_6, giSoundFilehihatClosed_OH_7, giSoundFilehihatClosed_OH_8, giSoundFilehihatClosed_OH_9, giSoundFilehihatClosed_OH_10, giSoundFilehihatClosed_OH_11, giSoundFilehihatClosed_OH_12, giSoundFilehihatClosed_OH_13, giSoundFilehihatClosed_OH_14, giSoundFilehihatClosed_OH_15, giSoundFilehihatClosed_OH_16, giSoundFilehihatClosed_OH_17, giSoundFilehihatClosed_OH_18, giSoundFilehihatClosed_OH_19, giSoundFilehihatClosed_OH_20, giSoundFilehihatClosed_OH_21, giSoundFilehihatClosed_OH_22, giSoundFilehihatClosed_OH_23, giSoundFilehihatClosed_OH_24, giSoundFilehihatClosed_OH_25, giSoundFilehihatClosed_OH_26, giSoundFilehihatClosed_OH_27, giSoundFilehihatClosed_OH_28, giSoundFilehihatClosed_OH_29, giSoundFilehihatClosed_OH_30, giSoundFilehihatClosed_OH_31, giSoundFilehihatClosed_OH_32, giSoundFilehihatClosed_OH_33, giSoundFilehihatClosed_OH_34, giSoundFilehihatClosed_OH_35, giSoundFilehihatClosed_OH_36, giSoundFilehihatClosed_OH_37, giSoundFilehihatClosed_OH_38, giSoundFilehihatClosed_OH_39, giSoundFilehihatClosed_OH_40
gihihatClosed_OH__vels[] fillarray 65, 127
gihihatClosed_OH__lengths[] fillarray 20, 20
gihihatClosed_OH__offsets[] fillarray 0, 20, 40

                instr 1

        iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "electronic kick -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.2 

 aenv    expon 1, iDrumDur, 0.001 

 kcps    expon ( 100 * iPitch ), iDrumDur, 20 

 aEnvAmped  =  ( aenv * iAmp ) 

 asig    oscil aEnvAmped, kcps, giSine 

 aSigLPan, aSigRPan    pan2 asig, iPan 

  outs aSigLPan, aSigRPan

    endin
instr 6

        iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "snare -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.3 

 aenv    expon 1, iDrumDur, 0.001 

 anse    noise 1, 0 

 kcps    expon ( 200 * iPitch ), iDrumDur, 20 

 ajit    randomi 0.2, 1.8, 10000 

 aEnvAmped  =  ( aenv * iAmp ) 

 aJittercps  =  ( kcps * ajit ) 

 atne    oscil aEnvAmped, aJittercps, giSine 

 asig    sum ( atne * 0.5 ), ( anse * 0.5 ) 

 ares    vcomb asig, 0.02, 0, 0.1 

 aout  =  ( ( ares * aenv ) * iAmp ) 

 aSigLPan, aSigRPan    pan2 aout, iPan 

  outs aSigLPan, aSigRPan

    endin
instr 8

        iAmp  =  ( p4 / 127 ) 

 iPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 

 iPan  =  p6 

  printf "closed hihat -> iPitch: %f, iAmp: %f\n", 1, iPitch, iAmp 

 iDrumDur  =  0.1 

 aenv    expon 1, iDrumDur, 0.001 

 anse    noise iAmp, 0 

 asig  =  ( anse * aenv ) 

 asighp    buthp asig, 7000 

 aSigLPan, aSigRPan    pan2 asighp, iPan 

  outs aSigLPan, aSigRPan

    endin
                instr 99
                endin
</CsInstruments>
<CsScore>

            i 1 0.00 0.30 114 49 0.50
i 1 0.30 0.40 82 48 0.50
i 1 0.40 0.30 119 49 0.50
i 1 0.70 0.40 80 59 0.50
i 1 0.80 0.30 115 48 0.50
i 1 1.10 0.40 78 59 0.50
i 1 1.20 0.30 117 49 0.50
i 1 1.50 0.40 76 59 0.50
i 1 1.60 0.30 114 49 0.50
i 1 1.90 0.40 82 59 0.50
i 1 2.00 0.30 114 48 0.50
i 1 2.30 0.40 78 59 0.50
i 1 2.40 0.30 117 49 0.50
i 1 2.70 0.40 76 59 0.50
i 1 2.80 0.30 115 49 0.50
i 1 3.10 0.40 80 48 0.50
i 1 3.20 0.30 119 59 0.50
i 1 3.50 0.40 82 48 0.50
i 1 3.60 0.30 119 59 0.50
i 1 3.90 0.40 78 48 0.50
i 1 4.00 0.30 119 49 0.50
i 1 4.30 0.40 82 59 0.50
i 1 4.40 0.30 119 48 0.50
i 1 4.70 0.40 78 49 0.50
i 1 4.80 0.30 116 48 0.50
i 1 5.10 0.40 82 59 0.50
i 1 5.20 0.30 118 49 0.50
i 1 5.50 0.40 80 59 0.50
i 1 5.60 0.30 118 48 0.50
i 1 5.90 0.40 76 59 0.50
i 1 6.00 0.30 119 48 0.50
i 1 6.30 0.40 82 59 0.50
i 8 0.00 0.10 117 49 0.40
i 8 0.10 0.10 76 59 0.40
i 8 0.20 0.10 80 48 0.40
i 8 0.30 0.40 78 59 0.40
i 8 0.40 0.10 117 48 0.40
i 8 0.50 0.10 82 49 0.40
i 8 0.60 0.10 78 59 0.40
i 8 0.70 0.40 78 48 0.40
i 8 0.80 0.10 119 49 0.40
i 8 0.90 0.10 80 59 0.40
i 8 1.00 0.10 82 49 0.40
i 8 1.10 0.40 80 59 0.40
i 8 1.20 0.10 117 49 0.40
i 8 1.30 0.10 78 48 0.40
i 8 1.40 0.10 76 49 0.40
i 8 1.50 0.40 82 59 0.40
i 8 1.60 0.10 114 48 0.40
i 8 1.70 0.10 78 59 0.40
i 8 1.80 0.10 82 48 0.40
i 8 1.90 0.40 76 59 0.40
i 8 2.00 0.10 118 48 0.40
i 8 2.10 0.10 76 59 0.40
i 8 2.20 0.10 80 48 0.40
i 8 2.30 0.40 78 59 0.40
i 8 2.40 0.10 118 49 0.40
i 8 2.50 0.10 80 59 0.40
i 8 2.60 0.10 78 49 0.40
i 8 2.70 0.40 78 59 0.40
i 8 2.80 0.10 117 49 0.40
i 8 2.90 0.10 82 48 0.40
i 8 3.00 0.10 78 59 0.40
i 8 3.10 0.40 76 49 0.40
i 8 3.20 0.10 119 48 0.40
i 8 3.30 0.10 78 59 0.40
i 8 3.40 0.10 80 49 0.40
i 8 3.50 0.40 78 48 0.40
i 8 3.60 0.10 117 49 0.40
i 8 3.70 0.10 82 48 0.40
i 8 3.80 0.10 82 49 0.40
i 8 3.90 0.40 82 48 0.40
i 8 4.00 0.10 116 49 0.40
i 8 4.10 0.10 80 59 0.40
i 8 4.20 0.10 80 49 0.40
i 8 4.30 0.40 78 48 0.40
i 8 4.40 0.10 114 49 0.40
i 8 4.50 0.10 78 48 0.40
i 8 4.60 0.10 80 59 0.40
i 8 4.70 0.40 82 49 0.40
i 8 4.80 0.10 119 48 0.40
i 8 4.90 0.10 82 49 0.40
i 8 5.00 0.10 80 59 0.40
i 8 5.10 0.40 78 48 0.40
i 8 5.20 0.10 116 59 0.40
i 8 5.30 0.10 82 48 0.40
i 8 5.40 0.10 76 49 0.40
i 8 5.50 0.40 80 48 0.40
i 8 5.60 0.10 117 59 0.40
i 8 5.70 0.10 78 49 0.40
i 8 5.80 0.10 80 59 0.40
i 8 5.90 0.40 76 48 0.40
i 8 6.00 0.10 117 49 0.40
i 8 6.10 0.10 76 59 0.40
i 8 6.20 0.10 82 48 0.40
i 8 6.30 0.40 82 49 0.40
i 6 0.10 0.40 78 48 0.60
i 6 0.60 0.40 82 59 0.60
i 6 0.50 0.40 80 48 0.60
i 6 1.00 0.40 78 59 0.60
i 6 0.90 0.40 80 48 0.60
i 6 1.40 0.40 80 49 0.60
i 6 1.30 0.40 78 48 0.60
i 6 1.80 0.40 76 59 0.60
i 6 1.70 0.40 78 49 0.60
i 6 2.20 0.40 82 59 0.60
i 6 2.10 0.40 82 49 0.60
i 6 2.60 0.40 76 59 0.60
i 6 2.50 0.40 80 49 0.60
i 6 3.00 0.40 78 59 0.60
i 6 2.90 0.40 76 49 0.60
i 6 3.40 0.40 78 48 0.60
i 6 3.30 0.40 82 49 0.60
i 6 3.80 0.40 78 59 0.60
i 6 3.70 0.40 80 49 0.60
i 6 4.20 0.40 82 48 0.60
i 6 4.10 0.40 78 59 0.60
i 6 4.60 0.40 82 49 0.60
i 6 4.50 0.40 80 48 0.60
i 6 5.00 0.40 76 49 0.60
i 6 4.90 0.40 76 59 0.60
i 6 5.40 0.40 76 49 0.60
i 6 5.30 0.40 82 48 0.60
i 6 5.80 0.40 76 59 0.60
i 6 5.70 0.40 78 48 0.60
i 6 6.20 0.40 82 49 0.60
i 6 6.10 0.40 78 48 0.60
i 6 6.60 0.40 78 49 0.60
            
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>

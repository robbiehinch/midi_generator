<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
<CsInstruments>

sr = 48000
ksmps = 32
nchnls = 2
0dbfs = 1

giSoundFilekick_OH_1   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_FF_1.wav", 0, 0, 0
giSoundFilekick_OH_2   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_FF_2.wav", 0, 0, 0


giSoundFilekick_OH_13   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_F_1.wav", 0, 0, 0
giSoundFilekick_OH_14   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_F_2.wav", 0, 0, 0


giSoundFilekick_OH_24   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_P_1.wav", 0, 0, 0
giSoundFilekick_OH_25   ftgen   0, 0, 0, 1, "F:\\Media\\VSTs\\Drums\\SalamanderDrumkit\\salamanderDrumkit\\OH\\kick_OH_P_2.wav", 0, 0, 0

;opcode IndexOf, i, i[]
;opcode IndexOf, i[], i

;iAin[] xin             ; read input parameters
;iOut = 0
;xout iOut
;endop
        
opcode IndexOf, i, i[]ii

iAin[], iValSearch, iCurrIndex xin             ; read input parameters
iOut = 0

	iFoundVal = iAin[iCurrIndex]
	if (iFoundVal > iValSearch) then
		iOut = iCurrIndex
	else
		iOut IndexOf iAin, iValSearch, (iCurrIndex+1)
	endif


xout iOut
endop
;        
        
gikick_OH_[] fillarray 0, 0, 0
gikick_OH_vels[] fillarray 40, 80, 128

gikick_OH_Arr[] fillarray giSoundFilekick_OH_1, giSoundFilekick_OH_2, giSoundFilekick_OH_13, giSoundFilekick_OH_14, giSoundFilekick_OH_24, giSoundFilekick_OH_25

gikick_OH_offsets[] fillarray 0, 2, 4
gikick_OH_lengths[] fillarray 2, 2, 2

instr 1
 iKick_OH_vels[] fillarray 40, 80, 128
 kAmp  =  ( p4 / 127 ) 
 kPitch  =   powoftwo( ( ( 60 - p5 ) / 12 ) ) 
 kLoopStart  =  0 
 kPan  =  p6 
; iSampleArrayChoice IndexOf 0
 iSampleArrayChoice IndexOf ikick_OH_vels, p4, 0
 iSampleOffset = gikick_OH_offsets[iSampleArrayChoice]
 iSampleChoiceInArray = gikick_OH_[iSampleArrayChoice]
 iSampleChoice = iSampleOffset + iSampleChoiceInArray
; iSampleArray = gikick_OH_Arr[iSampleChoice]
 iSampleId  =  gikick_OH_Arr[iSampleChoiceInArray]
 iVelocityLengths = gikick_OH_lengths[iSampleArrayChoice]
 gikick_OH_[iSampleArrayChoice] = ( ( iSampleChoiceInArray + 1 ) %  iVelocityLengths ) 

; iSampleId = gikick_OH_Arr[0]

 kLoopEnd  =  (  nsamp( iSampleId ) / sr ) 
 kCrossFade  =  0 
 aSigL, aSigR    loscil kAmp, kPitch, iSampleId, 1, 0 
 kPanL    min ( 1 - kPan ), 0.5 
 kPanR    min kPan, 0.5 
 kPanL  =  ( kPanL * 2 ) 
 kPanR  =  ( kPanR * 2 ) 
  printf "kPanL: %f, kPanR: %f\n", 1, kPanL, kPanR 
 aSigLPan  =  ( aSigL * kPanL ) 
 aSigRPan  =  ( aSigR * kPanR ) 
  outs aSigLPan, aSigRPan
endin

instr 8
endin

instr 99
endin

</CsInstruments>
<CsScore>

            i 1 0.00 1.20 114 49 0.00
i 1 1.20 1.60 78 48 1.00
i 1 1.60 1.20 116 59 0.00
i 1 2.80 1.60 78 48 1.00

            
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>

﻿module RhythmEvent

type IRhythmEvent =
    abstract StartTime : float32 with get
    abstract Duration : float32 with get
    abstract Velocity : int with get
    abstract member Copy : float32 * float32 * int -> IRhythmEvent


type RhythmEvent(startTime:float32, duration:float32, velocity:int) = class
    interface IRhythmEvent with
        member x.StartTime = startTime
        member x.Duration = duration
        member x.Velocity = velocity
        member x.Copy( startTime, duration, velocity) =
            new RhythmEvent(startTime, duration, velocity) :> IRhythmEvent

    member x.CopyWithNewVelocity newVelocity =
        new RhythmEvent(startTime, duration, newVelocity)

    member x.CopyWithNewPitchGen pitchGen =
        new RhythmEvent(startTime, duration, velocity)

end

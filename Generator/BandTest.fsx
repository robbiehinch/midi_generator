﻿
#r @"..\packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"

open Midi
open System.Linq

let loopBe1Out = OutputDevice.InstalledDevices.FirstOrDefault(fun d -> d.Name.Contains("LoopBe"))

loopBe1Out.Open()

//drums
//clock.Stop();
//clock.Reset();
//loopBe1Out.SilenceAllNotes()

#load "BigMonoFsz.fs"
#load "MTPDK2.fs"
#load "PitchGenerator.fs"
#load "PatternGenerator.fs"
#load "RhythmEvent.fs"
#load "Note.fs"
#load "VelocityEffect.fs"
#load "DrumSequencer.fs"
#load "ChordEvolver.fs"
#load "SubBassGenerator.fs"

let bigMono = new BigMonoFsz.BigMonoFsz()
let mtpdk2 = new MTPDK2.MTPDK2()
let clock = Clock(240.0f)

let snareHits = [|
                [| 0; 0; 1; 0 |];
                |]
let snarePitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.SnareLH; bigMono.SnareRH |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.Snare1; mtpdk2.Snare2; mtpdk2.Snare3 |]) :> PitchGenerator.IPitchGenerator
    |]

snarePitchGens |> DrumSequencer.SequenceNotesOnChannels snareHits [| 0 |] 4 4 16 loopBe1Out 0.0f clock 1

let kickHits = [|
                [| 1; 0; 0; 0 |];
                [| 1; 1; 0; 0 |]
                |]
let kickPitchGens =  [|
    new PitchGenerator.Unit(bigMono.Kick) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.Kick; mtpdk2.Kick2; mtpdk2.Kick3 |]) :> PitchGenerator.IPitchGenerator
    |]

kickPitchGens |> DrumSequencer.SequenceNotesOnChannels kickHits [| 0 |] 4 8 8 loopBe1Out 0.0f clock 1
[| new PitchGenerator.Unit(bigMono.Kick) :> PitchGenerator.IPitchGenerator |] |> DrumSequencer.SequenceNotesOnChannels kickHits [| 0 |] 4 8 8 loopBe1Out 0.0f clock 6


let hiHatPitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.HiHat1; bigMono.HiHat2; bigMono.HiHat3 |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.HiHatClosed; mtpdk2.HiHatHalfClosed; mtpdk2.HiHatPedal |]) :> PitchGenerator.IPitchGenerator
    |]

hiHatPitchGens |> DrumSequencer.SequenceNotesOnChannels [| [| 1; 1; 1; 1 |] |] [| 0 |] 4 4 16 loopBe1Out 0.0f clock 1


let crashHits = [|
                [| 0; 0; 0; 0 |];
                [| 0; 1; 0; 0 |];
                [| 1; 0; 0; 1 |]
                [| 1; 1; 0; 0 |]
                |]
let crashPitchGens =  [|
    new PitchGenerator.NotLast([| bigMono.Crash1; bigMono.Crash2; bigMono.HiHatOpen |]) :> PitchGenerator.IPitchGenerator ;
    new PitchGenerator.NotLast([| mtpdk2.China; mtpdk2.CrashLeft; mtpdk2.CrashRight; mtpdk2.Splash |]) :> PitchGenerator.IPitchGenerator
    |]
crashPitchGens |> DrumSequencer.SequenceNotesOnChannels crashHits [| 0; 2 |] 4 16 4 loopBe1Out 0.0f clock 1

let noteRhythm = [|
                [| 1; 0; 1; 0 |];
                [| 1; 0; 1; 1 |];
                [| 0; 1; 1; 0 |];
                [| 0; 0; 0; 1 |];
//                [| 1; 0; 1; 1 |];
                |]

let scale = new Scale(new Note('C'), Scale.Major)
let bassGen = new PitchGenerator.Soloist(scale, Pitch.C4, Pitch.C3, Pitch.GSharp4)
let chordGen = new PitchGenerator.Chords(scale, bassGen)


let bassGens =  [|
     bassGen :> PitchGenerator.IPitchGenerator
    |]

let bassNoteMessages = bassGens |> DrumSequencer.GenMidiNotesOnChannels noteRhythm [| 0 |] 4 16 4 loopBe1Out 0.0f clock 3
                                |> Seq.map (fun ms -> ms |> Seq.toArray)
                                |> Seq.toArray

bassNoteMessages |> Seq.iter (fun channNotes -> channNotes |> Seq.iter (fun m -> clock.Schedule(m)))
let chordMessages = bassNoteMessages.[0] |> ChordEvolver.GenChordMessagesFromBassLine clock Channel.Channel4 scale
chordMessages |> Seq.iter (fun m -> clock.Schedule(m))
let subBassMessages = bassNoteMessages.[0] |> SubBassGenerator.GenSubBassFromBassLine clock Channel.Channel5 scale
subBassMessages |> Seq.iter (fun m -> clock.Schedule(m))

bassNoteMessages.[0].Length

clock.Start();


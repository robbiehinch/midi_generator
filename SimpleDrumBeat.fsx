﻿
#r @"F:\src\music\CsoundNetLib\CsoundNetLib\bin\Release\Csound6NetLib.dll"
#r @"packages\midi-dot-net.1.1.0\lib\net35\Midi.dll"
#r @"Generator\bin\Debug\Generator.dll"

open Midi
open System.Linq


let midiDevice = OutputDevice.InstalledDevices.FirstOrDefault()
let drumClock = Clock(240.0f)


//hiHat
let hiHatHits = [|
                [| 1; 1; 1; 1 |];
                |]

let midRanges = Enumerable.Range((int)(Pitch.C4)-1, 3) |> Seq.map (fun pi -> System.Enum.ToObject(Pitch.A0.GetType(), pi))
                                                        |> Seq.cast<Pitch>
                                                        |> Seq.toArray

let hiHatPitchGens =  [|
    new PitchGenerator.NotLast(midRanges) :> PitchGenerator.IPitchGenerator
    |]

let hiHatNotes = hiHatPitchGens |> DrumSequencer.GenMidiNotesOnChannels hiHatHits [| 0 |] 4 4 16 midiDevice 0.0f drumClock 8
                                |> Seq.collect ( fun s -> s)
                                |> Seq.toArray

let panner = Pan.SinePanCreate 100. 222. 0.1 0.5
//let panner = Pan.PanWindowCreate 0.5 0.1 0.02
//let panner = Pan.AlternatePanCreate

let hiHatPannedNotes = Pan.PanNotes hiHatNotes panner
                        |> Seq.toArray
let hiHatScore = MidiToCSoundScore.Convert (MidiToCSoundScore.PannedMidiNotes hiHatPannedNotes) 0.5 0.4f


//kick
let kickHits = [|
                [| 1; 0; 0; 0 |];
                [| 1; 1; 0; 0 |];
                |]

let kickNotes = hiHatPitchGens |> DrumSequencer.GenMidiNotesOnChannels kickHits [| 0 |] 4 8 8 midiDevice 0.0f drumClock 0
                                |> Seq.collect ( fun s -> s)
                                |> Seq.toArray

let kickScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes kickNotes) 0.5 0.4f


//snare
let snareHits = [|
                [| 0; 0; 1; 0 |];
                [| 0; 0; 1; 0 |];
                |]

let snareNotes = hiHatPitchGens |> DrumSequencer.GenMidiNotesOnChannels snareHits [| 0 |] 4 8 8 midiDevice 0.0f drumClock 5
                                |> Seq.collect ( fun s -> s)
                                |> Seq.toArray

let snareScore = MidiToCSoundScore.Convert (MidiToCSoundScore.MidiNotes snareNotes) 0.4 0.4f


//bring it together
let drumNotes = [|hiHatNotes; kickNotes; snareNotes|]

let usedChannels = (Seq.concat drumNotes) |> Seq.map (fun n -> n.Channel)
                                            |> Seq.distinct
                                            |> Seq.map (fun c -> sprintf "instr %d," (System.Convert.ToInt32(c) + 1))
                                            |> Seq.toArray


//full score
let scores = [|hiHatScore; kickScore; snareScore|]
let drumScoreLines = (Seq.concat scores) |> String.concat "\n"

let score = sprintf "
            %s
            " drumScoreLines


//let drumFolder = "H:\Media\VSTs\Drums\SalamanderDrumkit\salamanderDrumkit"
let drumFolder = "F:\Media\VSTs\Drums\SalamanderDrumkit\salamanderDrumkit"
let drumSfz = "ALL.sfz"
let drumCSoundInsts = SfzParser.SfzToCSoundFtgen drumFolder drumSfz
                            |> Seq.toArray

let drumCSoundInstsRefed = drumCSoundInsts
                            |> Seq.filter (fun (t, i) -> usedChannels |> Array.exists (fun uc -> i.Contains(uc) ))
                            |> Seq.toArray

let wavs = drumCSoundInstsRefed |> Array.map (fun (w, i) -> w)
let insts = drumCSoundInstsRefed |> Array.map (fun (w, i) -> i)

let orchestra = sprintf "
                sr = 48000
                ksmps = 32
                nchnls = 2
                0dbfs = 1

                %s

                %s
                instr 99
                endin" (wavs |> String.concat "\n" ) (insts |> String.concat "\n" )

let option = "-odac"

//let csdText = (MidiToCSoundScore.GenCSD option orchestra score)
//System.IO.File.WriteAllText(__SOURCE_FILE__ + ".csd", csdText)

let ExecuteInCSound option orchestra score p ct =
    use c = new csound6netlib.Csound6Net()
    c.SetOption(option) |> ignore
    c.CompileOrc(orchestra) |> ignore
    c.ReadScore(score) |> ignore
    c.Start() |> ignore
//    c.PerformAsync(p, ct)
    c.Perform() |> ignore
    c.Stop()
//
//printfn "Play?"
//let run = System.Console.In.ReadLine()
//
//if run.ToLower().StartsWith("y") then (ExecuteInCSound option orchestra score) 

let p = new System.Progress<float32>()
let ct = new System.Threading.CancellationToken()
let t = ExecuteInCSound option orchestra score p ct
//t.Wait()

